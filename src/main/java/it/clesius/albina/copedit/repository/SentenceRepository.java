package it.clesius.albina.copedit.repository;

import it.clesius.albina.copedit.domain.Sentence;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Sentence entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SentenceRepository extends JpaRepository<Sentence, Long> {

}
