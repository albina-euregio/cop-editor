package it.clesius.albina.copedit.repository;

import it.clesius.albina.copedit.domain.SentenceLang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SentenceLang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SentenceLangRepository extends JpaRepository<SentenceLang, Long> {

}
