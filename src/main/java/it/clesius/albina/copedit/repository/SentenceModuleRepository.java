package it.clesius.albina.copedit.repository;

import it.clesius.albina.copedit.domain.SentenceModule;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SentenceModule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SentenceModuleRepository extends JpaRepository<SentenceModule, Long> {

}
