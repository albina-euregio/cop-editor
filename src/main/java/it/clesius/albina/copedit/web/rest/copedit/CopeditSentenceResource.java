package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.domain.copedit.SentenceDTO;
import it.clesius.albina.copedit.repository.custom.CopeditSentenceRepository;
import it.clesius.albina.copedit.service.SentenceService;
import it.clesius.albina.copedit.web.rest.SentenceResource;
import it.clesius.albina.copedit.web.rest.copedit.validators.SentenceValidator;
import it.clesius.albina.copedit.web.rest.errors.FieldErrorVM;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import it.clesius.albina.copedit.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/copedit")
public class CopeditSentenceResource {

    private final Logger log = LoggerFactory.getLogger(SentenceResource.class);

    private static final String ENTITY_NAME = "sentence";

    @Autowired
    CopeditSentenceRepository copeditSentenceRepository;
    @Autowired
    SentenceService sentenceService;
    @Autowired
    SentenceValidator sentenceValidator;

    /**
     * POST  /sentences : Create a new sentence.
     *
     * @param sentence the sentence to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sentence, or with status 400 (Bad Request) if the sentence has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sentences")
    @Timed
    public ResponseEntity<?> createSentence(@RequestBody SentenceDTO sentence) throws URISyntaxException {
        log.debug("REST request to save Sentence : {}", sentence);
        Boolean sentenceIsIncomplete = sentenceValidator.sentenceIsComplete(sentence);
        if (!sentenceIsIncomplete) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "Sentence must have all headers and name and at least one phraseOption per language"));
        }
        Boolean sentenceAlreadyExists = sentenceValidator.sentenceAlreadyExists(sentence);
        if (sentenceAlreadyExists) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "A sentence with name " +
                sentence.getSentencesDE().getName() + " already exists"));
        }
        SentenceDTO result = copeditSentenceRepository.createSentence(sentence);
        return ResponseEntity.ok()
            //.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sentences : Updates an existing sentence.
     *
     * @param sentence the sentence to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sentence,
     * or with status 400 (Bad Request) if the sentence is not valid,
     * or with status 500 (Internal Server Error) if the sentence couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sentences")
    @Timed
    public ResponseEntity<?> updateSentence(@RequestBody SentenceDTO sentence) throws URISyntaxException {
        log.debug("REST request to update Sentence : {}", sentence);
        Boolean sentenceIsIncomplete = sentenceValidator.sentenceIsComplete(sentence);
        if (!sentenceIsIncomplete) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "Sentence must have all headers and name and at least one phraseOption per language"));
        }
        Boolean sentenceAlreadyExists = sentenceValidator.sentenceAlreadyExists(sentence);
        if (sentenceAlreadyExists) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "A sentence with name " +
                sentence.getSentencesDE().getName() + " already exists"));
        }

        SentenceDTO result = copeditSentenceRepository.updateSentence(sentence);
        return ResponseEntity.ok()
            //.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sentence.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sentences : get all the sentences.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sentences in body
     */
    @GetMapping("/sentences")
    @Timed
    public ResponseEntity<List<Sentence>> getAllSentences(Pageable pageable,
                                                          @RequestParam(value = "deleted", required = false) Boolean deleted,
                                                          @RequestParam(value = "joker", required = false) Boolean joker,
                                                          @RequestParam(value = "search", required = false) String search,
                                                          @RequestParam(value = "phrase", required = false) Integer phraseId,
                                                          @RequestParam(value = "position", required = false) Integer position,
                                                          @RequestParam(value = "domain", required = false) Integer domainId,
                                                          @RequestParam(value = "version", required = false) String versionId
    ) {
        log.debug("REST request to get a page of Sentences");
        Page<Sentence> page = copeditSentenceRepository.findAll(pageable, deleted, joker, search, phraseId, position, domainId, versionId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/copedit/sentences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sentences/{header}")
    @Timed
    public ResponseEntity<SentenceDTO> getSentenceByHeader(@PathVariable String header, @RequestParam(value = "domain", required = true) Integer domainId) {
        log.debug("REST request to get Sentence : {}", header);
        SentenceDTO sentence = copeditSentenceRepository.findSentencesByheader(header, domainId);
        return ResponseUtil.wrapOrNotFound(Optional.of(sentence));
    }

    @GetMapping("/sentences/newSentence")
    @Timed
    public ResponseEntity<SentenceDTO> getNewSentence(@RequestParam(value = "domain", required = false, defaultValue = "1") Integer domain) {
        log.debug("REST request to get new Sentence : {}");
        SentenceDTO sentence = copeditSentenceRepository.getNewSentence(domain);
        return ResponseUtil.wrapOrNotFound(Optional.of(sentence));
    }

    /**
     * GET  /sentences/:id : get the "id" sentence.
     *
     * @param id the id of the sentence to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sentence, or with status 404 (Not Found)
     */
   /* @GetMapping("/sentences/{id}")
    @Timed
    public ResponseEntity<Sentence> getSentence(@PathVariable Long id) {
        log.debug("REST request to get Sentence : {}", id);
        Optional<Sentence> sentence = sentenceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sentence);
    }*/

    /**
     * DELETE  /sentences/:id : delete the "id" sentence.
     *
     * @param name the name of the sentence to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sentences/{name}")
    @Timed
    public ResponseEntity<Void> deleteSentence(@PathVariable String name) {
        log.debug("REST request to delete Sentence : {}", name);
        copeditSentenceRepository.deleteSentence(name);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, name)).build();
    }
}
