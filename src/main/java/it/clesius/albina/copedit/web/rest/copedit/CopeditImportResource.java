package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.config.ApplicationProperties;
import it.clesius.albina.copedit.domain.enumeration.LanguageEnum;
import it.clesius.albina.copedit.service.copedit.ImportFilesService;
import it.clesius.albina.copedit.web.rest.copedit.gitControl.branch;
import it.clesius.albina.copedit.web.rest.errors.FieldErrorVM;
import it.clesius.albina.copedit.web.rest.copedit.gitControl.GitControl;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.lib.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/copedit")
public class CopeditImportResource {

    private final Logger log = LoggerFactory.getLogger(CopeditImportResource.class);

    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    ImportFilesService importFilesService;

    // 'test socket'
//    @Autowired
 //   private SimpMessagingTemplate template;



    /**
     * POST  /import : Import a zip file.
     *
     * @param files files array to import
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<?> createPhrase(@RequestPart(value = "files[]", required = true) MultipartFile[] files,
                                          @RequestParam(value = "domain", required = true) String domainName) {

        List<MultipartFile> fileList = Arrays.asList(files);

        /* check file names */
        FieldErrorVM error = checkFileNames(fileList.stream().map(MultipartFile::getOriginalFilename));
        if (error != null) {
            return ResponseEntity.badRequest().body(error);
        }

        LocalDateTime now = LocalDateTime.now();
        /* backup existing files in folder, overwrite with new files */
        fileList.forEach(file -> {
            try {
                File languageFile = new File(applicationProperties.getImportProperties().getImportFolder() + "/" + file.getOriginalFilename());
                if (languageFile.exists()) {

                    NumberFormat f = new DecimalFormat("00");
                    String backupDirName = String.format("backup_%d%s%s%s%s%s", now.getYear(), f.format(now.getDayOfMonth()), f.format(now.getMonthValue()), f.format(now.getHour()), f.format(now.getMinute()), f.format(now.getSecond()));
                    File backupDirectory = new File(applicationProperties.getImportProperties().getImportFolder() + "/" + backupDirName);
                    // backup file
                    FileUtils.moveFileToDirectory(languageFile, backupDirectory, true);
                }
                // overwrite/create
                FileUtils.writeByteArrayToFile(languageFile, file.getBytes());
            } catch (IOException e) {
                log.error("IOException in backup/copy import: ",  e);
            }
        });

        importFilesService.launchNewThreadAsync(domainName);
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }

    private FieldErrorVM checkFileNames(Stream<String> files) {
        TreeSet<String> expectedFiles = Arrays.stream(LanguageEnum.values())
            .map(l -> l.name().toLowerCase(Locale.ROOT) + ".zip")
            .collect(Collectors.toCollection(TreeSet::new));
        TreeSet<String> actualFiles = files.collect(Collectors.toCollection(TreeSet::new));

        if (Objects.equals(expectedFiles, actualFiles)) {
            return null;
        }
        return new FieldErrorVM("sentence", "", "The files must be named EXACTLY " + expectedFiles);
    }

    @GetMapping("/import/git/importFromRepoGit")
    public ResponseEntity<?> importFromRepoGit(@RequestParam(value = "domain", required = true) String domainName) {
        GitControl gc = null;

        try {
            gc = new GitControl(applicationProperties.getGitProperties().getLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());
            final File folder = new File(gc.getlocalPath());
            List<File> result = new ArrayList<File>();

            for (final File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                } else {
                    if (fileEntry.getName().contains(".zip")) {
                        result.add(fileEntry);
                    }
                }
            }
            FieldErrorVM error = checkFileNames(result.stream().map(File::getName));
            if (error != null) {
                return ResponseEntity.badRequest().body(error);
            }

            LocalDateTime now = LocalDateTime.now();

            log.info ("backup old files");
            // backup existing files in folder, overwrite with new files
            result.forEach(file -> {
                try {
                File languageFile = new File(applicationProperties.getImportProperties().getImportFolder() + "/" + file.getName());
                if (languageFile.exists()) {
                    NumberFormat f = new DecimalFormat("00");
                    String backupDirName = String.format("backup_%d%s%s%s%s%s", now.getYear(), f.format(now.getDayOfMonth()), f.format(now.getMonthValue()), f.format(now.getHour()), f.format(now.getMinute()), f.format(now.getSecond()));
                    File backupDirectory = new File(applicationProperties.getImportProperties().getImportFolder() + "/" + backupDirName);
                    // backup file

                        FileUtils.moveFileToDirectory(languageFile, backupDirectory, true);

                }
                // overwrite/create
                FileUtils.copyFile(file, languageFile, true);
                } catch (IOException e) {
                    log.error("IOException in backup/copy importFromRepoGit: ",  e);
                }
            });

        } catch (IOException e) {
            log.error("IOException in importFromRepoGit: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in importFromRepoGit: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in importFromRepoGit: ",  e);
        }

        log.info ("start async import");
        importFilesService.launchNewThreadAsync(domainName);
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }


    @GetMapping("/import/git/initBranch")
    @Timed
    public ResponseEntity<?> initBranch() {
        GitControl gc = null;
        try {
            gc = new GitControl(applicationProperties.getGitProperties().getLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());
            String result = gc.initBranch();

            return ResponseEntity.ok()
                .body(result);

        } catch (IOException e) {
            log.error("IOException in initBranch: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in initBranch: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in initBranch: ",  e);
        }

        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }

    @GetMapping("/import/git/getRemoteBranches")
    @Timed
    public ResponseEntity<List<branch>> getRemoteBranches() {
        GitControl gc = null;
        try {
            gc = new GitControl(applicationProperties.getGitProperties().getLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());
            List<Ref> list = gc.getRemoteBranches();

            List<branch> listNames = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                String name = list.get(i).getName();
                branch b = new branch();
                b.setName(name);
                b.setLabel(name.replace("refs/remotes/origin/", ""));
                listNames.add(b);
            }
            return ResponseEntity.ok()
                .body(listNames);

        } catch (IOException e) {
            log.error("IOException in getRemoteBranches: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in getRemoteBranches: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in getRemoteBranches: ",  e);
        }
        return null;
    }

    @GetMapping("/import/git/selectBranch")
    @Timed
    public ResponseEntity<?> selectBranch(@RequestParam(value = "lb", required = true) String localBranch) {
        GitControl gc = null;
        localBranch = localBranch.replace("refs/remotes/origin/", "refs/heads/");
        try {
            gc = new GitControl(applicationProperties.getGitProperties().getLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());
            String result = gc.selectBranch(localBranch);

            log.info("selected branch: " + result );

            // test websocket
            //this.template.convertAndSend("/import/importStatus", "OK" );

            return ResponseEntity.ok(new FieldErrorVM("", "branch", result));


        } catch (IOException e) {
            log.error("IOException in selectBranch: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in selectBranch: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in selectBranch: ",  e);
        }
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }


    @GetMapping(value = "", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Timed
    public ResponseEntity<byte[]> exportDomain(@RequestParam Integer domainId) throws IOException {
        return null;
    }


}
