package it.clesius.albina.copedit.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.service.PhraseOptionService;
import it.clesius.albina.copedit.web.rest.errors.BadRequestAlertException;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PhraseOption.
 */
@RestController
@RequestMapping("/api")
public class PhraseOptionResource {

    private final Logger log = LoggerFactory.getLogger(PhraseOptionResource.class);

    private static final String ENTITY_NAME = "phraseOption";

    private final PhraseOptionService phraseOptionService;

    public PhraseOptionResource(PhraseOptionService phraseOptionService) {
        this.phraseOptionService = phraseOptionService;
    }

    /**
     * POST  /phrase-options : Create a new phraseOption.
     *
     * @param phraseOption the phraseOption to create
     * @return the ResponseEntity with status 201 (Created) and with body the new phraseOption, or with status 400 (Bad Request) if the phraseOption has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/phrase-options")
    @Timed
    public ResponseEntity<PhraseOption> createPhraseOption(@RequestBody PhraseOption phraseOption) throws URISyntaxException {
        log.debug("REST request to save PhraseOption : {}", phraseOption);
        if (phraseOption.getId() != null) {
            throw new BadRequestAlertException("A new phraseOption cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhraseOption result = phraseOptionService.save(phraseOption);
        return ResponseEntity.created(new URI("/api/phrase-options/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /phrase-options : Updates an existing phraseOption.
     *
     * @param phraseOption the phraseOption to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated phraseOption,
     * or with status 400 (Bad Request) if the phraseOption is not valid,
     * or with status 500 (Internal Server Error) if the phraseOption couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/phrase-options")
    @Timed
    public ResponseEntity<PhraseOption> updatePhraseOption(@RequestBody PhraseOption phraseOption) throws URISyntaxException {
        log.debug("REST request to update PhraseOption : {}", phraseOption);
        if (phraseOption.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhraseOption result = phraseOptionService.save(phraseOption);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, phraseOption.getId().toString()))
            .body(result);
    }

    /**
     * GET  /phrase-options : get all the phraseOptions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of phraseOptions in body
     */
    @GetMapping("/phrase-options")
    @Timed
    public List<PhraseOption> getAllPhraseOptions() {
        log.debug("REST request to get all PhraseOptions");
        return phraseOptionService.findAll();
    }

    /**
     * GET  /phrase-options/:id : get the "id" phraseOption.
     *
     * @param id the id of the phraseOption to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the phraseOption, or with status 404 (Not Found)
     */
    @GetMapping("/phrase-options/{id}")
    @Timed
    public ResponseEntity<PhraseOption> getPhraseOption(@PathVariable Long id) {
        log.debug("REST request to get PhraseOption : {}", id);
        Optional<PhraseOption> phraseOption = phraseOptionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phraseOption);
    }

    /**
     * DELETE  /phrase-options/:id : delete the "id" phraseOption.
     *
     * @param id the id of the phraseOption to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/phrase-options/{id}")
    @Timed
    public ResponseEntity<Void> deletePhraseOption(@PathVariable Long id) {
        log.debug("REST request to delete PhraseOption : {}", id);
        phraseOptionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
