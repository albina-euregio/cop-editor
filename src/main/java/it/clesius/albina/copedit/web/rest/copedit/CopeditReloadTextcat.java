package it.clesius.albina.copedit.web.rest.copedit;

    import it.clesius.albina.copedit.config.ApplicationProperties;
    import it.clesius.albina.copedit.web.rest.errors.FieldErrorVM;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.http.ResponseEntity;
    import org.springframework.stereotype.Service;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RequestParam;
    import org.springframework.web.client.RestTemplate;
    import org.springframework.web.util.UriComponentsBuilder;

@Service
@RequestMapping("/api/copedit")
public class CopeditReloadTextcat {

    private final Logger log = LoggerFactory.getLogger(CopeditReloadTextcat.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @GetMapping("/reloadTexcat")
    public ResponseEntity<?> reloadTexcat(@RequestParam(value = "lang", required = true) String lang,
                                                 @RequestParam(value = "domain", required = true) String domain) {
        try {
            RestTemplate rt = new RestTemplate();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(applicationProperties.getTextcatApi().gettextcatAPIurl() + applicationProperties.getTextcatApi().getReloadUrl())
                .queryParam("lang", lang)
                .queryParam("domain", domain);

            String response = rt.getForObject(builder.toUriString(), String.class);
            log.info(String.format("Recode outcome: %s", response));
            return ResponseEntity.ok().body(new FieldErrorVM("", "", response));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().body(new FieldErrorVM("", "", "ERROR"));
    }
}
