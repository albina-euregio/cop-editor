package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import it.clesius.albina.copedit.domain.copedit.PhraseOptionEditDTO;
import it.clesius.albina.copedit.repository.custom.CopeditPhraseOptionRepository;
import it.clesius.albina.copedit.repository.custom.CopeditPhraseRepository;
import it.clesius.albina.copedit.web.rest.PhraseResource;
import it.clesius.albina.copedit.web.rest.copedit.validators.PhraseValidator;
import it.clesius.albina.copedit.web.rest.errors.FieldErrorVM;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.Optional;

@Service
@RequestMapping("/api/copedit")
public class CopeditPhraseResource {

    private final Logger log = LoggerFactory.getLogger(PhraseResource.class);

    private static final String ENTITY_NAME = "phrase";

    @Autowired
    CopeditPhraseRepository copeditPhraseRepository;
    @Autowired
    CopeditPhraseOptionRepository copeditPhraseOptionRepository;
    @Autowired
    PhraseValidator phraseValidator;

    /**
     * POST  /phrases : Create a new phrase.
     *
     * @param phrase the phrase to create
     * @return the ResponseEntity with status 201 (Created) and with body the new phrase, or with status 400 (Bad Request) if the phrase has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/phrases")
    @Timed
    public ResponseEntity<?> createPhrase(@RequestBody PhraseOptionEditDTO phrase) throws URISyntaxException {
        log.debug("REST request to save Phrase : {}", phrase);
        Boolean sentenceAlreadyExists = phraseValidator.phraseAlreadyExistsOnCreate(phrase);
        if (sentenceAlreadyExists) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("phrase", "", "A phrase with name " +
                phrase.getPhrasesDE().getName() + " already exists"));
        }
        PhraseOptionEditDTO result = copeditPhraseRepository.createPhrase(phrase);
        return ResponseEntity.ok().body(result);
    }

    /**
     * PUT  /phrases : Updates an existing phrase.
     *
     * @param phrase the phrase to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated phrase,
     * or with status 400 (Bad Request) if the phrase is not valid,
     * or with status 500 (Internal Server Error) if the phrase couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/phrases")
    @Timed
    public ResponseEntity<?> updatePhrase(@RequestBody PhraseOptionEditDTO phrase) throws URISyntaxException {
        log.debug("REST request to update Phrase : {}", phrase);
        Boolean sentenceAlreadyExists = phraseValidator.phraseAlreadyExists(phrase);
        if (sentenceAlreadyExists) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("phrase", "", "A phrase with name " +
                phrase.getPhrasesDE().getName() + " already exists"));
        }
        PhraseOptionEditDTO result = copeditPhraseRepository.updatePhrase(phrase);
        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /phrases : get all the phrases.
     *
     * @param domain domain in which create new phrase
     * @return the ResponseEntity with status 200 (OK) and the empty phrase in body
     */
    @GetMapping("/phrases/newPhrase")
    @Timed
    public ResponseEntity<PhraseOptionEditDTO> getNewPhrase(@RequestParam(value = "domain", required = false, defaultValue = "1") Integer domain) {
        log.debug("REST request to get a new Phrase");
        PhraseOptionEditDTO phrase = copeditPhraseRepository.getNewPhrase(domain);
        return ResponseUtil.wrapOrNotFound(Optional.of(phrase));
    }

    @GetMapping("/phrases/{name}")
    @Timed
    public ResponseEntity<PhraseOptionEditDTO> getSentenceByHeader(@PathVariable String name, @RequestParam(value = "domain", required = true) Integer domain, @RequestParam(value = "withSentences", required = false, defaultValue = "false") Boolean withSentences ) {
        log.debug("REST request to get Sentence : {}", name);
        PhraseOptionEditDTO phrase = copeditPhraseRepository.findPhraseByName(name, domain, withSentences );
        return ResponseUtil.wrapOrNotFound(Optional.of(phrase));
    }

    @GetMapping("/phrases/pv/{name}")
    @Timed
    public ResponseEntity<PhraseOptionEditDTO> getSentenceByHeader_pv(@PathVariable String name, @RequestParam(value = "domain", required = true) Integer domain) {
        log.debug("REST request to get Sentence : {}", name);
        PhraseOptionEditDTO phrase = copeditPhraseRepository.findPhraseByNameOLD(name, domain);
        return ResponseUtil.wrapOrNotFound(Optional.of(phrase));
    }

    /**
     * DELETE  /phrases/:id : delete the "id" phrase.
     *
     * @param id the id of the phrase to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/phrases/{id}")
    @Timed
    public ResponseEntity<Void> deletePhrase(@PathVariable Long id) {
        log.debug("REST request to delete Phrase : {}", id);
        copeditPhraseOptionRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
