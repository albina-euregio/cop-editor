package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.domain.Domain;
import it.clesius.albina.copedit.service.DomainService;
import it.clesius.albina.copedit.web.rest.DomainResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/copedit")
public class CopeditDomainResource {

    private final Logger log = LoggerFactory.getLogger(DomainResource.class);

    private static final String ENTITY_NAME = "domain";

    @Autowired
    private DomainService domainService;


    @GetMapping("/domains")
    @Timed
    public List<Domain> getAllDomains() {
        log.debug("REST request to get all Domains");
        return domainService.findAll();
    }
}
