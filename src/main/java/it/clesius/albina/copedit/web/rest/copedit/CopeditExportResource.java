package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.config.ApplicationProperties;
import it.clesius.albina.copedit.service.copedit.CopeditExportService;
import it.clesius.albina.copedit.web.rest.copedit.gitControl.GitControl;
import it.clesius.albina.copedit.web.rest.copedit.gitControl.branch;
import it.clesius.albina.copedit.web.rest.errors.FieldErrorVM;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.lib.Ref;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/copedit")
public class CopeditExportResource {

    private final Logger log = LoggerFactory.getLogger(CopeditExportService.class);

    @Autowired
    CopeditExportService copeditExportService;
    @Autowired
    ApplicationProperties applicationProperties;


    private final String ATTACHMENT_FILENAME = "attachment; filename=\"%s\"";

    @GetMapping(value = "/export", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Timed
    public ResponseEntity<byte[]> exportDomain(@RequestParam Integer domainId) throws IOException {

            try {
            byte[] zipFile = copeditExportService.exportDomain(domainId);
            String fileName = copeditExportService.getExportZipFileName(domainId);
            return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, String.format(ATTACHMENT_FILENAME, fileName))
                .contentType(MediaType.parseMediaType("application/zip"))
                .body(zipFile);
        } catch (IOException e) {
                log.error("IOException in export: ",  e);
        }
        return null;

    }


    @GetMapping("/export/git/initBranch")
    @Timed
    public ResponseEntity<?> initBranch() {
        GitControl gc = null;
        try {
            gc = new GitControl(applicationProperties.getGitProperties().getExportLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());
            String result = gc.initBranch();

            return ResponseEntity.ok()
                .body(result);

        } catch (IOException e) {
            log.error("IOException in initBranch: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in initBranch: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in initBranch: ",  e);
        }
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }

    @GetMapping("/export/git/getRemoteBranches")
    @Timed
    public ResponseEntity<List<branch>> getRemoteBranches() {
        GitControl gc = null;
        try {
            gc = new GitControl(applicationProperties.getGitProperties().getExportLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());
            List<Ref> list = gc.getRemoteBranches();

            List<branch> listNames = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                String name = list.get(i).getName();
                branch b = new branch();
                b.setName(name);
                b.setLabel(name.replace("refs/remotes/origin/", ""));
                listNames.add(b);
            }

            if (listNames.size() == 0) {
                initBranch();
                getRemoteBranches();
            }

            return ResponseEntity.ok()
                .body(listNames);

        } catch (IOException e) {
            log.error("IOException in getRemoteBranches: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in getRemoteBranches: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in getRemoteBranches: ",  e);
        }
        return null;
    }

    @GetMapping("/export/git/selectRemoteBranch")
    @Timed
    public ResponseEntity<?> selectRemoteBranch(@RequestParam(value = "rb", required = true) String localBranch) {
        GitControl gc = null;
        localBranch = localBranch.replace("refs/remotes/origin/", "refs/heads/");
        try {
            gc = new GitControl(applicationProperties.getGitProperties().getExportLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());
            String result = gc.selectBranch(localBranch);

            return ResponseEntity.ok(new FieldErrorVM("", "branch", result));

        } catch (IOException e) {
            log.error("IOException in selectRemoteBranch: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in selectRemoteBranch: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in selectRemoteBranch: ",  e);
        }
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }

    @GetMapping("/export/git/pushToGit")
    public ResponseEntity<?> pushToGit(@RequestParam(value = "domain", required = true) String domainID, @RequestParam(value = "branchName", required = true) String branchName) {
        GitControl gc = null;

        try {
            //crete zip files in temp dir
            byte[] zipFile = copeditExportService.exportDomainToGit(Integer.parseInt(domainID));

            File srcFolder = new File(applicationProperties.getExportProperties().getTempFolder());
            List<File> srcResult = new ArrayList<File>();

            for (final File fileEntry : srcFolder.listFiles()) {
                if (fileEntry.isDirectory()) {
                } else {
                    if (fileEntry.getName().contains(".zip")) {
                        srcResult.add(fileEntry);
                    }
                }
            }

            if (srcResult.size() != 4) {
                return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "The number of files to upload must be EXACTLY 4"));
            }

            // check file names
            Boolean isDePresent = srcResult.stream().filter(x -> x.getName().equals("de.zip")).count() == 1L;
            Boolean isFrPresent = srcResult.stream().filter(x -> x.getName().equals("fr.zip")).count() == 1L;
            Boolean isEnPresent = srcResult.stream().filter(x -> x.getName().equals("en.zip")).count() == 1L;
            Boolean isItPresent = srcResult.stream().filter(x -> x.getName().equals("it.zip")).count() == 1L;

            if (!BooleanUtils.and(new Boolean[]{isDePresent, isFrPresent, isEnPresent, isItPresent})) {
                return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "Not exported de.zip, it.zip, fr.zip and en.zip"));
            }

            log.info("Zip files created");

            gc = new GitControl(applicationProperties.getGitProperties().getExportLocalPath(), applicationProperties.getGitProperties().getRemotePath(), applicationProperties.getGitProperties().getName(),applicationProperties.getGitProperties().getPassword());

            File folder = new File(gc.getlocalPath());
            List<File> result = new ArrayList<File>();
            for (final File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                } else {
                    if (fileEntry.getName().contains(".zip")) {
                        result.add(fileEntry);
                    }
                }
            }

           //checkOut repo
            gc.selectBranch(branchName);
            log.info("CheckOut branch: " + branchName);
            LocalDateTime now = LocalDateTime.now();

            // backup existing files in exportrepo folder, overwrite with new files
            result.forEach(file -> {
                try {
                    File languageFile = new File(applicationProperties.getGitProperties().getExportLocalPath() + "/" + file.getName());
                    if (languageFile.exists()) {
                        NumberFormat f = new DecimalFormat("00");
                        String backupDirName = String.format("backup_%d%s%s%s%s%s", now.getYear(), f.format(now.getDayOfMonth()), f.format(now.getMonthValue()), f.format(now.getHour()), f.format(now.getMinute()), f.format(now.getSecond()));
                        File backupDirectory = new File(applicationProperties.getExportProperties().getExportFolder() + "/" + backupDirName);
                        // backup file

                        FileUtils.moveFileToDirectory(languageFile, backupDirectory, true);

                    }
                    // overwrite/create
                    File srclanguageFile = new File(applicationProperties.getExportProperties().getTempFolder() + "/" + file.getName());
                    FileUtils.copyFile(srclanguageFile, languageFile, true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            log.info("Zip files copied to repo");

            gc.pushToRepo();

            log.info("Push finished");

        } catch (IOException e) {
            log.error("IOException in pushToGit: ",  e);
        } catch (NoFilepatternException e) {
            log.error("NoFilepatternException in pushToGit: ",  e);
        } catch (GitAPIException e) {
            log.error("GitAPIException in pushToGit: ",  e);
        }
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }

    @GetMapping("/export/git/exportDB")
    public ResponseEntity<?> exportDB() {

        try {
            byte[] backUpDbFile = copeditExportService.exportDB();

        } catch (Exception e) {
                log.error("IOException in pushToGit: ",  e);
        }
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }
}
