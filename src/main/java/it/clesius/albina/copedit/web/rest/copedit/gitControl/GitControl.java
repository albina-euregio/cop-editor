package it.clesius.albina.copedit.web.rest.copedit.gitControl;

    import it.clesius.albina.copedit.service.copedit.CopeditExportService;
    import org.apache.commons.io.FileUtils;
    import org.eclipse.jgit.api.*;
    import org.eclipse.jgit.api.errors.*;
    import org.eclipse.jgit.internal.storage.file.FileRepository;
    import org.eclipse.jgit.lib.*;
    import org.eclipse.jgit.lib.IndexDiff.StageState;
    import org.eclipse.jgit.revwalk.RevCommit;
    import org.eclipse.jgit.revwalk.RevTree;
    import org.eclipse.jgit.revwalk.RevWalk;
    import org.eclipse.jgit.transport.CredentialsProvider;
    import org.eclipse.jgit.transport.PushResult;
    import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
    import org.eclipse.jgit.treewalk.AbstractTreeIterator;
    import org.eclipse.jgit.treewalk.CanonicalTreeParser;
    import org.eclipse.jgit.treewalk.TreeWalk;
    import org.eclipse.jgit.treewalk.filter.PathFilter;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import java.io.File;
    import java.io.IOException;
    import java.text.DecimalFormat;
    import java.text.NumberFormat;
    import java.time.LocalDateTime;
    import java.util.Iterator;
    import java.util.List;
    import java.util.Map;
    import java.util.Set;

/**
 *
 *
 * https://github.com/centic9/jgit-cookbook/tree/master/src/main/java/org/dstadler/jgit/porcelain
 *
 */

public class GitControl {

    private final Logger log = LoggerFactory.getLogger(CopeditExportService.class);

    private Repository localRepo;
    private Git git;
    private CredentialsProvider cp;
    private String name ;
    private String password ;
    private String localPath;
    private String remotePath;


    public GitControl(String localPath2, String  remotePath, String name, String password) throws IOException {
        this.localPath = localPath2;
        this.remotePath = remotePath;
        this.name = name;
        this.password = password;
        this.localRepo = new FileRepository(this.localPath + "/.git");
        cp = new UsernamePasswordCredentialsProvider(this.name, this.password);
        git = new Git(localRepo);
    }

    public List<Ref> getRemoteBranches() throws IOException, NoFilepatternException, GitAPIException {
        List<Ref> listRemoteRefsBranches = git.branchList().setListMode(ListBranchCommand.ListMode.REMOTE).call();
        return listRemoteRefsBranches;
    }

    public String getlocalPath() throws IOException, NoFilepatternException, GitAPIException {
        return this.localPath;
    }

    public String selectBranch(String localBranchName) throws IOException, NoFilepatternException, GitAPIException {

        String currentBranch = git.getRepository().getFullBranch();
        if (!currentBranch.equals(localBranchName)) {
            Ref command = git.checkout().setName(localBranchName).call();
            String remoteBranch = localBranchName.replace("refs/heads/","");
            PullResult result = git.pull()
                .setCredentialsProvider(cp)
                .setRemote("origin")
                .setRemoteBranchName(remoteBranch)
                .call();
            if (result.isSuccessful()) {
                log.info("Selected branch: " + currentBranch);
            } else {
                log.error(result.toString());
            }
            return command.getName();
        }
        return currentBranch;
    }

    public String initBranch() throws IOException, WrongRepositoryStateException,
        InvalidConfigurationException, DetachedHeadException,
        InvalidRemoteException, CanceledException, RefNotFoundException,
        NoHeadException, GitAPIException {
    /*
        List<Ref> listRemoteRefsBranches = git.branchList().setListMode(ListBranchCommand.ListMode.REMOTE).call();
        List<Ref> listLocalRefsBranches = git.branchList().call();
        for (Ref refBranch : listRemoteRefsBranches) {
            if (!listLocalRefsBranches.contains(refBranch)) {
                cloneRepo(refBranch.getName());
            }
            System.out.println("Branch : " + refBranch.getName());
        }
      */
        File tmplocalPath = new File(localPath);
        if (tmplocalPath.exists()) {
            FileUtils.forceDelete(tmplocalPath);
        }
        log.info ("deleted repo: " + tmplocalPath) ;

        if(localRepo.exactRef("refs/heads/master") == null) {
            cloneRepo("master");
            log.info("Cloned master");
        }
        log.info ("cloned master" ) ;
        if(localRepo.exactRef("refs/heads/production") == null) {
            // first we need to ensure that the remote branch is visible locally
            // cloneRepo("production");
            Ref ref = git.branchCreate().setName("production").setStartPoint("origin/production").call();
            System.out.println("Created local production branch with ref: " + ref);
            log.info ("Created local production branch with ref: " + ref ) ;
        }

        return "";

        /*

        // the diff works on TreeIterators, we prepare two for the two branches
        AbstractTreeIterator oldTreeParser = prepareTreeParser(localRepo, "refs/heads/production");
        AbstractTreeIterator newTreeParser = prepareTreeParser(localRepo, "refs/heads/master");

        // then the procelain diff-command returns a list of diff entries
        List<DiffEntry> diff = git.diff().setOldTree(oldTreeParser).setNewTree(newTreeParser).call();
        for (DiffEntry entry : diff) {
            System.out.println("Entry: " + entry);
        }
        */
    }

    public List<String> getFiles() throws IOException, NoFilepatternException, GitAPIException {
        RevTree tree = getTree(localRepo);
        try (TreeWalk treeWalk = new TreeWalk(localRepo)) {
            treeWalk.addTree(tree);
            treeWalk.setRecursive(false);
            treeWalk.setFilter(PathFilter.create("README.md"));
            if (!treeWalk.next()) {
                throw new IllegalStateException("Did not find expected file 'README.md'");
            }

            // FileMode specifies the type of file, FileMode.REGULAR_FILE for normal file, FileMode.EXECUTABLE_FILE for executable bit
            // set
            FileMode fileMode = treeWalk.getFileMode(0);
            ObjectLoader loader = localRepo.open(treeWalk.getObjectId(0));
            System.out.println("README.md: " + getFileMode(fileMode) + ", type: " + fileMode.getObjectType() + ", mode: " + fileMode +
                " size: " + loader.getSize());
        }
        return null;
    }


    private static RevTree getTree(Repository repository) throws IOException {
        ObjectId lastCommitId = repository.resolve("refs/heads/");

        // a RevWalk allows to walk over commits based on some filtering
        try (RevWalk revWalk = new RevWalk(repository)) {
            RevCommit commit = revWalk.parseCommit(lastCommitId);

            System.out.println("Time of commit (seconds since epoch): " + commit.getCommitTime());

            // and using commit's tree find the path
            RevTree tree = commit.getTree();
            System.out.println("Having tree: " + tree);
            return tree;
        }
    }

    private static String getFileMode(FileMode fileMode) {
        if (fileMode.equals(FileMode.EXECUTABLE_FILE)) {
            return "Executable File";
        } else if (fileMode.equals(FileMode.REGULAR_FILE)) {
            return "Normal File";
        } else if (fileMode.equals(FileMode.TREE)) {
            return "Directory";
        } else if (fileMode.equals(FileMode.SYMLINK)) {
            return "Symlink";
        } else {
            // there are a few others, see FileMode javadoc for details
            throw new IllegalArgumentException("Unknown type of file encountered: " + fileMode);
        }
    }


    private static AbstractTreeIterator prepareTreeParser(Repository repository, String ref) throws IOException {
        // from the commit we can build the tree which allows us to construct the TreeParser
        Ref head = repository.exactRef(ref);
        try (RevWalk walk = new RevWalk(repository)) {
            RevCommit commit = walk.parseCommit(head.getObjectId());
            RevTree tree = walk.parseTree(commit.getTree().getId());

            CanonicalTreeParser treeParser = new CanonicalTreeParser();
            try (ObjectReader reader = repository.newObjectReader()) {
                treeParser.reset(reader, tree.getId());
            }

            walk.dispose();

            return treeParser;
        }
    }

    public void checkUncommitted() throws IOException, NoFilepatternException, GitAPIException {
        Status status = git.status().call();
        Set<String> conflicting = status.getConflicting();
        for(String conflict : conflicting) {
            System.out.println("Conflicting: " + conflict);
        }

        Set<String> added = status.getAdded();
        for(String add : added) {
            System.out.println("Added: " + add);
        }

        Set<String> changed = status.getChanged();
        for(String change : changed) {
            System.out.println("Change: " + change);
        }

        Set<String> missing = status.getMissing();
        for(String miss : missing) {
            System.out.println("Missing: " + miss);
        }

        Set<String> modified = status.getModified();
        for(String modify : modified) {
            System.out.println("Modification: " + modify);
        }

        Set<String> removed = status.getRemoved();
        for(String remove : removed) {
            System.out.println("Removed: " + remove);
        }

        Set<String> uncommittedChanges = status.getUncommittedChanges();
        for(String uncommitted : uncommittedChanges) {
            System.out.println("Uncommitted: " + uncommitted);
        }

        Set<String> untracked = status.getUntracked();
        for(String untrack : untracked) {
            System.out.println("Untracked: " + untrack);
        }

        Set<String> untrackedFolders = status.getUntrackedFolders();
        for(String untrack : untrackedFolders) {
            System.out.println("Untracked Folder: " + untrack);
        }

        Map<String, StageState> conflictingStageState = status.getConflictingStageState();
        for(Map.Entry<String, StageState> entry : conflictingStageState.entrySet()) {
            System.out.println("ConflictingState: " + entry);
        }
    }

    public void cloneRepo(String branchName) throws IOException, NoFilepatternException, GitAPIException {
        Git.cloneRepository()
            .setURI(remotePath)
            .setDirectory(new File(localPath))
            .setCredentialsProvider(cp)
            .setBranch(branchName)
            .call();
    }

    public void addToRepo() throws IOException, NoFilepatternException, GitAPIException {
        AddCommand add = git.add();
        add.addFilepattern(".").call();
    }

    public void commitToRepo(String message) throws IOException, NoHeadException,
        NoMessageException, ConcurrentRefUpdateException,
        JGitInternalException, WrongRepositoryStateException, GitAPIException {
        git.commit().setMessage(message).call();
    }

    public void pushToRepo() throws IOException, JGitInternalException,
        InvalidRemoteException, GitAPIException {
        //comit
        LocalDateTime now = LocalDateTime.now();
        NumberFormat f = new DecimalFormat("00");
        String comitTime = String.format("%d%s%s%s%s%s", now.getYear(), f.format(now.getDayOfMonth()), f.format(now.getMonthValue()), f.format(now.getHour()), f.format(now.getMinute()), f.format(now.getSecond()));
        git.commit()
            .setAll(true)
            .setMessage("Commit from Copedit " + comitTime )
            .call();

        PushCommand pc = git.push();
        pc.setCredentialsProvider(cp)
            .setForce(true)
            .setPushAll();
        try {
            Iterator<PushResult> it = pc.call().iterator();
            if (it.hasNext()) {
                log.info("pushed: " + it.next().toString());
            }
        } catch (InvalidRemoteException e) {
            log.error("push failed");
        }
    }

    public void pullFromRepo() throws IOException, WrongRepositoryStateException,
        InvalidConfigurationException, DetachedHeadException,
        InvalidRemoteException, CanceledException, RefNotFoundException,
        NoHeadException, GitAPIException {
        git.pull().setCredentialsProvider(cp).call();
    }

}
