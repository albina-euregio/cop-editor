package it.clesius.albina.copedit.web.rest.copedit.validators;

import it.clesius.albina.copedit.domain.copedit.PhraseOptionEditDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

@Service
public class PhraseValidator {

    @Autowired
    EntityManager entityManager;

    /**
     * Checks if a phrase with same name already exists
     *
     * @param dto dto to check
     * @return true if phrase already exists, else false
     */
    public Boolean phraseAlreadyExists(PhraseOptionEditDTO dto) {

        List<String> ids = Arrays.asList(
            dto.getPhrasesDE().getId().toString(),
            dto.getPhrasesEN().getId().toString(),
            dto.getPhrasesIT().getId().toString(),
            dto.getPhrasesFR().getId().toString(),
            dto.getPhrasesES().getId().toString(),
            dto.getPhrasesCA().getId().toString(),
            dto.getPhrasesOC().getId().toString()
        );

        String query = String.format("SELECT count(*) FROM phrase_option WHERE name = '%s' AND domain_id = %d AND phrase_option_id NOT IN (%s)",
            dto.getPhrasesDE().getName(), dto.getDomain().getId(), String.join(",", ids));
        BigInteger count = (BigInteger) entityManager.createNativeQuery(query).getSingleResult();
        return count.signum() > 0;
    }

    /**
     * Checks if a phrase with same name already exists
     *
     * @param dto dto to check
     * @return true if phrase already exists, else false
     */
    public Boolean phraseAlreadyExistsOnCreate(PhraseOptionEditDTO dto) {

        String query = String.format("SELECT count(*) FROM phrase_option where name = '%s'",
            dto.getPhrasesDE().getName());
        BigInteger count = (BigInteger) entityManager.createNativeQuery(query).getSingleResult();
        return count.signum() > 0;
    }
}
