package it.clesius.albina.copedit.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;


/**
 * A PhraseOptionItem.
 */
@Entity
@Table(name = "phrase_option_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PhraseOptionItem implements Serializable {

    @Id
    @GeneratedValue(generator = "my_phrase_option_item")
    @SequenceGenerator(name = "my_phrase_option_item", sequenceName = "SEQ_PHRASE_OPTION_ITEM_ID", allocationSize = 1)
    @Column(name = "phrase_option_item_id")
    private Long phraseOptionItemId;

    @Column(name = "item_no")
    private Integer itemNo;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "phrase_option_id")
    private Long phraseOptionId;

    @Column(name = "version")
    private String version;

    @Column(name = "basis_version")
    private String basisVersion;

    @Column(name = "datenew")
    private ZonedDateTime datenew;

    @Column(name = "datelast")
    private ZonedDateTime datelast;

    public Long getPhraseOptionItemId() {
        return phraseOptionItemId;
    }

    public void setPhraseOptionItemId(Long phraseOptionItemId) {
        this.phraseOptionItemId = phraseOptionItemId;
    }

    public Integer getItemNo() {
        return itemNo;
    }

    public void setItemNo(Integer itemNo) {
        this.itemNo = itemNo;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getPhraseOptionId() {
        return phraseOptionId;
    }

    public void setPhraseOptionId(Long phraseOptionId) {
        this.phraseOptionId = phraseOptionId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBasisVersion() {
        return basisVersion;
    }

    public void setBasisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }


    public PhraseOptionItem phraseOptionItemId(Long phraseOptionItemId) {
        this.phraseOptionItemId = phraseOptionItemId;
        return this;
    }

    public PhraseOptionItem itemNo(Integer itemNo) {
        this.itemNo = itemNo;
        return this;
    }

    public PhraseOptionItem deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public PhraseOptionItem phraseOptionId(Long phraseOptionId) {
        this.phraseOptionId = phraseOptionId;
        return this;
    }

    public PhraseOptionItem version(String version) {
        this.version = version;
        return this;
    }

    public PhraseOptionItem basisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
        return this;
    }

    public PhraseOptionItem datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public PhraseOptionItem datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }
}
