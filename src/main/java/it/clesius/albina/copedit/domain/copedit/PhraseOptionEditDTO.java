package it.clesius.albina.copedit.domain.copedit;

import java.util.List;

public class PhraseOptionEditDTO {

    DomainDTO domain;
    VersionDTO version;
    PhraseLanguageDTO phrasesDE;
    PhraseLanguageDTO phrasesIT;
    PhraseLanguageDTO phrasesEN;
    PhraseLanguageDTO phrasesFR;
    PhraseLanguageDTO phrasesES;
    PhraseLanguageDTO phrasesCA;
    PhraseLanguageDTO phrasesOC;
    List<SentenceLanguage> sentenceList;

    public PhraseOptionEditDTO setSentenceList(List<SentenceLanguage> sentenceList) {
        this.sentenceList = sentenceList;
        return this;
    }

    public List<SentenceLanguage> getSentenceList() {
        return sentenceList;
    }

    public DomainDTO getDomain() {
        return domain;
    }

    public PhraseOptionEditDTO setDomain(DomainDTO domain) {
        this.domain = domain;
        return this;
    }

    public VersionDTO getVersion() {
        return version;
    }

    public PhraseOptionEditDTO setVersion(VersionDTO version) {
        this.version = version;
        return this;
    }

    public PhraseLanguageDTO getPhrasesDE() {
        return phrasesDE;
    }

    public PhraseOptionEditDTO setPhrasesDE(PhraseLanguageDTO phrasesDE) {
        this.phrasesDE = phrasesDE;
        return this;
    }

    public PhraseLanguageDTO getPhrasesIT() {
        return phrasesIT;
    }

    public PhraseOptionEditDTO setPhrasesIT(PhraseLanguageDTO phrasesIT) {
        this.phrasesIT = phrasesIT;
        return this;
    }

    public PhraseLanguageDTO getPhrasesEN() {
        return phrasesEN;
    }

    public PhraseOptionEditDTO setPhrasesEN(PhraseLanguageDTO phrasesEN) {
        this.phrasesEN = phrasesEN;
        return this;
    }

    public PhraseLanguageDTO getPhrasesFR() {
        return phrasesFR;
    }

    public PhraseOptionEditDTO setPhrasesFR(PhraseLanguageDTO phrasesFR) {
        this.phrasesFR = phrasesFR;
        return this;
    }

    public PhraseLanguageDTO getPhrasesES() {
        return phrasesES;
    }

    public PhraseOptionEditDTO setPhrasesES(PhraseLanguageDTO phrasesES) {
        this.phrasesES = phrasesES;
        return this;
    }

    public PhraseLanguageDTO getPhrasesCA() {
        return phrasesCA;
    }

    public PhraseOptionEditDTO setPhrasesCA(PhraseLanguageDTO phrasesCA) {
        this.phrasesCA = phrasesCA;
        return this;
    }

    public PhraseLanguageDTO getPhrasesOC() {
        return phrasesOC;
    }

    public PhraseOptionEditDTO setPhrasesOC(PhraseLanguageDTO phrasesOC) {
        this.phrasesOC = phrasesOC;
        return this;
    }
}
