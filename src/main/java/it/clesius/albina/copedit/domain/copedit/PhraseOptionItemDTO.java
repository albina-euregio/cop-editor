package it.clesius.albina.copedit.domain.copedit;

import java.time.ZonedDateTime;

public class PhraseOptionItemDTO {

    Long phraseOptionItemId;
    Integer itemNo;
    Boolean deleted;
    Long phraseOptionId;
    String version;
    String basisVersion;
    ZonedDateTime datenew;
    ZonedDateTime datelast;
    PhraseDTO phrase;

    public PhraseDTO getPhrase() {
        return phrase;
    }

    public void setPhrase(PhraseDTO phrase) {
        this.phrase = phrase;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBasisVersion() {
        return basisVersion;
    }

    public void setBasisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
    }

    public Long getPhraseOptionItemId() {
        return phraseOptionItemId;
    }

    public void setPhraseOptionItemId(Long phraseOptionItemId) {
        this.phraseOptionItemId = phraseOptionItemId;
    }

    public Integer getItemNo() {
        return itemNo;
    }

    public void setItemNo(Integer itemNo) {
        this.itemNo = itemNo;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getPhraseOptionId() {
        return phraseOptionId;
    }

    public void setPhraseOptionId(Long phraseOptionId) {
        this.phraseOptionId = phraseOptionId;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }

    public PhraseOptionItemDTO phraseOptionItemId(Long phraseOptionItemId) {
        this.phraseOptionItemId = phraseOptionItemId;
        return this;
    }

    public PhraseOptionItemDTO itemNo(Integer itemNo) {
        this.itemNo = itemNo;
        return this;
    }

    public PhraseOptionItemDTO deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public PhraseOptionItemDTO phraseOptionId(Long phraseOptionId) {
        this.phraseOptionId = phraseOptionId;
        return this;
    }

    public PhraseOptionItemDTO datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public PhraseOptionItemDTO datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public PhraseOptionItemDTO version(String version) {
        this.version = version;
        return this;
    }

    public PhraseOptionItemDTO basisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
        return this;
    }

    public PhraseOptionItemDTO phrase(PhraseDTO phrase) {
        this.phrase = phrase;
        return this;
    }
}
