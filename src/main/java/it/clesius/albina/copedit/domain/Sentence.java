package it.clesius.albina.copedit.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Sentence.
 */
@Entity
@Table(name = "sentence")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sentence implements Serializable {

    private static final long serialVersionUID = 1L;

    public Sentence(Long id, String name, String header, Boolean deleted, Boolean jokerSentence) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
        this.jokerSentence = jokerSentence;
        this.header = header;
    }

    public Sentence() {
    }

    @Id
    @GeneratedValue(generator = "my_seq_sentence")
    @SequenceGenerator(name = "my_seq_sentence", sequenceName = "SEQ_SENTENCE_ID", allocationSize = 1)
    @Column(name = "sentence_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "header")
    private String header;

    @Column(name = "language")
    private String language;

    @Column(name = "structure")
    private String structure;

    @Column(name = "version")
    private String version;

    @Column(name = "basis_version")
    private String basisVersion;

    @Column(name = "remark")
    private String remark;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "joker_sentence")
    private Boolean jokerSentence;

    @Column(name = "domain_id")
    private Integer domainId;

    @Column(name = "datenew")
    private ZonedDateTime datenew;

    @Column(name = "datelast")
    private ZonedDateTime datelast;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Sentence name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public Sentence header(String header) {
        this.header = header;
        return this;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getLanguage() {
        return language;
    }

    public Sentence language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStructure() {
        return structure;
    }

    public Sentence structure(String structure) {
        this.structure = structure;
        return this;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public String getVersion() {
        return version;
    }

    public Sentence version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBasisVersion() {
        return basisVersion;
    }

    public Sentence basisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
        return this;
    }

    public void setBasisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
    }

    public String getRemark() {
        return remark;
    }

    public Sentence remark(String remark) {
        this.remark = remark;
        return this;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Sentence deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isJokerSentence() {
        return jokerSentence;
    }

    public Sentence jokerSentence(Boolean jokerSentence) {
        this.jokerSentence = jokerSentence;
        return this;
    }

    public void setJokerSentence(Boolean jokerSentence) {
        this.jokerSentence = jokerSentence;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public Sentence domainId(Integer domainId) {
        this.domainId = domainId;
        return this;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public Sentence datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public Sentence datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sentence sentence = (Sentence) o;
        if (sentence.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sentence.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sentence{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", header='" + getHeader() + "'" +
            ", language='" + getLanguage() + "'" +
            ", structure='" + getStructure() + "'" +
            ", version='" + getVersion() + "'" +
            ", basisVersion='" + getBasisVersion() + "'" +
            ", remark='" + getRemark() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", jokerSentence='" + isJokerSentence() + "'" +
            ", domainId=" + getDomainId() +
            ", datenew='" + getDatenew() + "'" +
            ", datelast='" + getDatelast() + "'" +
            "}";
    }
}
