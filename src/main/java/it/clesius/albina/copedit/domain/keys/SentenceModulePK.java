package it.clesius.albina.copedit.domain.keys;

import java.io.Serializable;

public class SentenceModulePK implements Serializable {

    private Integer sentenceId;
    private Integer phraseOptionId;

    public SentenceModulePK() {
    }

    public SentenceModulePK(Integer sentenceId, Integer phraseOptionId) {
        this.sentenceId = sentenceId;
        this.phraseOptionId = phraseOptionId;
    }

    public Integer getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(Integer sentenceId) {
        this.sentenceId = sentenceId;
    }

    public Integer getPhraseOptionId() {
        return phraseOptionId;
    }

    public void setPhraseOptionId(Integer phraseOptionId) {
        this.phraseOptionId = phraseOptionId;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
