package it.clesius.albina.copedit.domain.copedit;

import java.time.ZonedDateTime;

public class PhraseOptionDTO implements Cloneable {

    private Long id;
    private String name;
    private String language;
    private String header;
    private String version;
    private String basisVersion;
    private String remark;
    private Boolean deleted;
    private Integer domainId;
    private ZonedDateTime dateNew;
    private ZonedDateTime dateLast;
    private Integer moduleNo;
    private Integer itemNo;

    public Integer getItemNo() {
        return itemNo;
    }

    public void setItemNo(Integer itemNo) {
        this.itemNo = itemNo;
    }

    public Integer getModuleNo() {
        return moduleNo;
    }

    public void setModuleNo(Integer moduleNo) {
        this.moduleNo = moduleNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBasisVersion() {
        return basisVersion;
    }

    public void setBasisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public ZonedDateTime getDateNew() {
        return dateNew;
    }

    public void setDateNew(ZonedDateTime dateNew) {
        this.dateNew = dateNew;
    }

    public ZonedDateTime getDateLast() {
        return dateLast;
    }

    public void setDateLast(ZonedDateTime dateLast) {
        this.dateLast = dateLast;
    }


    public PhraseOptionDTO name(String name) {
        this.name = name;
        return this;
    }

    public PhraseOptionDTO language(String language) {
        this.language = language;
        return this;
    }

    public PhraseOptionDTO header(String header) {
        this.header = header;
        return this;
    }

    public PhraseOptionDTO version(String version) {
        this.version = version;
        return this;
    }

    public PhraseOptionDTO basisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
        return this;
    }

    public PhraseOptionDTO remark(String remark) {
        this.remark = remark;
        return this;
    }

    public PhraseOptionDTO deletd(Boolean deletd) {
        this.deleted = deletd;
        return this;
    }

    public PhraseOptionDTO domainId(Integer domainId) {
        this.domainId = domainId;
        return this;
    }

    public PhraseOptionDTO dateNew(ZonedDateTime dateNew) {
        this.dateNew = dateNew;
        return this;
    }

    public PhraseOptionDTO dateLast(ZonedDateTime dateLast) {
        this.dateLast = dateLast;
        return this;
    }


    public PhraseOptionDTO id(Long id) {
        this.id = id;
        return this;
    }

    public PhraseOptionDTO position(Integer position) {
        this.moduleNo = position;
        return this;
    }

    public PhraseOptionDTO deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public PhraseOptionDTO moduleNo(Integer moduleNo) {
        this.moduleNo = moduleNo;
        return this;
    }

    public PhraseOptionDTO itemNo(Integer itemNo) {
        this.itemNo = itemNo;
        return this;
    }

    @Override
    public Object clone() {
        PhraseOptionDTO clone = null;
        try {
            clone = (PhraseOptionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        return clone;
    }
}
