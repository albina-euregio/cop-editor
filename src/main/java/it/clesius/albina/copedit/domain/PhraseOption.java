package it.clesius.albina.copedit.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A PhraseOption.
 */
@Entity
@Table(name = "phrase_option")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PhraseOption implements Serializable {

    private static final long serialVersionUID = 1L;

    public PhraseOption(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public PhraseOption() {
    }

    public PhraseOption(Long id, String header, ZonedDateTime dateLast, Integer usedInSentence) {
        this.id = id;
        this.header = header;
    }

    @Id
    @GeneratedValue(generator = "my_phrase_option_sentence")
    @SequenceGenerator(name = "my_phrase_option_sentence", sequenceName = "SEQ_PHRASE_OPTION_ID", allocationSize = 1)
    @Column(name = "phrase_option_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "language")
    private String language;

    @Column(name = "header")
    private String header;

    @Column(name = "version")
    private String version;

    @Column(name = "basis_version")
    private String basisVersion;

    @Column(name = "remark")
    private String remark;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "domain_id")
    private Integer domainId;

    @Column(name = "datenew")
    private ZonedDateTime datenew;

    @Column(name = "datelast")
    private ZonedDateTime datelast;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public PhraseOption name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public PhraseOption language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getHeader() {
        return header;
    }

    public PhraseOption header(String header) {
        this.header = header;
        return this;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getVersion() {
        return version;
    }

    public PhraseOption version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBasisVersion() {
        return basisVersion;
    }

    public PhraseOption basisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
        return this;
    }

    public void setBasisVersion(String basisVersion) {
        this.basisVersion = basisVersion;
    }

    public String getRemark() {
        return remark;
    }

    public PhraseOption remark(String remark) {
        this.remark = remark;
        return this;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public PhraseOption deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public PhraseOption domainId(Integer domainId) {
        this.domainId = domainId;
        return this;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public PhraseOption datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public PhraseOption datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PhraseOption phraseOption = (PhraseOption) o;
        if (phraseOption.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phraseOption.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhraseOption{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", language='" + getLanguage() + "'" +
            ", header='" + getHeader() + "'" +
            ", version='" + getVersion() + "'" +
            ", basisVersion='" + getBasisVersion() + "'" +
            ", remark='" + getRemark() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", domainId=" + getDomainId() +
            ", datenew='" + getDatenew() + "'" +
            ", datelast='" + getDatelast() + "'" +
            "}";
    }
}
