package it.clesius.albina.copedit.domain.copedit.mappers;

import it.clesius.albina.copedit.domain.Domain;
import it.clesius.albina.copedit.domain.copedit.DomainDTO;
import org.springframework.stereotype.Service;

@Service
public class CopeditDomainMapper {

    public DomainDTO domainToDomainDTO(Domain d) {

        return new DomainDTO()
            .name(d.getName())
            .id(d.getId());
    }
}
