package it.clesius.albina.copedit.domain.copedit;

import java.time.ZonedDateTime;

public class PhraseOptionListItemDTO {

    Long id;
    String name;
    String header;
    ZonedDateTime datelast;
    Integer nrOfSentences;

    public PhraseOptionListItemDTO(Long id, String header, String name, ZonedDateTime datelast, Integer nrOfSentences) {
        this.id = id;
        this.name = name;
        this.header = header;
        this.datelast = datelast;
        this.nrOfSentences = nrOfSentences;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }


    public Integer getNrOfSentences() {
        return nrOfSentences;
    }

    public void setNrOfSentences(Integer nrOfSentences) {
        this.nrOfSentences = nrOfSentences;
    }

    public PhraseOptionListItemDTO id(Long id) {
        this.id = id;
        return this;
    }

    public PhraseOptionListItemDTO name(String name) {
        this.name = name;
        return this;
    }

    public PhraseOptionListItemDTO header(String header) {
        this.header = header;
        return this;
    }

    public PhraseOptionListItemDTO nrOfSentences(Integer nrOfSentences) {
        this.nrOfSentences = nrOfSentences;
        return this;
    }

    public PhraseOptionListItemDTO datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }
}
