package it.clesius.albina.copedit.domain.copedit;

public class VersionDTO {

    String version;
    String baseVersion;

    public VersionDTO(String version, String basisVersion) {
        this.version = version;
        this.baseVersion = basisVersion;
    }

    public VersionDTO() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBaseVersion() {
        return baseVersion;
    }

    public void setBaseVersion(String baseVersion) {
        this.baseVersion = baseVersion;
    }

    public VersionDTO version(String version) {
        this.version = version;
        return this;
    }

    public VersionDTO baseVersion(String baseVersion) {
        this.baseVersion = baseVersion;
        return this;
    }
}
