package it.clesius.albina.copedit.domain.copedit.mappers;

import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.domain.copedit.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static it.clesius.albina.copedit.config.Constants.*;

@Service
public class CopeditSentenceMapper {

    @Autowired
    CopeditCommonMapper copeditCommonMapper;

    public SentenceDTO sentenceDTOMapper(List<Sentence> sentence, Map<String, List<PhraseOptionDTO>> phraseOptionsMap) {
        SentenceDTO bean = new SentenceDTO();
        bean.setDomain(new DomainDTO()
                .id(sentence.get(0).getDomainId().longValue())
                .name(copeditCommonMapper.getDomainNameById(sentence.get(0).getDomainId()))
            //.datenew(sentence.get(0).getDatenew())
            //.datelast(sentence.get(0).getDatelast())
        );
        bean.setDeleted(sentence.get(0).isDeleted());
        bean.setJokerSentence(sentence.get(0).isJokerSentence());
        bean.setVersion(new VersionDTO()
            .version(sentence.get(0).getVersion())
            .baseVersion(sentence.get(0).getBasisVersion())
        );
        for (Sentence s : sentence) {

            SentenceLanguage languageSentence = new SentenceLanguage();
            languageSentence.setName(s.getName());
            languageSentence.setHeader(s.getHeader());
            languageSentence.setLanguage(s.getLanguage());
            languageSentence.setStructure(s.getStructure());
            languageSentence.setRemark(s.getRemark());
            languageSentence.id(s.getId());
            languageSentence.updatedDate(s.getDatelast());
            languageSentence.createdDate(s.getDatenew());

            /* add language*/
            switch (s.getLanguage()) {
                case LANGUAGE_DE:
                    bean.setSentencesDE(languageSentence);
                    bean.getSentencesDE().phraseOptions(phraseOptionsMap.get(LANGUAGE_DE));
                    break;
                case LANGUAGE_IT:
                    bean.setSentencesIT(languageSentence);
                    bean.getSentencesIT().phraseOptions(phraseOptionsMap.get(LANGUAGE_IT));
                    break;
                case LANGUAGE_EN:
                    bean.setSentencesEN(languageSentence);
                    bean.getSentencesEN().phraseOptions(phraseOptionsMap.get(LANGUAGE_EN));
                    break;
                case LANGUAGE_FR:
                    bean.setSentencesFR(languageSentence);
                    bean.getSentencesFR().phraseOptions(phraseOptionsMap.get(LANGUAGE_FR));
                    break;
                case LANGUAGE_ES:
                    bean.setSentencesES(languageSentence);
                    bean.getSentencesES().phraseOptions(phraseOptionsMap.get(LANGUAGE_ES));
                    break;
                case LANGUAGE_CA:
                    bean.setSentencesCA(languageSentence);
                    bean.getSentencesCA().phraseOptions(phraseOptionsMap.get(LANGUAGE_CA));
                    break;
                case LANGUAGE_OC:
                    bean.setSentencesOC(languageSentence);
                    bean.getSentencesOC().phraseOptions(phraseOptionsMap.get(LANGUAGE_OC));
                    break;
            }
        }
        reorderPhraseOptions(bean.getSentencesDE());
        reorderPhraseOptions(bean.getSentencesIT());
        reorderPhraseOptions(bean.getSentencesFR());
        reorderPhraseOptions(bean.getSentencesEN());
        reorderPhraseOptions(bean.getSentencesES());
        reorderPhraseOptions(bean.getSentencesCA());
        reorderPhraseOptions(bean.getSentencesOC());

        return bean;
    }

    public List<Sentence> sentenceDTOToSentence(SentenceDTO dto) {

        List<Sentence> sentences = new ArrayList<>();
        sentences.add(sentenceLanguageToSentence(dto.getSentencesDE(), dto));
        sentences.add(sentenceLanguageToSentence(dto.getSentencesIT(), dto));
        sentences.add(sentenceLanguageToSentence(dto.getSentencesEN(), dto));
        sentences.add(sentenceLanguageToSentence(dto.getSentencesFR(), dto));
        sentences.add(sentenceLanguageToSentence(dto.getSentencesES(), dto));
        sentences.add(sentenceLanguageToSentence(dto.getSentencesCA(), dto));
        sentences.add(sentenceLanguageToSentence(dto.getSentencesOC(), dto));
        return sentences;
    }

    public Sentence sentenceLanguageToSentence(SentenceLanguage sl, SentenceDTO dto) {
        Sentence s = new Sentence();
        s.setVersion(dto.getVersion().getVersion());
        s.setBasisVersion(dto.getVersion().getBaseVersion());
        s.setDomainId(dto.getDomain().getId().intValue());
        s.setDeleted(dto.getDeleted());
        s.setJokerSentence(Optional.ofNullable(dto.getJokerSentence()).orElse(false));
        s.header(sl.getHeader());
        s.name(sl.getName());
        s.structure(sl.getStructure());
        s.language(sl.getLanguage());
        s.setId(sl.getId());
        s.datenew(sl.getCreatedDate());
        s.datelast(sl.getUpdatedDate());
        s.remark(sl.getRemark());
        return s;
    }

    /**
     * Reorder the list of the phraseOptions based on the sentence structure
     *
     * @param sl sentence to reorder
     */
    public void reorderPhraseOptions(SentenceLanguage sl) {

        List<PhraseOptionDTO> reorderedPhraseOptions = new ArrayList<>();
        List<String> splittedElements = Arrays.asList(sl.getStructure().split(","));
        splittedElements.forEach(element -> {

            List<String> el = Arrays.asList(element.split("\\."));
            Integer index = Integer.parseInt(el.get(0));
            PhraseOptionDTO po = sl.getPhraseOptions().get(index - 1);

            PhraseOptionDTO clone = (PhraseOptionDTO) po.clone();
            Integer itemNo = Integer.parseInt(el.get(1));
            if (itemNo == 2) {
                clone.setItemNo(2);
            } else {
                clone.setItemNo(1);
            }
            reorderedPhraseOptions.add(clone);

        });
        sl.setPhraseOptions(reorderedPhraseOptions);

    }

}
