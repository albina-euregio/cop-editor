package it.clesius.albina.copedit.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Domain.
 */
@Entity
@Table(name = "domain")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Domain implements Serializable {

    private static final long serialVersionUID = 1L;

    public Domain() {
    }

    public Domain(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @Column(name = "domain_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "datenew")
    private ZonedDateTime datenew;

    @Column(name = "datelast")
    private ZonedDateTime datelast;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Domain name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public Domain datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public Domain datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Domain domain = (Domain) o;
        if (domain.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), domain.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Domain{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", datenew='" + getDatenew() + "'" +
            ", datelast='" + getDatelast() + "'" +
            "}";
    }
}
