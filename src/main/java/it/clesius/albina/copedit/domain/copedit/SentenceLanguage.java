package it.clesius.albina.copedit.domain.copedit;

import java.time.ZonedDateTime;
import java.util.List;

public class SentenceLanguage {

    Long id;
    String name;
    String header;
    String language;
    ZonedDateTime updatedDate;
    ZonedDateTime createdDate;
    String structure;
    String remark;
    private List<PhraseOptionDTO> phraseOptions;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    
    public List<PhraseOptionDTO> getPhraseOptions() {
        return phraseOptions;
    }

    public void setPhraseOptions(List<PhraseOptionDTO> phraseOptions) {
        this.phraseOptions = phraseOptions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ZonedDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public SentenceLanguage name(String name) {
        this.name = name;
        return this;
    }

    public SentenceLanguage header(String header) {
        this.header = header;
        return this;
    }

    public SentenceLanguage language(String language) {
        this.language = language;
        return this;
    }

    public SentenceLanguage updatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public SentenceLanguage structure(String structure) {
        this.structure = structure;
        return this;
    }


    public SentenceLanguage id(Long id) {
        this.id = id;
        return this;
    }


    public SentenceLanguage phraseOptions(List<PhraseOptionDTO> phraseOptions) {
        this.phraseOptions = phraseOptions;
        return this;
    }

    public SentenceLanguage createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public SentenceLanguage remark(String remark) {
        this.remark = remark;
        return this;
    }
}
