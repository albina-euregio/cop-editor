package it.clesius.albina.copedit.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Phrase.
 */
@Entity
@Table(name = "phrase")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Phrase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "my_seq_phrase")
    @SequenceGenerator(name = "my_seq_phrase", sequenceName = "SEQ_PHRASE_ID", allocationSize = 1)
    @Column(name = "phrase_id")
    private Long id;

    @Column(name = "value")
    private String value;

    @Column(name = "space_before")
    private Boolean spaceBefore;

    @Column(name = "space_after")
    private Boolean spaceAfter;

    @Column(name = "remove_punctuation_before")
    private Boolean removePunctuationBefore;

    @Column(name = "item_part_no")
    private Integer itemPartNo;

    @Column(name = "incorrect")
    private Boolean incorrect;

    @Column(name = "phrase_option_item_id")
    private Integer phraseOptionItemId;

    @Column(name = "datelast")
    private ZonedDateTime datelast;

    @Column(name = "datenew")
    private ZonedDateTime datenew;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public Phrase value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean isSpaceBefore() {
        return spaceBefore;
    }

    public Phrase spaceBefore(Boolean spaceBefore) {
        this.spaceBefore = spaceBefore;
        return this;
    }

    public void setSpaceBefore(Boolean spaceBefore) {
        this.spaceBefore = spaceBefore;
    }

    public Boolean isSpaceAfter() {
        return spaceAfter;
    }

    public Phrase spaceAfter(Boolean spaceAfter) {
        this.spaceAfter = spaceAfter;
        return this;
    }

    public void setSpaceAfter(Boolean spaceAfter) {
        this.spaceAfter = spaceAfter;
    }

    public Boolean isRemovePunctuationBefore() {
        return removePunctuationBefore;
    }

    public Phrase removePunctuationBefore(Boolean removePunctuationBefore) {
        this.removePunctuationBefore = removePunctuationBefore;
        return this;
    }

    public void setRemovePunctuationBefore(Boolean removePunctuationBefore) {
        this.removePunctuationBefore = removePunctuationBefore;
    }

    public Integer getItemPartNo() {
        return itemPartNo;
    }

    public Phrase itemNo(Integer itemPartNo) {
        this.itemPartNo = itemPartNo;
        return this;
    }

    public void setItemPartNo(Integer itemPartNo) {
        this.itemPartNo = itemPartNo;
    }

    public Boolean isIncorrect() {
        return incorrect;
    }

    public Phrase incorrect(Boolean incorrect) {
        this.incorrect = incorrect;
        return this;
    }

    public void setIncorrect(Boolean incorrect) {
        this.incorrect = incorrect;
    }

    public Integer getPhraseOptionItemId() {
        return phraseOptionItemId;
    }

    public Phrase phraseOptionItemId(Integer phraseOptionItemId) {
        this.phraseOptionItemId = phraseOptionItemId;
        return this;
    }

    public void setPhraseOptionItemId(Integer phraseOptionItemId) {
        this.phraseOptionItemId = phraseOptionItemId;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public Phrase datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public Phrase datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Phrase phrase = (Phrase) o;
        if (phrase.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phrase.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Phrase{" +
            "id=" + getId() +
            ", value='" + getValue() + "'" +
            ", spaceBefore='" + isSpaceBefore() + "'" +
            ", spaceAfter='" + isSpaceAfter() + "'" +
            ", removePunctuationBefore='" + isRemovePunctuationBefore() + "'" +
            ", itemPartNo=" + getItemPartNo() +
            ", incorrect='" + isIncorrect() + "'" +
            ", phraseOptionItemId=" + getPhraseOptionItemId() +
            ", datelast='" + getDatelast() + "'" +
            ", datenew='" + getDatenew() + "'" +
            "}";
    }
}
