package it.clesius.albina.copedit.domain.copedit.mappers;

import org.springframework.stereotype.Service;

@Service
public class CopeditCommonMapper {

    /**
     * Returns the domain name based on the domain id
     *
     * @param id domain id
     * @return corresponding domain name
     */
    public String getDomainNameById(Integer id) {

        String res;
        switch (id.intValue()) {

            case -10:
                res = "INTEGRATION_TEST";
                break;
            case 1:
                res = "PRODUCTION";
                break;
            case 2:
                res = "STAGING";
                break;
            default:
                res = "";
                break;
        }
        return res;
    }
}
