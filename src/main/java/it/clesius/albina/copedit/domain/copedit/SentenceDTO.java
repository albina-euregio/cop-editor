package it.clesius.albina.copedit.domain.copedit;

public class SentenceDTO {

    DomainDTO domain;
    VersionDTO version;
    Boolean deleted;
    Boolean jokerSentence;
    SentenceLanguage sentencesDE;
    SentenceLanguage sentencesIT;
    SentenceLanguage sentencesFR;
    SentenceLanguage sentencesEN;
    SentenceLanguage sentencesES;
    SentenceLanguage sentencesCA;
    SentenceLanguage sentencesOC;

    public DomainDTO getDomain() {
        return domain;
    }

    public SentenceDTO setDomain(DomainDTO domain) {
        this.domain = domain;
        return this;
    }

    public VersionDTO getVersion() {
        return version;
    }

    public SentenceDTO setVersion(VersionDTO version) {
        this.version = version;
        return this;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public SentenceDTO setDeleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public Boolean getJokerSentence() {
        return jokerSentence;
    }

    public SentenceDTO setJokerSentence(Boolean jokerSentence) {
        this.jokerSentence = jokerSentence;
        return this;
    }

    public SentenceLanguage getSentencesDE() {
        return sentencesDE;
    }

    public SentenceDTO setSentencesDE(SentenceLanguage sentencesDE) {
        this.sentencesDE = sentencesDE;
        return this;
    }

    public SentenceLanguage getSentencesIT() {
        return sentencesIT;
    }

    public SentenceDTO setSentencesIT(SentenceLanguage sentencesIT) {
        this.sentencesIT = sentencesIT;
        return this;
    }

    public SentenceLanguage getSentencesFR() {
        return sentencesFR;
    }

    public SentenceDTO setSentencesFR(SentenceLanguage sentencesFR) {
        this.sentencesFR = sentencesFR;
        return this;
    }

    public SentenceLanguage getSentencesEN() {
        return sentencesEN;
    }

    public SentenceDTO setSentencesEN(SentenceLanguage sentencesEN) {
        this.sentencesEN = sentencesEN;
        return this;
    }

    public SentenceLanguage getSentencesES() {
        return sentencesES;
    }

    public SentenceDTO setSentencesES(SentenceLanguage sentencesES) {
        this.sentencesES = sentencesES;
        return this;
    }

    public SentenceLanguage getSentencesCA() {
        return sentencesCA;
    }

    public SentenceDTO setSentencesCA(SentenceLanguage sentencesCA) {
        this.sentencesCA = sentencesCA;
        return this;
    }

    public SentenceLanguage getSentencesOC() {
        return sentencesOC;
    }

    public SentenceDTO setSentencesOC(SentenceLanguage sentencesOC) {
        this.sentencesOC = sentencesOC;
        return this;
    }
}
