package it.clesius.albina.copedit.service.impl;

import it.clesius.albina.copedit.service.PhraseService;
import it.clesius.albina.copedit.domain.Phrase;
import it.clesius.albina.copedit.repository.PhraseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Phrase.
 */
@Service
@Transactional
public class PhraseServiceImpl implements PhraseService {

    private final Logger log = LoggerFactory.getLogger(PhraseServiceImpl.class);

    private final PhraseRepository phraseRepository;

    public PhraseServiceImpl(PhraseRepository phraseRepository) {
        this.phraseRepository = phraseRepository;
    }

    /**
     * Save a phrase.
     *
     * @param phrase the entity to save
     * @return the persisted entity
     */
    @Override
    public Phrase save(Phrase phrase) {
        log.debug("Request to save Phrase : {}", phrase);        return phraseRepository.save(phrase);
    }

    /**
     * Get all the phrases.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Phrase> findAll(Pageable pageable) {
        log.debug("Request to get all Phrases");
        return phraseRepository.findAll(pageable);
    }


    /**
     * Get one phrase by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Phrase> findOne(Long id) {
        log.debug("Request to get Phrase : {}", id);
        return phraseRepository.findById(id);
    }

    /**
     * Delete the phrase by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Phrase : {}", id);
        phraseRepository.deleteById(id);
    }
}
