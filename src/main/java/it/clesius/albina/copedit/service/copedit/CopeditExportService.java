package it.clesius.albina.copedit.service.copedit;

import com.jcraft.jsch.SftpException;
import it.clesius.albina.copedit.config.ApplicationProperties;
import it.clesius.albina.copedit.config.Constants;
import it.clesius.albina.copedit.domain.Domain;
import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditCommonMapper;
import it.clesius.albina.copedit.service.DomainService;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class CopeditExportService {

    private final Logger log = LoggerFactory.getLogger(CopeditExportService.class);

    @Autowired
    EntityManager entityManager;
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    CopeditCommonMapper copeditCommonMapper;

    private static final String FILE_ENCODING = "UTF-8";
    private static final String RANGES_DIR = "Ranges";
    private static final String SENTENCES_DIR = "Sentences";
    private static final String FILE_PATTERN = "S%s.txt";
    private static final String RANGES_FILE_PATTERN = "%s.txt";

    /* sentence file placeholders*/
    private static final String ST_HEADER = "ST_Header";
    private static final String ST_CURLYNAME = "ST_CurlyName";
    private static final String RS_CURLYNAME = "RS_CurlyName";
    private static final String PA_POS = "PA_Pos";
    private static final String PA_POSGERMAN = "PA_PosGerman";

    /* ranges file placeholder */
    private static final String LINE = "Line";
    private static final String EMPTY_VALUE = "[Empty]";
    private static final String RS_HEADER = "RS_Header";

    private static final String SPACE_BEFORE_PREFIX = "(-)";
    private static final String REMOVE_PUNCTUATION_PREFIX = "(--)";
    private static final String BEGIN_REGION = "Begin: %s";
    private static final String END_REGION = "End: %s";

    private final static byte[] BOM = new byte[]{(byte) 0xFF, (byte) 0xFE};

    private final NumberFormat NAME_FORMATTER = new DecimalFormat("000");
    private final NumberFormat TWO_DIGITS_FORMATTER = new DecimalFormat("00");


    @Autowired
    private DomainService domainService;

    /**
     * Exports the given domain in a zip file contaning all sentences and phrases
     *
     * @param domainId domain to export
     * @return zip file contaning a zip file for each language
     */
    public byte[] exportDomain(Integer domainId) throws IOException {

        ByteArrayOutputStream foutDE = new ByteArrayOutputStream();
        ZipOutputStream zoutDE = new ZipOutputStream(foutDE);
        ByteArrayOutputStream foutIT = new ByteArrayOutputStream();
        ZipOutputStream zoutIT = new ZipOutputStream(foutIT);
        ByteArrayOutputStream foutEN = new ByteArrayOutputStream();
        ZipOutputStream zoutEN = new ZipOutputStream(foutEN);
        ByteArrayOutputStream foutFR = new ByteArrayOutputStream();
        ZipOutputStream zoutFR = new ZipOutputStream(foutFR);
        ByteArrayOutputStream foutES = new ByteArrayOutputStream();
        ZipOutputStream zoutES = new ZipOutputStream(foutES);
        ByteArrayOutputStream foutCA = new ByteArrayOutputStream();
        ZipOutputStream zoutCA = new ZipOutputStream(foutCA);
        ByteArrayOutputStream foutOC = new ByteArrayOutputStream();
        ZipOutputStream zoutOC = new ZipOutputStream(foutOC);

        /*SENTENCES*/
        List<Sentence> sentenceList = entityManager.createNativeQuery(String.format("SELECT * FROM sentence WHERE language='de' AND deleted = false AND domain_id = %d ORDER BY sentence_id", domainId), Sentence.class)
            .getResultList();

        log.info("Exporting SENTENCES...");
        for (Sentence sentence : sentenceList) {

            zoutDE.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutIT.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutEN.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutFR.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutES.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutCA.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutOC.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));

            String sentenceName = sentence.getName();

            StringBuilder sb = new StringBuilder();
            sb.append("SELECT sentence.sentence_id, sentence.name, sentence.header, sentence.language, structure, sentence_module.module_no, phrase_option.name as po_name\n");
            sb.append("FROM sentence\n");
            sb.append("INNER JOIN sentence_module ON sentence.sentence_id = sentence_module.sentence_id\n");
            sb.append("INNER JOIN phrase_option ON sentence_module.phrase_option_id = phrase_option.phrase_option_id\n");
            sb.append(String.format("WHERE sentence.name = '%s'\n", sentenceName));
            sb.append(String.format("AND sentence.domain_id = '%d'\n", domainId));
            sb.append("ORDER BY sentence.language, sentence_module.module_no");

            List<Tuple> result = entityManager.createNativeQuery(sb.toString(), Tuple.class).getResultList();
            List<Tuple> sentenceDE = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_DE)).collect(Collectors.toList());
            String structureDE = (String) sentenceDE.get(0).get("structure");
            List<Tuple> sentenceEN = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_EN)).collect(Collectors.toList());
            List<Tuple> sentenceFR = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_FR)).collect(Collectors.toList());
            List<Tuple> sentenceIT = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_IT)).collect(Collectors.toList());
            List<Tuple> sentenceES = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_ES)).collect(Collectors.toList());
            List<Tuple> sentenceCA = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_CA)).collect(Collectors.toList());
            List<Tuple> sentenceOC = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_OC)).collect(Collectors.toList());
            List<String> fileContentDE = writeContentToSentenceFile(sentenceDE, structureDE);
            writeFileContent(fileContentDE, zoutDE);
            writeFileContent(writeContentToSentenceFile(sentenceIT, structureDE), zoutIT);
            writeFileContent(writeContentToSentenceFile(sentenceEN, structureDE), zoutEN);
            writeFileContent(writeContentToSentenceFile(sentenceFR, structureDE), zoutFR);
            writeFileContent(writeContentToSentenceFile(sentenceES, structureDE), zoutES);
            writeFileContent(writeContentToSentenceFile(sentenceCA, structureDE), zoutCA);
            writeFileContent(writeContentToSentenceFile(sentenceOC, structureDE), zoutOC);

        }


        log.info("Exporting RANGES...");
        /* RANGES */
        List<PhraseOption> phraseOptions = entityManager.createNativeQuery(
            String.format("SELECT * FROM phrase_option WHERE language='de' AND deleted = false AND domain_id = %d ORDER BY phrase_option_id", domainId)
            , PhraseOption.class)
            .getResultList();

        for (PhraseOption phraseOption : phraseOptions) {

            String phraseOptionHeader = phraseOption.getName();

            StringBuilder sb = new StringBuilder("SELECT phrase_option.header, phrase_option.name, phrase_option.language, phrase_option.domain_id, phrase_option.deleted, phrase.value, phrase_option_item.item_no, phrase.item_part_no,\n" +
                " phrase.space_before, phrase.space_after, phrase.remove_punctuation_before, region.region\n");
            sb.append("FROM phrase_option\n");
            sb.append("INNER JOIN phrase_option_item ON phrase_option.phrase_option_id = phrase_option_item.phrase_option_id\n");
            sb.append("INNER JOIN phrase ON phrase_option_item.phrase_option_item_id = phrase.phrase_option_item_id\n");
            sb.append("LEFT JOIN region_phrase ON phrase.phrase_id = region_phrase.phrase_id\n");
            sb.append("LEFT JOIN region  ON region_phrase.region_id = region.region_id\n");
            sb.append(String.format("WHERE phrase_option.name = '%s'\n", phraseOptionHeader));
            sb.append(String.format("AND phrase_option.domain_id = '%d'\n", domainId));
            sb.append("ORDER BY phrase_option.language, phrase_option_item.item_no");

            List<Tuple> results = entityManager.createNativeQuery(sb.toString(), Tuple.class).getResultList();
            List<Tuple> phraseOptionsDE = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_DE)).collect(Collectors.toList());
            List<Tuple> phraseOptionsEN = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_EN)).collect(Collectors.toList());
            List<Tuple> phraseOptionsFR = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_FR)).collect(Collectors.toList());
            List<Tuple> phraseOptionsIT = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_IT)).collect(Collectors.toList());
            List<Tuple> phraseOptionsES = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_ES)).collect(Collectors.toList());
            List<Tuple> phraseOptionsCA = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_CA)).collect(Collectors.toList());
            List<Tuple> phraseOptionsOC = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_OC)).collect(Collectors.toList());
            writeContentToRangesFile(zoutDE, phraseOptionsDE);
            writeContentToRangesFile(zoutEN, phraseOptionsEN);
            writeContentToRangesFile(zoutFR, phraseOptionsFR);
            writeContentToRangesFile(zoutIT, phraseOptionsIT);
            writeContentToRangesFile(zoutES, phraseOptionsES);
            writeContentToRangesFile(zoutCA, phraseOptionsCA);
            writeContentToRangesFile(zoutOC, phraseOptionsOC);

        }

        /* close streams */
        foutDE.close();
        foutEN.close();
        foutIT.close();
        foutFR.close();
        foutES.close();
        foutCA.close();
        foutOC.close();
        zoutDE.close();
        zoutEN.close();
        zoutIT.close();
        zoutFR.close();
        zoutES.close();
        zoutCA.close();
        zoutOC.close();

        log.info("Preparing final zip file ...");
        ByteArrayOutputStream zipBos = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(zipBos);

        // de.zip
        zip.putNextEntry(new ZipEntry("de.zip"));
        zip.write(foutDE.toByteArray());
        zip.closeEntry();
        // it.zip
        zip.putNextEntry(new ZipEntry("it.zip"));
        zip.write(foutIT.toByteArray());
        zip.closeEntry();
        // en.zip
        zip.putNextEntry(new ZipEntry("en.zip"));
        zip.write(foutEN.toByteArray());
        zip.closeEntry();
        // fr.zip
        zip.putNextEntry(new ZipEntry("fr.zip"));
        zip.write(foutFR.toByteArray());
        zip.closeEntry();
        // es.zip
        zip.putNextEntry(new ZipEntry("es.zip"));
        zip.write(foutES.toByteArray());
        zip.closeEntry();
        // ca.zip
        zip.putNextEntry(new ZipEntry("ca.zip"));
        zip.write(foutCA.toByteArray());
        zip.closeEntry();
        // oc.zip
        zip.putNextEntry(new ZipEntry("oc.zip"));
        zip.write(foutOC.toByteArray());
        zip.closeEntry();
        zip.close();

        // just for test
        // FileUtils.writeByteArrayToFile(new File(applicationProperties.getExportProperties().getTempFolder() + "/export.zip"), zipBos.toByteArray());
        zipBos.close();
        return zipBos.toByteArray();

    }

    /**
     * Exports the given domain in a zip file contaning all sentences and phrases
     *
     * @param domainId domain to export
     * @return zip file contaning a zip file for each language
     */
    public byte[] exportDomainToGit(Integer domainId) throws IOException {
        File srcFileDE = new File(applicationProperties.getExportProperties().getTempFolder() + "/de.zip");
        File srcFileIT = new File(applicationProperties.getExportProperties().getTempFolder() + "/it.zip");
        File srcFileEN = new File(applicationProperties.getExportProperties().getTempFolder() + "/en.zip");
        File srcFileFR = new File(applicationProperties.getExportProperties().getTempFolder() + "/fr.zip");

        FileOutputStream fisDE = new FileOutputStream(srcFileDE);
        ZipOutputStream zoutDE = new ZipOutputStream(fisDE);

        FileOutputStream fisIT = new FileOutputStream(srcFileIT);
        ZipOutputStream zoutIT = new ZipOutputStream(fisIT);

        FileOutputStream fisEN = new FileOutputStream(srcFileEN);
        ZipOutputStream zoutEN = new ZipOutputStream(fisEN);

        FileOutputStream fisFR = new FileOutputStream(srcFileFR);
        ZipOutputStream zoutFR = new ZipOutputStream(fisFR);

        /*SENTENCES*/
        List<Sentence> sentenceList = entityManager.createNativeQuery(String.format("SELECT * FROM sentence WHERE language='de' AND deleted = false AND domain_id = %d ORDER BY sentence_id", domainId), Sentence.class)
            .getResultList();

        log.info("Exporting SENTENCES...");
        for (Sentence sentence : sentenceList) {

            zoutDE.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutIT.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutEN.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));
            zoutFR.putNextEntry(new ZipEntry(SENTENCES_DIR + "/" + String.format(FILE_PATTERN, NAME_FORMATTER.format(sentence.getId()))));

            String sentenceName = sentence.getName();

            StringBuilder sb = new StringBuilder();
            sb.append("SELECT sentence.sentence_id, sentence.name, sentence.header, sentence.language, structure, sentence_module.module_no, phrase_option.name as po_name\n");
            sb.append("FROM sentence\n");
            sb.append("INNER JOIN sentence_module ON sentence.sentence_id = sentence_module.sentence_id\n");
            sb.append("INNER JOIN phrase_option ON sentence_module.phrase_option_id = phrase_option.phrase_option_id\n");
            sb.append(String.format("WHERE sentence.name = '%s'\n", sentenceName));
            sb.append(String.format("AND sentence.domain_id = '%d'\n", domainId));
            sb.append("ORDER BY sentence.language, sentence_module.module_no");

            List<Tuple> result = entityManager.createNativeQuery(sb.toString(), Tuple.class).getResultList();
            List<Tuple> sentenceDE = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_DE)).collect(Collectors.toList());
            String structureDE = (String) sentenceDE.get(0).get("structure");
            List<Tuple> sentenceEN = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_EN)).collect(Collectors.toList());
            List<Tuple> sentenceFR = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_FR)).collect(Collectors.toList());
            List<Tuple> sentenceIT = result.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_IT)).collect(Collectors.toList());

            List<String> fileContentDE = writeContentToSentenceFile(sentenceDE, structureDE);
            writeFileContent(fileContentDE, zoutDE);
            writeFileContent(writeContentToSentenceFile(sentenceIT, structureDE), zoutIT);
            writeFileContent(writeContentToSentenceFile(sentenceEN, structureDE), zoutEN);
            writeFileContent(writeContentToSentenceFile(sentenceFR, structureDE), zoutFR);

        }


        log.info("Exporting RANGES...");
        /* RANGES */
        List<PhraseOption> phraseOptions = entityManager.createNativeQuery(
            String.format("SELECT * FROM phrase_option WHERE language='de' AND deleted = false AND domain_id = %d ORDER BY phrase_option_id", domainId)
            , PhraseOption.class)
            .getResultList();

        for (PhraseOption phraseOption : phraseOptions) {

            String phraseOptionHeader = phraseOption.getName();

            StringBuilder sb = new StringBuilder("SELECT phrase_option.header, phrase_option.name, phrase_option.language, phrase_option.domain_id, phrase_option.deleted, phrase.value, phrase_option_item.item_no, phrase.item_part_no,\n" +
                " phrase.space_before, phrase.space_after, phrase.remove_punctuation_before, region.region\n");
            sb.append("FROM phrase_option\n");
            sb.append("INNER JOIN phrase_option_item ON phrase_option.phrase_option_id = phrase_option_item.phrase_option_id\n");
            sb.append("INNER JOIN phrase ON phrase_option_item.phrase_option_item_id = phrase.phrase_option_item_id\n");
            sb.append("LEFT JOIN region_phrase ON phrase.phrase_id = region_phrase.phrase_id\n");
            sb.append("LEFT JOIN region  ON region_phrase.region_id = region.region_id\n");
            sb.append(String.format("WHERE phrase_option.name = '%s'\n", phraseOptionHeader));
            sb.append(String.format("AND phrase_option.domain_id = '%d'\n", domainId));
            sb.append("ORDER BY phrase_option.language, phrase_option_item.item_no");

            List<Tuple> results = entityManager.createNativeQuery(sb.toString(), Tuple.class).getResultList();
            List<Tuple> phraseOptionsDE = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_DE)).collect(Collectors.toList());
            List<Tuple> phraseOptionsEN = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_EN)).collect(Collectors.toList());
            List<Tuple> phraseOptionsFR = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_FR)).collect(Collectors.toList());
            List<Tuple> phraseOptionsIT = results.stream().filter(x -> x.get("language").equals(Constants.LANGUAGE_IT)).collect(Collectors.toList());
            writeContentToRangesFile(zoutDE, phraseOptionsDE);
            writeContentToRangesFile(zoutEN, phraseOptionsEN);
            writeContentToRangesFile(zoutFR, phraseOptionsFR);
            writeContentToRangesFile(zoutIT, phraseOptionsIT);

        }

        /* close streams */
        zoutDE.close();
        zoutEN.close();
        zoutIT.close();
        zoutFR.close();

        // FileUtils.writeByteArrayToFile(new File(applicationProperties.getExportProperties().getTempFolder() + "/export.zip"), zipBos.toByteArray());

        return null;

    }


    public byte[] exportDB() throws Exception {

        CopBackSshEngine ssh = new CopBackSshEngine();
        String backupDB;
        try {
            ssh.openConnection();

            ssh.executeRemoteCommandAndWaitResponce("cd /home/", 60000);
            ssh.executeRemoteCommandAndWaitResponce("/home/makebackup.sh", 60000);
            backupDB = ssh.getFileStr("/home/cop.bak");

            /* String path = "/mnt/export";
            FileOutputStream out = new FileOutputStream(path);
            out = new FileOutputStream("cop.bak");
            out.write(backupDB.getBytes());
            out.close();
            */

        } finally {
            try {
                ssh.disconnect();
            } catch (Exception e) {
            }
        }
        return backupDB.getBytes() ;
    }

    /**
     * Writes the lines in the stream
     *
     * @param lines line to write
     * @param zout  strem to write on
     */
    public void writeFileContent(List<String> lines, ZipOutputStream zout) {

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(bos, StandardCharsets.UTF_8));
            // zout.write(BOM);
            for (String re : lines) {
                writer.write(re);
                writer.newLine();
            }
            writer.close();
            zout.write(bos.toByteArray(), 0, bos.toByteArray().length);
            bos.close();
            zout.closeEntry();
        } catch (IOException e) {
            log.error("Error in writeFileContent", e);
        }
    }

    /**
     * Writes the content to the given sentence file
     *
     * @param data        data to write
     * @param structureDE structure value in German
     */
    public List<String> writeContentToSentenceFile(List<Tuple> data, String structureDE) {

        String header = data.get(0).get("header", String.class);
        String curlyName = data.get(0).get("name", String.class);
        String structure = data.get(0).get("structure", String.class);

        List<String> lines = new ArrayList<>();
        lines.add(ST_HEADER + ": " + header);
        lines.add(ST_CURLYNAME + ": " + curlyName.replace("{", "").replace("}", ""));
        /* order of lines is given by structure*/
        List<String> elements = Arrays.asList(structure.split(","));
        for (int i = 0; i < elements.size(); i++) {

            String element = elements.get(i);
            Integer moduleNO = Integer.valueOf(element.substring(0, element.indexOf('.')));
            Boolean isException = element.endsWith(".2");
            Tuple correspondingSentence = data.stream().filter(x -> ((BigInteger) x.get("module_no")).intValue() == moduleNO.intValue()).findFirst().get();

            String phraseOption = correspondingSentence.get("po_name", String.class);
            BigInteger psPos = BigInteger.valueOf(i + 1);
            BigInteger moduleNo = getPosGerman(structureDE, (BigInteger) correspondingSentence.get("module_no"));

            lines.add(String.format(PA_POS + ": %d", psPos));
            lines.add(String.format(PA_POSGERMAN + ": %d", isException ? 0 : moduleNo));
            lines.add(String.format(RS_CURLYNAME + ": %s", isException ? phraseOption + "_NO" : phraseOption));
        }

        lines.add(""); // just for comparison with existing files
        return lines;

    }

    /**
     * Creates the file and writes content into the stream. If any exception exists, creates file ending with _NO and
     * writes content
     *
     * @param zout stream to write to
     * @param data data to elaborate and write
     */
    public void writeContentToRangesFile(ZipOutputStream zout, List<Tuple> data) {

        try {
            String header = data.get(0).get("header", String.class);
            String curlyName = data.get(0).get("name", String.class);
            String language = data.get(0).get("language", String.class);

            /* create the file */
            zout.putNextEntry(new ZipEntry(RANGES_DIR + "/" + String.format(RANGES_FILE_PATTERN, curlyName)));

            /* if contains item_part_no = 2 create file ending with _NO*/
            List<Tuple> exceptions = data.stream().filter(x -> ((Short) x.get("item_part_no")).intValue() == 2).collect(Collectors.toList());
            List<Tuple> regular = data.stream().filter(x -> ((Short) x.get("item_part_no")).intValue() == 1).collect(Collectors.toList());

            List<String> lines = new ArrayList<>();
            lines.add(RS_HEADER + ": " + header.replace("{", "").replace("}", ""));
            lines.add(RS_CURLYNAME + ": " + curlyName);
            String currentRegion = null;

            Iterator<Tuple> iter = regular.iterator();
            while (iter.hasNext()) {
                Tuple record = iter.next();

                String value = record.get("value", String.class);
                Boolean spaceBefore = record.get("space_before", Boolean.class);
                Boolean spaceAfter = record.get("space_after", Boolean.class);
                String region = record.get("region", String.class);
                Boolean removePunctuation = record.get("remove_punctuation_before", Boolean.class);
                /* if space_before is false and remove_punctuation is true, add (-) prefix to value*/
                if (BooleanUtils.isFalse(spaceBefore) && BooleanUtils.isTrue(removePunctuation)) {
                    value = REMOVE_PUNCTUATION_PREFIX + value;
                }
                /* if space_before is false and remove_punctuation is true, add (-) prefix to value*/
                if (BooleanUtils.isFalse(spaceBefore) && BooleanUtils.isFalse(removePunctuation)) {
                    value = SPACE_BEFORE_PREFIX + value;
                }
                /* if space_after is false add (-) suffix to value */
                if (BooleanUtils.isFalse(spaceAfter)) {
                    value = value + SPACE_BEFORE_PREFIX;
                }

                /* end previous region if present*/
                if (StringUtils.isNotEmpty(currentRegion) && StringUtils.isNotEmpty(region) && !region.equals(currentRegion)) {
                    lines.add(String.format(END_REGION, currentRegion));
                }

                /* add region information if is not null*/
                if (StringUtils.isNotEmpty(region) && (currentRegion == null || !currentRegion.equals(region))) {
                    currentRegion = region;
                    lines.add(String.format(BEGIN_REGION, region));
                }

                lines.add(String.format(LINE + ": %s", StringUtils.isEmpty(value) ? EMPTY_VALUE : value));

                /* if there is no record left to process and current region is set, close region*/
                if (!iter.hasNext() && StringUtils.isNotEmpty(currentRegion)) {
                    lines.add(String.format(END_REGION, currentRegion));
                }
            }
            lines.add(""); // just for comparison with existing files
            writeFileContent(lines, zout);

            /* create exception file with name ending with _NO*/
            if (exceptions.size() > 0) {

                /* create _NO file */
                zout.putNextEntry(new ZipEntry(RANGES_DIR + "/" + String.format(RANGES_FILE_PATTERN, curlyName + "_NO")));
                List<String> linesToAdd = new ArrayList<>();
                linesToAdd.add(RS_HEADER + ": " + header.replace("{", "").replace("}", ""));
                linesToAdd.add(RS_CURLYNAME + ": " + curlyName + "_NO");
                for (Tuple record : exceptions) {

                    String value = (String) record.get("value");
                    linesToAdd.add(String.format(LINE + ": %s", StringUtils.isEmpty(value) ? EMPTY_VALUE : value));
                }
                linesToAdd.add(""); // just for comparison with existing files
                writeFileContent(linesToAdd, zout);

            }

        } catch (IOException e) {
            log.error("Error in writeContentToRangesFile", e);
        }

    }


    /**
     * Calculates the position of the element with given moduleNO in German structure
     *
     * @param structure German structure of the sentence
     * @param moduleNO  moduleNo to check
     * @return position of the element in the structure
     */
    public BigInteger getPosGerman(String structure, BigInteger moduleNO) {

        List<String> elements = Arrays.asList(structure.split(","));

        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).startsWith("" + moduleNO)) {
                return moduleNO;
            }
        }
        return BigInteger.ZERO;
    }

    /**
     * Composes the export zip filename based on domain and current time
     *
     * @param domainId domain exported
     * @return filename
     */
    public String getExportZipFileName(Integer domainId) {

        ZonedDateTime now = ZonedDateTime.now();
        String domainName = copeditCommonMapper.getDomainNameById(domainId);
        /* create zip file with timestamp and domain name*/
        String fileNamePattern = "export_%s_%s_%s_%s_%s_%s.zip";
        String fileName = String.format(fileNamePattern,
            domainName,
            now.getYear(),
            TWO_DIGITS_FORMATTER.format(now.getMonthValue()),
            TWO_DIGITS_FORMATTER.format(now.getDayOfMonth()),
            TWO_DIGITS_FORMATTER.format(now.getHour()),
            TWO_DIGITS_FORMATTER.format(now.getMinute()));
        return fileName;
    }

}
