package it.clesius.albina.copedit.service.copedit;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Random;

import org.apache.commons.io.IOUtils;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class CopBackSshEngine {

	private JSch jsch = null;
	private Session session = null;
	private ChannelShell shellchannel = null;
	private ChannelSftp sftpChannel = null;
	private PrintStream outpl = null;
	private ServerSocketReaderHandler ssh = null;
	private StringBuffer sbResponce = new StringBuffer();

	public synchronized void openConnection() throws Exception {

		String ip = "192.168.30.206";
		String user = "root";
		String password = "XXXXX;";
		String port = "22";

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		jsch = new JSch();
		session = jsch.getSession(user, ip, Integer.parseInt(port));
		session.setConfig(config);
		session.setPassword(password);
		session.setServerAliveInterval(10000);
		session.connect(100000);

		Channel channel = session.openChannel("sftp");
		channel.connect();
		sftpChannel = (ChannelSftp) channel;

		shellchannel = (ChannelShell) session.openChannel("shell");
		shellchannel.setPtyType("vt102");
		shellchannel.setPtySize(500, 500, 1980, 1920);

		OutputStream sshOutputStream = shellchannel.getOutputStream();
		InputStream sshInputStream = shellchannel.getInputStream();

		shellchannel.connect(3000);

		Thread.sleep(1000);
		startReader(sshInputStream, sshOutputStream);
	}

	public void sendFile(String origin, String dest) throws SftpException {
		sftpChannel.put(origin, dest);
	}

	public void sendFileContent(String content, String dest) throws SftpException {
		InputStream stream = new ByteArrayInputStream(content.getBytes());
		sftpChannel.put(stream, dest);
	}

	public InputStream getFile(String origin) throws SftpException {
		return sftpChannel.get(origin);
	}

	public String getFileStr(String origin) throws Exception {
		InputStream inputStream = sftpChannel.get(origin);
		String str = IOUtils.toString(inputStream);
		return str;
	}

	public boolean isConnected() {
		try {
			if (this.session.isConnected()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public void disconnect() {
		ssh.stop();
		outpl.close();
		sftpChannel.disconnect();
		shellchannel.disconnect();
		session.disconnect();
	}

	public String getLastResult() {
		return sbResponce.toString();
	}

	public class ServerSocketReaderHandler implements Runnable {
		private InputStream is;
		Thread t = null;
		private boolean iStop = false;
		CopBackSshEngine iSshClient;

		public ServerSocketReaderHandler(InputStream is, CopBackSshEngine  backUpEngineSsh) {
			this.is = is;
			this.iSshClient = backUpEngineSsh;
			t = new Thread(this);
			t.start();
		}

		public void stop() {
			iStop = true;
			t.interrupt();
		}

		public void run() {
			try {
				try {
					while (true) {
						if (iStop) {
							// System.out.println("Stop");
							break;
						}
						if (iSshClient == null || !iSshClient.isConnected()) {
							// System.out.println("Stop");
							break;
						}
						Thread.sleep(1);
						byte[] buff = new byte[2024];
						int c = is.read(buff);
						if (c != -1) {
							// System.out.println(new String(buff,0,c));
							String str = new String(buff, 0, c);
							sbResponce.append(str);
							// System.out.println(str);
						}
						// System.out.println("ERRor");
					}
				} catch (Exception e) {
					if (!iStop) {
						e.printStackTrace();
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void startReader(InputStream is, OutputStream ou) throws Exception {
		outpl = new PrintStream(ou);
		ssh = new ServerSocketReaderHandler(is, this);
	}

	public static String generateString(Random rng, String characters, int length) {
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	public String executeRemoteCommandAndWaitResponce(String command, int timeOut) {
		sbResponce.delete(0, sbResponce.length());
		long timex = System.currentTimeMillis();

		String res = "";
		sbResponce.delete(0, sbResponce.length());

		String echo = generateString(new Random(), "QWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnm",
				20);
		writeRemote(command + " ; echo '[" + echo + "]'");
		int time = 0;
		while (time < timeOut) {
			time = time + 10;
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
			if (isCommandEnd("[" + echo + "]")) {
				res = findString(command + " ; echo '[" + echo + "]'", "[" + echo + "]");
				break;
			}
		}

		// System.out.println((System.currentTimeMillis()-timex)+" "+command);
		return res;
	}

	public void writeRemote(String v) {

		if (v.length() > 0) {
			outpl.println(v);
			outpl.flush();
		}

	}

	public boolean isCommandEnd(String endStr) {
		boolean end = false;
		int indexInit = sbResponce.indexOf(endStr);
		if (indexInit > -1) {
			int indexInit2 = sbResponce.indexOf(endStr, indexInit + endStr.length());
			if (indexInit2 > -1) {
				end = true;
			}
		}
		return end;
	}

	public String findString(String init, String end) {
		String ret = "";
		int initInt = sbResponce.indexOf(init);
		if (initInt > -1) {
			int endInt = sbResponce.indexOf(end, initInt + init.length());
			if (endInt > -1) {
				ret = sbResponce.substring(initInt + init.length(), endInt);

				if (ret.length() > 4) {
					ret = ret.substring(2, ret.length() - 2);
				}
			}
		}
		return ret;
	}

}
