package it.clesius.albina.copedit.service;

import it.clesius.albina.copedit.domain.Domain;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Domain.
 */
public interface DomainService {

    /**
     * Save a domain.
     *
     * @param domain the entity to save
     * @return the persisted entity
     */
    Domain save(Domain domain);

    /**
     * Get all the domains.
     *
     * @return the list of entities
     */
    List<Domain> findAll();


    /**
     * Get the "id" domain.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Domain> findOne(Long id);

    /**
     * Delete the "id" domain.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
