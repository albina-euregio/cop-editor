package it.clesius.albina.copedit.service.impl;

import it.clesius.albina.copedit.service.SentenceService;
import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.repository.SentenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Sentence.
 */
@Service
@Transactional
public class SentenceServiceImpl implements SentenceService {

    private final Logger log = LoggerFactory.getLogger(SentenceServiceImpl.class);

    private final SentenceRepository sentenceRepository;

    public SentenceServiceImpl(SentenceRepository sentenceRepository) {
        this.sentenceRepository = sentenceRepository;
    }

    /**
     * Save a sentence.
     *
     * @param sentence the entity to save
     * @return the persisted entity
     */
    @Override
    public Sentence save(Sentence sentence) {
        log.debug("Request to save Sentence : {}", sentence);        return sentenceRepository.save(sentence);
    }

    /**
     * Get all the sentences.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Sentence> findAll(Pageable pageable) {
        log.debug("Request to get all Sentences");
        return sentenceRepository.findAll(pageable);
    }


    /**
     * Get one sentence by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Sentence> findOne(Long id) {
        log.debug("Request to get Sentence : {}", id);
        return sentenceRepository.findById(id);
    }

    /**
     * Delete the sentence by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Sentence : {}", id);
        sentenceRepository.deleteById(id);
    }
}
