package it.clesius.albina.copedit.config;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(it.clesius.albina.copedit.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Domain.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Sentence.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Sentence.class.getName() + ".phraseOptionDES", jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Sentence.class.getName() + ".phraseOptionITS", jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Sentence.class.getName() + ".phraseOptionENS", jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Sentence.class.getName() + ".phraseOptionFRS", jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.SentenceLang.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.SentenceLang.class.getName() + ".phraseOptions", jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.PhraseOption.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Version.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.Phrase.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.SentenceModule.class.getName(), jcacheConfiguration);
            cm.createCache(it.clesius.albina.copedit.domain.PhraseOptionItem.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
