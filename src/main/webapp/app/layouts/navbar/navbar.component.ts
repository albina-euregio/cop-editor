import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiLanguageService } from 'ng-jhipster';

import { VERSION } from 'app/app.constants';
import { JhiLanguageHelper, Principal, LoginModalService, LoginService } from 'app/core';
import { ProfileService } from '../profiles/profile.service';
import { MenuItem } from '../../../../../../node_modules/primeng/api';
import { SidebarService } from 'app/shared/sidebar.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['navbar.scss']
})
export class NavbarComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    showMenuFlag: Boolean;
    items: MenuItem[];
    subscription: Subscription;

    constructor(
        private loginService: LoginService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router,
        private sidebarService: SidebarService,
        private cd: ChangeDetectorRef
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
    }

    ngOnInit() {
        this.subscription = this.sidebarService.getSidebarShowStatus().subscribe(message => {
            this.showMenuFlag = message;
            this.cd.detectChanges();
        });

        this.languageHelper.getAll().then(languages => {
            this.languages = languages;
        });
        this.showMenuFlag = false;

        this.profileService.getProfileInfo().then(profileInfo => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });

        this.items = [
            {
                label: 'Sentences',
                items: [
                    {
                        label: 'Sentences List',
                        icon: 'fa fa-list',
                        routerLink: 'sentencesList'
                    }
                ]
            },
            {
                label: 'Phrases',
                items: [{ label: 'Phrases list', icon: 'fa fa-list', routerLink: 'phrasesList' }]
            },
            {
                label: 'Import',
                items: [
                    {
                        label: 'Import',
                        icon: 'fa fa-file-import',
                        routerLink: 'import'
                    }
                ]
            },
            {
                label: 'Export',
                items: [
                    {
                        label: 'Export',
                        icon: 'fa fa-file-export',
                        routerLink: 'export'
                    }
                ]
            },
            {
                label: 'Recode domain',
                icon: 'fa fa-coins',
                routerLink: 'promoteDomain'
            }
        ];
    }

    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }

    showMenu() {
        this.showMenuFlag = !this.showMenuFlag;
    }
}
