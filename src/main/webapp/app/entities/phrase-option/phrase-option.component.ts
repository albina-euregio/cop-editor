import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPhraseOption } from 'app/shared/model/phrase-option.model';
import { Principal } from 'app/core';
import { PhraseOptionService } from './phrase-option.service';

@Component({
    selector: 'jhi-phrase-option',
    templateUrl: './phrase-option.component.html'
})
export class PhraseOptionComponent implements OnInit, OnDestroy {
    phraseOptions: IPhraseOption[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private phraseOptionService: PhraseOptionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.phraseOptionService.query().subscribe(
            (res: HttpResponse<IPhraseOption[]>) => {
                this.phraseOptions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPhraseOptions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPhraseOption) {
        return item.id;
    }

    registerChangeInPhraseOptions() {
        this.eventSubscriber = this.eventManager.subscribe('phraseOptionListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
