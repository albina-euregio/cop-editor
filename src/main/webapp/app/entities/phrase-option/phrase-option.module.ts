import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared';
import {
    PhraseOptionComponent,
    PhraseOptionDetailComponent,
    PhraseOptionUpdateComponent,
    PhraseOptionDeletePopupComponent,
    PhraseOptionDeleteDialogComponent,
    phraseOptionRoute,
    phraseOptionPopupRoute
} from './';

const ENTITY_STATES = [...phraseOptionRoute, ...phraseOptionPopupRoute];

@NgModule({
    imports: [AppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PhraseOptionComponent,
        PhraseOptionDetailComponent,
        PhraseOptionUpdateComponent,
        PhraseOptionDeleteDialogComponent,
        PhraseOptionDeletePopupComponent
    ],
    entryComponents: [
        PhraseOptionComponent,
        PhraseOptionUpdateComponent,
        PhraseOptionDeleteDialogComponent,
        PhraseOptionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppPhraseOptionModule {}
