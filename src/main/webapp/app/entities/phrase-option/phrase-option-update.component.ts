import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IPhraseOption } from 'app/shared/model/phrase-option.model';
import { PhraseOptionService } from './phrase-option.service';

@Component({
    selector: 'jhi-phrase-option-update',
    templateUrl: './phrase-option-update.component.html'
})
export class PhraseOptionUpdateComponent implements OnInit {
    private _phraseOption: IPhraseOption;
    isSaving: boolean;
    datenew: string;
    datelast: string;

    constructor(private phraseOptionService: PhraseOptionService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ phraseOption }) => {
            this.phraseOption = phraseOption;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.phraseOption.datenew = moment(this.datenew, DATE_TIME_FORMAT);
        this.phraseOption.datelast = moment(this.datelast, DATE_TIME_FORMAT);
        if (this.phraseOption.id !== undefined) {
            this.subscribeToSaveResponse(this.phraseOptionService.update(this.phraseOption));
        } else {
            this.subscribeToSaveResponse(this.phraseOptionService.create(this.phraseOption));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPhraseOption>>) {
        result.subscribe((res: HttpResponse<IPhraseOption>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get phraseOption() {
        return this._phraseOption;
    }

    set phraseOption(phraseOption: IPhraseOption) {
        this._phraseOption = phraseOption;
        this.datenew = moment(phraseOption.datenew).format(DATE_TIME_FORMAT);
        this.datelast = moment(phraseOption.datelast).format(DATE_TIME_FORMAT);
    }
}
