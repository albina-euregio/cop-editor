import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPhraseOption } from 'app/shared/model/phrase-option.model';
import { PhraseOptionService } from './phrase-option.service';

@Component({
    selector: 'jhi-phrase-option-delete-dialog',
    templateUrl: './phrase-option-delete-dialog.component.html'
})
export class PhraseOptionDeleteDialogComponent {
    phraseOption: IPhraseOption;

    constructor(
        private phraseOptionService: PhraseOptionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.phraseOptionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'phraseOptionListModification',
                content: 'Deleted an phraseOption'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-phrase-option-delete-popup',
    template: ''
})
export class PhraseOptionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ phraseOption }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PhraseOptionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.phraseOption = phraseOption;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
