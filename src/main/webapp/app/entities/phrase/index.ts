export * from './phrase.service';
export * from './phrase-update.component';
export * from './phrase-delete-dialog.component';
export * from './phrase-detail.component';
export * from './phrase.component';
export * from './phrase.route';
