import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IPhrase } from 'app/shared/model/phrase.model';
import { PhraseService } from './phrase.service';

@Component({
    selector: 'jhi-phrase-update',
    templateUrl: './phrase-update.component.html'
})
export class PhraseUpdateComponent implements OnInit {
    private _phrase: IPhrase;
    isSaving: boolean;
    datelast: string;
    datenew: string;

    constructor(private phraseService: PhraseService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ phrase }) => {
            this.phrase = phrase;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.phrase.datelast = moment(this.datelast, DATE_TIME_FORMAT);
        this.phrase.datenew = moment(this.datenew, DATE_TIME_FORMAT);
        if (this.phrase.id !== undefined) {
            this.subscribeToSaveResponse(this.phraseService.update(this.phrase));
        } else {
            this.subscribeToSaveResponse(this.phraseService.create(this.phrase));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPhrase>>) {
        result.subscribe((res: HttpResponse<IPhrase>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get phrase() {
        return this._phrase;
    }

    set phrase(phrase: IPhrase) {
        this._phrase = phrase;
        this.datelast = moment(phrase.datelast).format(DATE_TIME_FORMAT);
        this.datenew = moment(phrase.datenew).format(DATE_TIME_FORMAT);
    }
}
