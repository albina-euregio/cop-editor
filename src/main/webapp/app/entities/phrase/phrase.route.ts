import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Phrase } from 'app/shared/model/phrase.model';
import { PhraseService } from './phrase.service';
import { PhraseComponent } from './phrase.component';
import { PhraseDetailComponent } from './phrase-detail.component';
import { PhraseUpdateComponent } from './phrase-update.component';
import { PhraseDeletePopupComponent } from './phrase-delete-dialog.component';
import { IPhrase } from 'app/shared/model/phrase.model';

@Injectable({ providedIn: 'root' })
export class PhraseResolve implements Resolve<IPhrase> {
    constructor(private service: PhraseService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((phrase: HttpResponse<Phrase>) => phrase.body));
        }
        return Observable.of(new Phrase());
    }
}

export const phraseRoute: Routes = [
    {
        path: 'phrase',
        component: PhraseComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'appApp.phrase.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'phrase/:id/view',
        component: PhraseDetailComponent,
        resolve: {
            phrase: PhraseResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phrase.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'phrase/new',
        component: PhraseUpdateComponent,
        resolve: {
            phrase: PhraseResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phrase.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'phrase/:id/edit',
        component: PhraseUpdateComponent,
        resolve: {
            phrase: PhraseResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phrase.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const phrasePopupRoute: Routes = [
    {
        path: 'phrase/:id/delete',
        component: PhraseDeletePopupComponent,
        resolve: {
            phrase: PhraseResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phrase.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
