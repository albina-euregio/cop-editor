import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared';
import {
    PhraseComponent,
    PhraseDetailComponent,
    PhraseUpdateComponent,
    PhraseDeletePopupComponent,
    PhraseDeleteDialogComponent,
    phraseRoute,
    phrasePopupRoute
} from './';

const ENTITY_STATES = [...phraseRoute, ...phrasePopupRoute];

@NgModule({
    imports: [AppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [PhraseComponent, PhraseDetailComponent, PhraseUpdateComponent, PhraseDeleteDialogComponent, PhraseDeletePopupComponent],
    entryComponents: [PhraseComponent, PhraseUpdateComponent, PhraseDeleteDialogComponent, PhraseDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppPhraseModule {}
