export * from './sentence-lang.service';
export * from './sentence-lang-update.component';
export * from './sentence-lang-delete-dialog.component';
export * from './sentence-lang-detail.component';
export * from './sentence-lang.component';
export * from './sentence-lang.route';
