import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISentenceLang } from 'app/shared/model/sentence-lang.model';

@Component({
    selector: 'jhi-sentence-lang-detail',
    templateUrl: './sentence-lang-detail.component.html'
})
export class SentenceLangDetailComponent implements OnInit {
    sentenceLang: ISentenceLang;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sentenceLang }) => {
            this.sentenceLang = sentenceLang;
        });
    }

    previousState() {
        window.history.back();
    }
}
