import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISentenceLang } from 'app/shared/model/sentence-lang.model';
import { SentenceLangService } from './sentence-lang.service';

@Component({
    selector: 'jhi-sentence-lang-update',
    templateUrl: './sentence-lang-update.component.html'
})
export class SentenceLangUpdateComponent implements OnInit {
    private _sentenceLang: ISentenceLang;
    isSaving: boolean;
    updatedDate: string;

    constructor(private sentenceLangService: SentenceLangService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sentenceLang }) => {
            this.sentenceLang = sentenceLang;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.sentenceLang.updatedDate = moment(this.updatedDate, DATE_TIME_FORMAT);
        if (this.sentenceLang.id !== undefined) {
            this.subscribeToSaveResponse(this.sentenceLangService.update(this.sentenceLang));
        } else {
            this.subscribeToSaveResponse(this.sentenceLangService.create(this.sentenceLang));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISentenceLang>>) {
        result.subscribe((res: HttpResponse<ISentenceLang>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get sentenceLang() {
        return this._sentenceLang;
    }

    set sentenceLang(sentenceLang: ISentenceLang) {
        this._sentenceLang = sentenceLang;
        this.updatedDate = moment(sentenceLang.updatedDate).format(DATE_TIME_FORMAT);
    }
}
