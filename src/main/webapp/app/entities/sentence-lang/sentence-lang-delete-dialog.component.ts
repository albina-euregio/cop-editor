import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISentenceLang } from 'app/shared/model/sentence-lang.model';
import { SentenceLangService } from './sentence-lang.service';

@Component({
    selector: 'jhi-sentence-lang-delete-dialog',
    templateUrl: './sentence-lang-delete-dialog.component.html'
})
export class SentenceLangDeleteDialogComponent {
    sentenceLang: ISentenceLang;

    constructor(
        private sentenceLangService: SentenceLangService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sentenceLangService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sentenceLangListModification',
                content: 'Deleted an sentenceLang'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sentence-lang-delete-popup',
    template: ''
})
export class SentenceLangDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sentenceLang }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SentenceLangDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sentenceLang = sentenceLang;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
