import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISentenceModule } from 'app/shared/model/sentence-module.model';
import { Principal } from 'app/core';
import { SentenceModuleService } from './sentence-module.service';

@Component({
    selector: 'jhi-sentence-module',
    templateUrl: './sentence-module.component.html'
})
export class SentenceModuleComponent implements OnInit, OnDestroy {
    sentenceModules: ISentenceModule[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private sentenceModuleService: SentenceModuleService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.sentenceModuleService.query().subscribe(
            (res: HttpResponse<ISentenceModule[]>) => {
                this.sentenceModules = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSentenceModules();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISentenceModule) {
        return item.id;
    }

    registerChangeInSentenceModules() {
        this.eventSubscriber = this.eventManager.subscribe('sentenceModuleListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
