import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISentenceModule } from 'app/shared/model/sentence-module.model';

type EntityResponseType = HttpResponse<ISentenceModule>;
type EntityArrayResponseType = HttpResponse<ISentenceModule[]>;

@Injectable({ providedIn: 'root' })
export class SentenceModuleService {
    private resourceUrl = SERVER_API_URL + 'api/sentence-modules';

    constructor(private http: HttpClient) {}

    create(sentenceModule: ISentenceModule): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(sentenceModule);
        return this.http
            .post<ISentenceModule>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(sentenceModule: ISentenceModule): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(sentenceModule);
        return this.http
            .put<ISentenceModule>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ISentenceModule>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ISentenceModule[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(sentenceModule: ISentenceModule): ISentenceModule {
        const copy: ISentenceModule = Object.assign({}, sentenceModule, {
            datenew: sentenceModule.datenew != null && sentenceModule.datenew.isValid() ? sentenceModule.datenew.toJSON() : null,
            datelast: sentenceModule.datelast != null && sentenceModule.datelast.isValid() ? sentenceModule.datelast.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.datenew = res.body.datenew != null ? moment(res.body.datenew) : null;
        res.body.datelast = res.body.datelast != null ? moment(res.body.datelast) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((sentenceModule: ISentenceModule) => {
            sentenceModule.datenew = sentenceModule.datenew != null ? moment(sentenceModule.datenew) : null;
            sentenceModule.datelast = sentenceModule.datelast != null ? moment(sentenceModule.datelast) : null;
        });
        return res;
    }
}
