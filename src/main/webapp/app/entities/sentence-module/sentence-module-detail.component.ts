import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISentenceModule } from 'app/shared/model/sentence-module.model';

@Component({
    selector: 'jhi-sentence-module-detail',
    templateUrl: './sentence-module-detail.component.html'
})
export class SentenceModuleDetailComponent implements OnInit {
    sentenceModule: ISentenceModule;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sentenceModule }) => {
            this.sentenceModule = sentenceModule;
        });
    }

    previousState() {
        window.history.back();
    }
}
