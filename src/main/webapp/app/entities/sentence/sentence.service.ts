import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISentence } from 'app/shared/model/sentence.model';

type EntityResponseType = HttpResponse<ISentence>;
type EntityArrayResponseType = HttpResponse<ISentence[]>;

@Injectable({ providedIn: 'root' })
export class SentenceService {
    private resourceUrl = SERVER_API_URL + 'api/sentences';

    constructor(private http: HttpClient) {}

    create(sentence: ISentence): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(sentence);
        return this.http
            .post<ISentence>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(sentence: ISentence): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(sentence);
        return this.http
            .put<ISentence>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ISentence>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ISentence[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(sentence: ISentence): ISentence {
        const copy: ISentence = Object.assign({}, sentence, {
            datenew: sentence.datenew != null && sentence.datenew.isValid() ? sentence.datenew.toJSON() : null,
            datelast: sentence.datelast != null && sentence.datelast.isValid() ? sentence.datelast.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.datenew = res.body.datenew != null ? moment(res.body.datenew) : null;
        res.body.datelast = res.body.datelast != null ? moment(res.body.datelast) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((sentence: ISentence) => {
            sentence.datenew = sentence.datenew != null ? moment(sentence.datenew) : null;
            sentence.datelast = sentence.datelast != null ? moment(sentence.datelast) : null;
        });
        return res;
    }
}
