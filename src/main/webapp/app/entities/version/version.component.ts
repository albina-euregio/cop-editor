import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IVersion } from 'app/shared/model/version.model';
import { Principal } from 'app/core';
import { VersionService } from './version.service';

@Component({
    selector: 'jhi-version',
    templateUrl: './version.component.html'
})
export class VersionComponent implements OnInit, OnDestroy {
    versions: IVersion[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private versionService: VersionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.versionService.query().subscribe(
            (res: HttpResponse<IVersion[]>) => {
                this.versions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInVersions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IVersion) {
        return item.id;
    }

    registerChangeInVersions() {
        this.eventSubscriber = this.eventManager.subscribe('versionListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
