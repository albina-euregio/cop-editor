import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { TEXTCAT_API } from 'app/app.constants';
import { LoginModalService, Principal, Account } from 'app/core';
import { LocalStorageService, LocalStorage } from '../../../../../node_modules/ngx-webstorage';
import { DomainDTO } from '../shared/copedit-dto/domainDTO.model';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { CopeditLookupService } from 'app/forms/copedit-shared/copedit-lookup.service';

import { JhiConfigurationService } from '../admin/configuration/configuration.service';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss'],
    providers: [LocalStorageService, CopeditLookupService]
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    @LocalStorage('currentDomain') public currentDomain: DomainDTO;
    domains: Array<DomainDTO>;
    textcatAPI: string;
    allConfiguration: any = null;
    configuration: any = null;
    configKeys: any[];

    constructor(
        private configurationService: JhiConfigurationService,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private localstorageService: LocalStorageService,
        private route: ActivatedRoute,
        private copEditLookupService: CopeditLookupService
    ) {
        this.configKeys = [];
    }

    ngOnInit() {
        this.textcatAPI = TEXTCAT_API;
        /* set default domain to PRODUCTION */
        if (this.currentDomain === undefined) {
            this.currentDomain = { id: 1, name: 'PRODUCTION' /*, datenew: null, datelast: null */ };
        }
        this.principal.identity().then(account => {
            this.account = account;
            this.copEditLookupService.getDomains().subscribe(res => {
                this.domains = res.body;
            });
        });
        this.registerAuthenticationSuccess();

        this.configurationService.get().subscribe(configuration => {
            this.configuration = configuration;

            for (const config of configuration) {
                if (config.properties !== undefined) {
                    this.configKeys.push(Object.keys(config.properties));
                }
            }
        });

        this.configurationService.getEnv().subscribe(configuration => {
            this.allConfiguration = configuration;
        });
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.principal.identity().then(account => {
                this.account = account;
                this.copEditLookupService.getDomains().subscribe(res => {
                    this.domains = res.body;
                });
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    keys(dict): Array<string> {
        return dict === (undefined || null) ? [] : Object.keys(dict);
    }

    toShow(key) {
        let keyToSee = 'applicationConfig: [classpath:/config/application-dev.yml]';
        keyToSee = keyToSee + ' applicationConfig: [classpath:/config/application-prod.yml]';
        if (keyToSee.indexOf(key) > -1) {
            return true;
        } else {
            return false;
        }
    }

    itemToShow(item) {
        let keyToSee = 'spring.datasource.url, server.contextPath, application.import-properties.import-folder, ';
        keyToSee =
            keyToSee +
            'application.textcat-api.textcatAPI-url, application.textcat-api.sentences-url, application.textcat-api.indexes-url, ';
        keyToSee =
            keyToSee + 'application.textcat-api.import-url, application.textcat-api.recode-url, application.textcat-api.reload-url, ';
        keyToSee = keyToSee + 'application.export-properties.temp-folder';
        keyToSee = keyToSee + 'application.git-properties.localPath';
        keyToSee = keyToSee + 'application.git-properties.exportlocalPath';
        keyToSee = keyToSee + 'application.git-properties.remotePath';
        keyToSee = keyToSee + 'application.git-properties.name';
        keyToSee = keyToSee + 'application.git-properties.password';

        if (keyToSee.indexOf(item.key) > -1) {
            return true;
        } else {
            return false;
        }
    }
}
