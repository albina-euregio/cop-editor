import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL, TEXTCAT_API } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISentence, Sentence } from 'app/shared/model/sentence.model';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { CopeditLookupService } from 'app/forms/copedit-shared/copedit-lookup.service';
import { IVersion } from 'app/shared/model/version.model';
import { Domain } from 'app/shared/model/domain.model';
import { Phrase } from 'app/shared/model/phrase.model';
import { CopEditPhraseService } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { SentenceDTO } from 'app/shared/copedit-dto/sentenceDTO.model';
import { SentenceLanguageDTO } from '../../shared/copedit-dto/sentenceLanguageDTO.model';
import { PhraseOptionDTO } from 'app/shared/copedit-dto/phraseOptionDTO.model';
import { structureRegex } from 'app/shared/constants/copedit.constants';
import { LocalStorageService } from '../../../../../../node_modules/ngx-webstorage';

type EntityResponseType = HttpResponse<SentenceDTO>;
type EntityArrayResponseType = HttpResponse<ISentence[]>;

@Injectable({ providedIn: 'root' })
export class CopeditSentencesService {
    private resourceUrl = SERVER_API_URL + 'api/copedit/sentences';

    constructor(private http: HttpClient, private localStorageService: LocalStorageService) {}

    create(sentence: SentenceDTO): Observable<EntityResponseType> {
        return this.http.post<SentenceDTO>(this.resourceUrl, sentence, { observe: 'response' });
    }

    update(sentence: SentenceDTO): Observable<EntityResponseType> {
        return this.http.put<SentenceDTO>(this.resourceUrl, sentence, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SentenceDTO>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    findAll(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http.get<SentenceDTO>(`${this.resourceUrl}`, { params: options, observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISentence[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(name: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }

    findByName(name: string): Observable<EntityResponseType> {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.http.get<SentenceDTO>(`${this.resourceUrl}/${name}?domain=${currentDomain.id}`, { observe: 'response' });
    }

    getNewSentence(): Observable<EntityResponseType> {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.http.get<SentenceDTO>(`${this.resourceUrl}/newSentence?domain=${currentDomain.id}`, { observe: 'response' });
    }

    reloadTexcat(lang: string, domain: string) {
        const resourceTextCatUrl = SERVER_API_URL + 'api/copedit/reloadTexcat';
        return this.http.get<any>(resourceTextCatUrl + `?lang=${lang}&domain=${domain}`, { observe: 'response' });
    }

    translate(lang: string, domain: string, sentence: string) {
        const resourceTextCatUrl = TEXTCAT_API + 'api/v2/translate?def=';
        return this.http.get<any>(resourceTextCatUrl + encodeURIComponent(sentence) + '&l=' + lang + '&dn=' + domain, {
            observe: 'response'
        });
    }

    /* Checks if structure is valid matching it to the phraseOptions array and testing it against regex and length
    *
    * @private
    * @returns {Boolean} true if structure is valid, else false
    * @memberof CopeditPhraseOptionEditorComponent
    */
    public isStructureValid(sentenceLanguage: SentenceLanguageDTO): Boolean {
        const struct = sentenceLanguage.structure.split(',');
        // check number of elements match
        if (struct.length !== sentenceLanguage.phraseOptions.length) {
            return false;
        }
        // check regex match
        if (!structureRegex.test(sentenceLanguage.structure)) {
            return false;
        }
        // check existence of every element
        let isValid: Boolean = true;

        struct.forEach(x => {
            const element: Array<string> = x.split('.');
            const moduleNO = Number(element[0]);
            const itemNO = Number(element[1]);
            const result: PhraseOptionDTO = sentenceLanguage.phraseOptions.find(el => el.itemNo === itemNO && el.moduleNo === moduleNO);
            if (result === undefined) {
                isValid = false;
            }
            // check duplicated/not present elements
            const nrOfOccurences: number = struct.filter(el => el === x).length;
            if (nrOfOccurences > 1) {
                isValid = false;
            }
        });
        return isValid;
    }
}

/* RESOLVERS */
@Injectable()
export class VersionsResolver implements Resolve<IVersion[]> {
    constructor(private lookupService: CopeditLookupService) {}

    resolve() {
        return this.lookupService.getVersions().pipe(map((x: HttpResponse<IVersion[]>) => x.body));
    }
}

@Injectable()
export class DomainsResolver implements Resolve<Domain[]> {
    constructor(private lookupService: CopeditLookupService) {}

    resolve() {
        return this.lookupService.getDomains().pipe(map((x: HttpResponse<Domain[]>) => x.body));
    }
}

@Injectable()
export class PhrasesResolver implements Resolve<Phrase[]> {
    constructor(private copeditPhraseService: CopEditPhraseService) {}

    resolve() {
        return this.copeditPhraseService.getList().pipe(map((x: HttpResponse<Domain[]>) => x.body));
    }
}

@Injectable()
export class CopeditSentenceResolver implements Resolve<SentenceDTO> {
    constructor(private copeditPhraseService: CopeditSentencesService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.copeditPhraseService.findByName(route.paramMap.get('name')).pipe(map((x: HttpResponse<SentenceDTO>) => x.body));
    }
}

@Injectable()
export class CopeditNewSentenceResolver implements Resolve<SentenceDTO> {
    constructor(private copeditPhraseService: CopeditSentencesService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.copeditPhraseService.getNewSentence().pipe(map((x: HttpResponse<SentenceDTO>) => x.body));
    }
}
