import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PhraseDTO } from 'app/shared/copedit-dto/phraseDTO.model';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { faSitemap } from '@fortawesome/free-solid-svg-icons';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';
import { ActivatedRoute, Router } from '@angular/router';
import { CopEditPhraseService } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { PhraseOption } from 'app/shared/model/phrase-option.model';

@Component({
    selector: 'jhi-copedit-phrase-dd2',
    templateUrl: 'copedit-phrase-dd2.component.html',
    providers: [MessageService]
})
export class CopeditPhraseOptiondd2Component implements OnInit {
    faEdit = faEdit;
    faPlusSquare = faPlusSquare;
    faSitemap = faSitemap;

    options: any = {
        removeOnSpill: true
    };

    @Input('subSubPhraseOptions') subSubPhraseOptions: any;
    @Input('selectedPhraseOption') selectedPhraseOption: any;
    @Input('idx1') idx1: number;
    @Input('idx2') idx2: number;
    @Input('idx3') idx3: number;
    @Input('sample') sample: string;
    @Input('subsubnames') subsubnames: Array<String>;
    @Input('lang') lang: string;

    @Output() sampleValueChange = new EventEmitter();

    changeSampleValue() {
        this.sampleValueChange.emit(this.sample);
    }

    constructor(private messageService: MessageService, private router: Router, private copEditPhraseService: CopEditPhraseService) {}

    ngOnInit() {
        const debug = '';
    }

    changeSubOption($event, idx) {
        const val = $event.value.value;
        const id = $event.value.id;
        if (val.indexOf('{') >= 0) {
            const dd = new Array();
            const str = $event.value.value.split('{');
            str.forEach(element => {
                if (element.indexOf('}') >= 0) {
                    dd.push(element.split('}')[0]);
                }
            });

            const idxOpt = this.subSubPhraseOptions[idx].findIndex(x => x.id === id);
            const po = this.subSubPhraseOptions[idx][idxOpt];
            po.phraseSubOption = new Array();
            po.selectedPhraseSubOption = new Array();
            for (let i = 0; i < dd.length; i++) {
                if (dd[i].toString().contains('_NO}')) {
                    dd[i] = dd[i].toString().replace('_NO}', '');
                }

                this.copEditPhraseService.findPhraseByNameOld(dd[i]).subscribe(x => {
                    po.phraseSubOption[i] = new Array();
                    po.selectedPhraseSubOption[i] = new Array();
                    if (this.lang === 'DE') {
                        this.updateSubPhraseValues(po, x.body.phrasesDE, i);
                    } else {
                        this.updateSubPhraseValues(po, x.body.phrasesIT, i);
                    }
                });
            }
        }
        this.changeSampleValue();
    }

    updateSubPhraseValues(po, p, i) {
        let y = 0;
        p.phrases.forEach(element => {
            if (!element.phrase.value) {
                element.phrase.value = '   _   ';
            }
            po.phraseSubOption[i].push(element.phrase);
            if (y === 0) {
                po.selectedPhraseSubOption[i] = po.phraseSubOption[i][0];
            }
            y += 1;
        });
    }
}
