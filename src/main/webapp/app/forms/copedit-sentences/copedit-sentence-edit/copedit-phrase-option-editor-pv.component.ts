import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { PhraseDTO } from 'app/shared/copedit-dto/phraseDTO.model';
import { SentenceDTO } from 'app/shared/copedit-dto/sentenceDTO.model';
import { SentenceLanguageDTO } from 'app/shared/copedit-dto/sentenceLanguageDTO.model';
import { CopeditSentencesService } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { faSitemap } from '@fortawesome/free-solid-svg-icons';
import { UpdatePhraseOptionSharedService } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-sentences-update-phraseOption.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';
import { structureRegex } from 'app/shared/constants/copedit.constants';
import { ActivatedRoute, Router } from '@angular/router';
import { CopEditPhraseService } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { SERVER_API_URL, TEXTCAT_API } from 'app/app.constants';

@Component({
    selector: 'jhi-copedit-phrase-option-editor-pv',
    templateUrl: 'copedit-phrase-option-editor-pv.component.html',
    providers: [MessageService]
})
export class CopeditPhraseOptionEditorPvComponent implements OnInit {
    @Input('sentence') sentence: SentenceDTO;

    faEdit = faEdit;
    faPlusSquare = faPlusSquare;
    faSitemap = faSitemap;
    options: any = {
        removeOnSpill: true
    };
    sentenceModule: any;

    sentenceLanguage: SentenceLanguageDTO;
    selectedPhraseOption: Array<PhraseDTO>;
    sample: string;
    indexOpt: Array<number>;
    texts: Record<string, string> = {};
    lang: string;
    loading: boolean;
    subnames: Array<Array<string>>;

    constructor(
        private sentenceService: CopeditSentencesService,
        private updatePhraseOptionSharedService: UpdatePhraseOptionSharedService,
        private messageService: MessageService,
        private router: Router,
        private copEditPhraseService: CopEditPhraseService
    ) {}

    ngOnInit() {
        if (this.lang == null) {
            this.lang = 'DE';
        }
        if (this.lang === 'DE') {
            this.sentenceLanguage = this.sentence.sentencesDE;
        } else {
            this.sentenceLanguage = this.sentence.sentencesIT;
        }

        this.indexOpt = new Array();
        this.selectedPhraseOption = new Array(this.sentenceLanguage.phraseOptions.length);
        this.subnames = new Array(this.sentenceLanguage.phraseOptions.length);
        for (let i = 0; i < this.sentenceLanguage.phraseOptions.length; i++) {
            this.indexOpt[i] = 0;
            const item = this.sentenceLanguage.phraseOptions[i];
            this.copEditPhraseService.findPhraseByNameOld(item.name).subscribe(x => {
                if (this.lang === 'DE') {
                    this.updatePhraseValues(x.body.phrasesDE, i);
                } else {
                    this.updatePhraseValues(x.body.phrasesIT, i);
                }
            });
        }
    }

    setLang($event) {
        if ($event.index === 0) {
            this.lang = 'DE';
        } else {
            this.lang = 'IT';
        }
        this.ngOnInit();
    }

    updatePhraseValues(p, i) {
        this.sentenceLanguage.phraseOptions[i].phrasesPV = new Array();
        if (this.sentenceLanguage.phraseOptions[i].itemNo === 2) {
            p.phrasesItemPartNo2.forEach(element => {
                element.phrase.phraseSubOption = new Array();
                element.phrase.selectedPhraseSubOption = new Array();
                if (!element.phrase.value) {
                    element.phrase.value = '   _   ';
                }
                this.sentenceLanguage.phraseOptions[i].phrasesPV.push(element.phrase);
            });
        } else {
            p.phrases.forEach(element => {
                element.phrase.phraseSubOption = new Array();
                element.phrase.selectedPhraseSubOption = new Array();
                if (!element.phrase.value) {
                    element.phrase.value = '   _   ';
                }
                this.sentenceLanguage.phraseOptions[i].phrasesPV.push(element.phrase);
            });
        }

        this.selectedPhraseOption[i] = this.sentenceLanguage.phraseOptions[i].phrasesPV[0];
        const val = this.sentenceLanguage.phraseOptions[i].phrasesPV[0].value;
        if (val && val.indexOf('{') >= 0) {
            const event = { value: null };
            event.value = this.selectedPhraseOption[i];
            this.changeOption(event, i);
        }
        this.loading = true;
        this.translate();
    }

    changeOption($event, idx) {
        const val = $event.value.value;
        const id = $event.value.id;
        const idxOpt = this.sentenceLanguage.phraseOptions[idx].phrasesPV.findIndex(x => x.id === id);
        this.indexOpt[idx] = idxOpt;
        const dd = new Array();
        if (val && val.indexOf('{') >= 0) {
            const str = $event.value.value.split('{');
            str.forEach(element => {
                if (element.indexOf('}') >= 0) {
                    dd.push(element.split('}')[0]);
                }
            });

            const po = this.sentenceLanguage.phraseOptions[idx].phrasesPV[idxOpt];
            let item_NO = -1;
            for (let i = 0; i < dd.length; i++) {
                const checkval = dd[i];
                if (checkval.indexOf('_NO') > 0) {
                    dd[i] = checkval.replace('_NO', '');
                    item_NO = i;
                }

                this.copEditPhraseService.findPhraseByNameOld(dd[i]).subscribe(async x => {
                    po.phraseSubOption[i] = new Array();
                    po.selectedPhraseSubOption[i] = new Array();
                    if (this.lang === 'DE') {
                        this.updateSubPhraseValues(po, x.body.phrasesDE, i, item_NO);
                    } else {
                        this.updateSubPhraseValues(po, x.body.phrasesIT, i, item_NO);
                    }
                });
            }
        }
        // sync the _NO
        if ($event.value.itemPartNo === 2) {
            const same = this.sentenceLanguage.phraseOptions.findIndex(
                x => x.name === this.sentenceLanguage.phraseOptions[idx].name && x.itemNo === 1
            );
            this.selectedPhraseOption[same] = this.sentenceLanguage.phraseOptions[same].phrasesPV[idxOpt];
            const debug = '';
        } else {
            const same = this.sentenceLanguage.phraseOptions.findIndex(
                x => x.name === this.sentenceLanguage.phraseOptions[idx].name && x.itemNo === 2
            );
            if (same > -1) {
                this.selectedPhraseOption[same] = this.sentenceLanguage.phraseOptions[same].phrasesPV[idxOpt];
            }
        }
        this.subnames[idx] = dd;
        this.loading = true;
        this.translate();
    }

    updateSubPhraseValues(po, p, i, item_NO) {
        let y = 0;
        if (i === item_NO) {
            p.phrasesItemPartNo2.forEach(element => {
                if (!element.phrase.value) {
                    element.phrase.value = '   _   ';
                }
                po.phraseSubOption[i].push(element.phrase);
                if (y === 0) {
                    po.selectedPhraseSubOption[i] = po.phraseSubOption[i][0];
                }
                y += 1;
            });
        } else {
            p.phrases.forEach(element => {
                if (!element.phrase.value) {
                    element.phrase.value = '   _   ';
                }
                po.phraseSubOption[i].push(element.phrase);
                if (y === 0) {
                    po.selectedPhraseSubOption[i] = po.phraseSubOption[i][0];
                }
                y += 1;
            });
        }
        this.loading = true;
        this.translate();
    }

    displaySample(sample) {
        this.sample = sample;
        this.loading = true;
        this.translate();
    }

    translate() {
        this.sample = this.sentenceLanguage.id + '[';
        this.selectedPhraseOption.forEach(element => {
            this.sample = this.sample + element.id.toString() + ',';
            let y = 0;
            let idDb = '';
            let idDb2 = '';
            let i;
            for (i = 0; i < element.selectedPhraseSubOption.length; i++) {
                const so = <PhraseDTO>element.selectedPhraseSubOption[i];
                if (so && so.id) {
                    idDb = idDb + so.id.toString() + ',';
                } else {
                    if (element.phraseSubOption[y]) {
                        idDb = idDb + element.phraseSubOption[y][0].id.toString() + ',';
                    }
                }
                y += 1;
                let yy = 0;
                if (so && so.selectedPhraseSubOption) {
                    let ii;
                    idDb2 = '';
                    for (ii = 0; ii < so.selectedPhraseSubOption.length; ii++) {
                        // so.selectedPhraseSubOption.forEach(e2 => {
                        const e2 = <PhraseDTO>so.selectedPhraseSubOption[ii];
                        if (e2 && e2.id) {
                            idDb2 = idDb2 + e2.id.toString() + ',';
                        } else {
                            idDb2 = idDb2 + so.phraseSubOption[yy][0].id.toString() + ',';
                        }
                        yy += 1;
                    }
                    if (idDb2 !== '') {
                        idDb2 = '[' + idDb2 + ']';
                        idDb2 = idDb2.replace(',]', '],');
                        const toreplace = so.id.toString();
                        idDb = idDb.replace(toreplace + ',', toreplace + idDb2);
                    }
                }
            }

            if (idDb !== '') {
                idDb = '[' + idDb + ']';
                idDb = idDb.replace(',]', ']');
                const toreplace = element.id.toString();
                this.sample = this.sample.replace(toreplace, toreplace + idDb);
            }
        });

        this.sample = this.sample + ']';
        this.sample = this.sample.replace(/,]/g, ']');
        const checkDef = this.sample.match(/\[\[([^\]]+)\]\]/);
        if (checkDef) {
            const toreplace = checkDef[0];
            let defok = toreplace.replace('[[', '[');
            defok = defok.replace(']]', ']');
            this.sample = this.sample.replace(toreplace, defok);
        }

        // some check before translate
        let numOpts = 0;
        this.selectedPhraseOption.forEach(element => {
            const val = element.value;
            if (val) {
                numOpts += val.split('{').length - 1;
            }
            if (element.selectedPhraseSubOption.length > 0) {
                let i;
                for (i = 0; i < element.selectedPhraseSubOption.length; i++) {
                    const e = <PhraseDTO>element.selectedPhraseSubOption[i];
                    if (e) {
                        const val2 = e.value;
                        if (val2) {
                            numOpts += val2.split('{').length - 1;
                        }
                    }
                }
            }
        });

        let numSubopts = 0;
        this.sample.split(',').forEach(element => {
            const val = element;
            numSubopts += val.split('[').length - 1;
        });

        if (this.sample.split(',').length + numSubopts - 1 < this.selectedPhraseOption.length + numOpts) {
            this.loading = false;
            return;
        }

        this.sentenceService.translate('de,it,en,fr,es,ca,oc', this.sentence.domain.name, this.sample).subscribe(
            res => {
                this.loading = false;
                this.texts = res.body.texts;
            },
            err => {
                this.loading = false;
                this.texts = {};
                this.messageService.add({
                    severity: 'error'
                });
            }
        );
    }
}
