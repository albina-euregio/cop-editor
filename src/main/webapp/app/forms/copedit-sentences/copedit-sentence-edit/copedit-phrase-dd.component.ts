import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PhraseOptionDTO } from 'app/shared/copedit-dto/phraseOptionDTO.model';
import { PhraseDTO } from 'app/shared/copedit-dto/phraseDTO.model';
import { SentenceDTO } from 'app/shared/copedit-dto/sentenceDTO.model';
import { PhraseLanguageDTO } from 'app/shared/copedit-dto/phraseLanguageDTO.model';
import { SentenceLanguageDTO } from 'app/shared/copedit-dto/sentenceLanguageDTO.model';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { faSitemap } from '@fortawesome/free-solid-svg-icons';
import { UpdatePhraseOptionSharedService } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-sentences-update-phraseOption.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';
import { structureRegex } from 'app/shared/constants/copedit.constants';
import { ActivatedRoute, Router } from '@angular/router';
import { CopEditPhraseService } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { PhraseOption } from 'app/shared/model/phrase-option.model';

@Component({
    selector: 'jhi-copedit-phrase-dd',
    templateUrl: 'copedit-phrase-dd.component.html',
    providers: [MessageService]
})
export class CopeditPhraseOptionddComponent implements OnInit {
    faEdit = faEdit;
    faPlusSquare = faPlusSquare;
    faSitemap = faSitemap;

    options: any = {
        removeOnSpill: true
    };

    idxOpt3: Array<number>;
    subsubnames: Array<Array<string>>;

    @Input('subPhraseOptions') subPhraseOptions: Array<Array<PhraseDTO>>;
    @Input('idx1') idx1: number;
    @Input('idx2') idx2: number;
    @Input('sample') sample: string;
    @Input('selectedPhraseOption') selectedPhraseOption: Array<PhraseDTO>;
    @Input('lang') lang: string;
    @Input('subnames') subnames: Array<String>;
    @Output() sampleValueChange = new EventEmitter();

    changeSampleValue() {
        this.sampleValueChange.emit(this.sample);
    }

    constructor(
        private updatePhraseOptionSharedService: UpdatePhraseOptionSharedService,
        private messageService: MessageService,
        private router: Router,
        private copEditPhraseService: CopEditPhraseService
    ) {}

    ngOnInit() {
        this.idxOpt3 = new Array();
        this.subsubnames = new Array();
        const debug = '';
    }

    changeSubOption($event, idx) {
        const val = $event.value.value;
        const id = $event.value.id;
        const idxOpt = this.subPhraseOptions[idx].findIndex(x => x.id === id);
        this.idxOpt3[idx] = idxOpt;
        const dd = new Array();
        if (val && val.indexOf('{') >= 0) {
            const str = val.split('{');
            str.forEach(element => {
                if (element.indexOf('}') >= 0) {
                    dd.push(element.split('}')[0]);
                }
            });
            this.subsubnames[idx] = dd;
            const po = this.subPhraseOptions[idx][idxOpt];
            if (!Array.isArray(po.phraseSubOption)) {
                po.phraseSubOption = new Array();
            }
            if (!Array.isArray(po.selectedPhraseSubOption)) {
                po.selectedPhraseSubOption = new Array();
            }
            let item_NO = -1;
            for (let i = 0; i < dd.length; i++) {
                // this.idxOpt3[i] = 0;
                const checkval = dd[i];
                if (checkval.indexOf('_NO') > 0) {
                    dd[i] = checkval.replace('_NO', '');
                    item_NO = i;
                }
                this.copEditPhraseService.findPhraseByNameOld(dd[i]).subscribe(x => {
                    if (!Array.isArray(po.phraseSubOption[i])) {
                        po.phraseSubOption[i] = new Array();
                    }
                    if (!Array.isArray(po.selectedPhraseSubOption[i])) {
                        po.selectedPhraseSubOption[i] = new Array();
                    }
                    if (this.lang === 'DE') {
                        this.updateSubPhraseValues(po, x.body.phrasesDE, i);
                    } else {
                        this.updateSubPhraseValues(po, x.body.phrasesIT, i);
                    }
                });
            }
        }
        // sync the _NO
        const dd2 = new Array();
        const find_NO = this.selectedPhraseOption[this.idx1].value.split('{');
        let val_no = '';
        find_NO.forEach(element => {
            if (element.indexOf('}') >= 0) {
                if (element.split('}')[0].indexOf('_NO') > 0) {
                    val_no = element.split('}')[0].replace('_NO', '');
                }
                dd2.push(element.split('}')[0]);
            }
        });

        // find the _no subphrase
        if ($event.value.itemPartNo === 2) {
            const same = dd2.findIndex(x => x === val_no);
            this.selectedPhraseOption[this.idx1].selectedPhraseSubOption[same] = this.subPhraseOptions[same][idxOpt];
        } else {
            const same = dd2.findIndex(x => x === val_no + '_NO');
            if (same > -1) {
                this.selectedPhraseOption[this.idx1].selectedPhraseSubOption[same] = this.subPhraseOptions[same][idxOpt];
            }
        }
        this.changeSampleValue();
    }

    // this.selectedPhraseOption[""0""].selectedPhraseSubOption

    updateSubPhraseValues(po, p, i) {
        let y = 0;
        p.phrases.forEach(element => {
            if (!element.phrase.value) {
                element.phrase.value = '   _   ';
            }
            // po.phraseSubOption[i] = new Array();
            po.phraseSubOption[i].push(element.phrase);
            if (y === 0) {
                po.selectedPhraseSubOption[i] = po.phraseSubOption[i][0];
                this.changeSampleValue();
            }
            y += 1;
        });
    }
}
