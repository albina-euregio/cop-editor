import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SentenceDTO } from 'app/shared/copedit-dto/sentenceDTO.model';
import { ActivatedRoute } from '@angular/router';
import { PhraseOptionDTO } from '../../../shared/copedit-dto/phraseOptionDTO.model';

import { PhraseOptionEditDTO } from 'app/shared/copedit-dto/phraseEditDTO.model';
import { CopEditPhraseService } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { DatePipe, Location } from '@angular/common';
import { CopeditSentencesService } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { MessageService } from '../../../../../../../node_modules/primeng/components/common/messageservice';
import { MenuItem, ConfirmationService } from '../../../../../../../node_modules/primeng/api';
import { TranslateService } from '@ngx-translate/core';

// For preview iframe
import { Renderer2 } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { TEXTCAT_API } from 'app/app.constants';
import { forEach } from '@angular/router/src/utils/collection';
import { Observable } from 'rxjs';
import { CopeditPhraseOptionEditorPvComponent } from './copedit-phrase-option-editor-pv.component';

@Component({
    selector: 'jhi-sentence-edit',
    templateUrl: 'copedit-sentence-edit.component.html',
    providers: [MessageService]
})
export class CopeditSentenceEditComponent implements OnInit, AfterViewInit {
    sentence: SentenceDTO;
    phraseOptionsDE: Array<PhraseOptionDTO>;
    phraseOptionsIT: Array<PhraseOptionDTO>;
    phraseOptionsEN: Array<PhraseOptionDTO>;
    phraseOptionsFR: Array<PhraseOptionDTO>;
    phraseOptionsES: Array<PhraseOptionDTO>;
    phraseOptionsCA: Array<PhraseOptionDTO>;
    phraseOptionsOC: Array<PhraseOptionDTO>;
    index: Array<number>;
    dataAgg: string;
    allLanguages: boolean;
    breadcrumbs: MenuItem[];
    home: MenuItem;

    public pmUrl: SafeUrl;
    sample: string;
    public phraseid: string;

    showProgressModal: Boolean = false;
    @ViewChild('receiver') receiver: ElementRef;
    @ViewChild('previewSentence') previewSentence: CopeditPhraseOptionEditorPvComponent;
    display: Boolean = false;

    isInited: boolean;

    showDialog() {
        this.display = true;
    }
    hideDialog() {
        this.display = false;
    }

    constructor(
        private route: ActivatedRoute,
        private sentenceService: CopeditSentencesService,
        private phraseService: CopEditPhraseService,
        private messageService: MessageService,
        private location: Location,
        private translateService: TranslateService,
        private renderer: Renderer2,
        private sanitizer: DomSanitizer,
        private confirmationService: ConfirmationService
    ) {
        // this.stopListening = renderer.listen('window', 'message', this.getText.bind(this));
    }

    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: [''] };
        this.breadcrumbs = [{ label: 'Sentences', routerLink: ['/sentencesList'] }, { label: 'Edit sentence' }];
        this.allLanguages = true;
        this.index = [0];
        this.sentence = this.route.snapshot.data.sentence;
        this.phraseOptionsDE = this.route.snapshot.data.phraseOptionsDE;
        this.phraseOptionsIT = this.route.snapshot.data.phraseOptionsIT;
        this.phraseOptionsFR = this.route.snapshot.data.phraseOptionsFR;
        this.phraseOptionsEN = this.route.snapshot.data.phraseOptionsEN;
        this.phraseOptionsES = this.route.snapshot.data.phraseOptionsES;
        this.phraseOptionsCA = this.route.snapshot.data.phraseOptionsCA;
        this.phraseOptionsOC = this.route.snapshot.data.phraseOptionsOC;
        const datePipe = new DatePipe('en-US');
        this.dataAgg = datePipe.transform(this.sentence.sentencesDE.updatedDate, 'dd/MM/yyyy HH:mm');
        this.pmUrl = this.sanitizer.bypassSecurityTrustResourceUrl(TEXTCAT_API + 'preview_pm.html?l=de&d=' + this.sentence.domain.name);
        this.display = false;
    }

    ngAfterViewInit() {
        this.isInited = true;
    }

    updateName($event) {
        const newname = $event.target.value;
        this.sentence.sentencesIT.name = newname;
        this.sentence.sentencesEN.name = newname;
        this.sentence.sentencesFR.name = newname;
        this.sentence.sentencesES.name = newname;
        this.sentence.sentencesCA.name = newname;
        this.sentence.sentencesOC.name = newname;
    }

    saveSentence() {
        /*  prevent from saving if any structure is invalid  */
        let structuresValid: Boolean = true;
        structuresValid = structuresValid && this.sentenceService.isStructureValid(this.sentence.sentencesDE);
        structuresValid = structuresValid && this.sentenceService.isStructureValid(this.sentence.sentencesIT);
        structuresValid = structuresValid && this.sentenceService.isStructureValid(this.sentence.sentencesEN);
        structuresValid = structuresValid && this.sentenceService.isStructureValid(this.sentence.sentencesFR);
        structuresValid = structuresValid && this.sentenceService.isStructureValid(this.sentence.sentencesES);
        structuresValid = structuresValid && this.sentenceService.isStructureValid(this.sentence.sentencesCA);
        structuresValid = structuresValid && this.sentenceService.isStructureValid(this.sentence.sentencesOC);

        if (!structuresValid) {
            this.messageService.add({
                severity: 'error',
                detail: this.translateService.instant('appApp.sentenceEdit.messages.invalidStructureMessage') // 'Unable to save sentence: one or more structures are invalid'
            });
            return;
        }

        /* save new sentence */
        if (this.sentence.sentencesDE.id === null) {
            this.sentenceService.create(this.sentence).subscribe(
                res => {
                    this.sentence = res.body;
                    this.messageService.add({
                        severity: 'success',
                        detail: this.translateService.instant('appApp.sentenceEdit.messages.saveSuccessMessage')
                    });
                    this.location.replaceState('/sentenceEdit/' + this.sentence.sentencesDE.name);
                },
                err => {
                    console.error(err);
                    if (err.hasOwnProperty('status') && err.status === 400) {
                        this.messageService.add({ severity: 'error', detail: err.error.message });
                    } else if (err.hasOwnProperty('status') && err.status === 500) {
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceEdit.messages.saveErrorMessage')
                        });
                    } else {
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceEdit.messages.saveErrorMessage')
                        });
                    }
                }
            );
        } else {
            /* update existing sentence */
            this.sentenceService.update(this.sentence).subscribe(
                res => {
                    this.sentence = res.body;
                    this.messageService.add({
                        severity: 'success',
                        detail: this.translateService.instant('appApp.sentenceEdit.messages.saveSuccessMessage')
                    });
                },
                err => {
                    console.error(err);
                    if (err.hasOwnProperty('status') && err.status === 400) {
                        this.messageService.add({ severity: 'error', detail: err.error.message });
                    } else {
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceEdit.messages.saveErrorMessage')
                        });
                    }
                }
            );
        }
        this.reloadTexcat(null, '', 'de', this.sentence.sentencesDE.id);
    }

    onIframeLoad($event, field, l, txtDef) {
        if (this.isInited) {
            setTimeout(() => {
                const receiver = this.receiver.nativeElement.contentWindow;
                // $event.preventDefault();
                // make Json to send to pm
                const inputDef = {
                    textField: field,
                    textDef: txtDef + '[]',
                    srcLang: 'de',
                    currentLang: 'de'
                };
                const pmData = JSON.stringify(inputDef);
                receiver.postMessage(pmData, '*');
            }, 500);
        }
    }

    loadPreview($event, field, l, txtDef) {
        // open modal to test sentence
        this.showDialog();
    }

    reloadTexcat($event, field, l, txtDef) {
        this.confirmationService.confirm({
            header: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessageHeader'),
            message: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessage', {
                lang: l,
                domain: this.sentence.domain.name
            }),
            icon: 'fa fa-warning',
            accept: () => {
                this.showProgressModal = true;
                this.sentenceService.reloadTexcat(l, this.sentence.domain.name).subscribe(
                    res => {
                        this.previewSentence.ngOnInit();
                        this.showProgressModal = false;
                    },
                    err => {
                        this.showProgressModal = false;
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadErrorMessage')
                        });
                    }
                );
            },
            reject: () => {
                this.showProgressModal = false;
            }
        });
    }

    trySentence($event, field, l, txtDef) {
        this.confirmationService.confirm({
            header: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessageHeader'),
            message: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessage', {
                lang: l,
                domain: this.sentence.domain.name
            }),
            icon: 'fa fa-warning',
            accept: () => {
                this.showProgressModal = true;
                this.sentenceService.reloadTexcat(l, this.sentence.domain.name).subscribe(
                    res => {
                        this.showProgressModal = false;
                        this.loadPreview($event, field, l, txtDef);
                    },
                    err => {
                        this.showProgressModal = false;
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadErrorMessage')
                        });
                    }
                );
            },
            reject: () => {
                this.showProgressModal = false;
                this.loadPreview($event, field, l, txtDef);
            }
        });
    }

    getText(e) {
        e.preventDefault();
        if (e.data.type !== 'webpackInvalid' && e.data.type !== 'webpackOk') {
            const pmData = JSON.parse(e.data);

            this[pmData.textField + 'Textcat'] = pmData.textDef;
            this[pmData.textField + 'It'] = pmData.textIt;
            this[pmData.textField + 'De'] = pmData.textDe;
            this[pmData.textField + 'En'] = pmData.textEn;
            this[pmData.textField + 'Fr'] = pmData.textFr;
            // TODO? ES CA OC

            this.hideDialog();
        }
    }
}
