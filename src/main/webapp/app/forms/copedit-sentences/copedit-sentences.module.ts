import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CopeditLookupService } from 'app/forms/copedit-shared/copedit-lookup.service';
import { CopEditPhraseService } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { copeditSentenceRoute } from 'app/forms/copedit-sentences/copedit-sentences.route';
import { SentencesListComponent } from 'app/forms/copedit-sentences/copedit-sentences-list/copedit-sentences-list.component';
import {
    VersionsResolver,
    DomainsResolver,
    PhrasesResolver,
    CopeditSentenceResolver,
    CopeditSentencesService,
    CopeditNewSentenceResolver
} from 'app/forms/copedit-sentences/copedit-sentences.service';
import { CopeditSentenceEditComponent } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-sentence-edit.component';
import { AccordionModule, TabViewModule, DragDropModule } from 'primeng/primeng';
import { CopeditPhraseOptionEditorComponent } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-phrase-option-editor.component';
import { CopeditPhraseOptionEditorPvComponent } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-phrase-option-editor-pv.component';
import { CopeditPhraseOptionddComponent } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-phrase-dd.component';
import { CopeditPhraseOptiondd2Component } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-phrase-dd2.component';
import { CardModule } from 'primeng/card';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { DragulaModule } from 'ng2-dragula';
import { UpdatePhraseOptionSharedService } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-sentences-update-phraseOption.service';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { GrowlModule } from 'primeng/growl';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import {
    CopEditPhraseOptionsService,
    PhraseOptionsDEResolver,
    PhraseOptionsITResolver,
    PhraseOptionsENResolver,
    PhraseOptionsFRResolver,
    PhraseOptionsESResolver,
    PhraseOptionsCAResolver,
    PhraseOptionsOCResolver
} from 'app/forms/copedit-shared/copedit-phrase-options.service';
import { DialogModule } from 'primeng/dialog';
import { ListboxModule } from 'primeng/listbox';

const ENTITY_STATES = [...copeditSentenceRoute];

@NgModule({
    imports: [
        AppSharedModule,
        FieldsetModule,
        CheckboxModule,
        InputTextModule,
        DropdownModule,
        ButtonModule,
        TableModule,
        BreadcrumbModule,
        AccordionModule,
        TabViewModule,
        DragDropModule,
        CardModule,
        AutoCompleteModule,
        RouterModule.forChild(ENTITY_STATES),
        DragulaModule,
        MessagesModule,
        MessageModule,
        GrowlModule,
        ConfirmDialogModule,
        DialogModule,
        ListboxModule
    ],
    // exports: [SentencesListComponent],
    declarations: [
        SentencesListComponent,
        CopeditSentenceEditComponent,
        CopeditPhraseOptionEditorComponent,
        CopeditPhraseOptionEditorPvComponent,
        CopeditPhraseOptionddComponent,
        CopeditPhraseOptiondd2Component
    ],
    providers: [
        CopeditSentencesService,
        CopeditLookupService,
        VersionsResolver,
        DomainsResolver,
        CopEditPhraseService,
        PhrasesResolver,
        CopeditSentenceResolver,
        PhraseOptionsDEResolver,
        PhraseOptionsITResolver,
        PhraseOptionsENResolver,
        PhraseOptionsFRResolver,
        PhraseOptionsESResolver,
        PhraseOptionsCAResolver,
        PhraseOptionsOCResolver,
        UpdatePhraseOptionSharedService,
        CopeditNewSentenceResolver,
        ConfirmationService,
        CopEditPhraseOptionsService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopeditSentencesModule {}
