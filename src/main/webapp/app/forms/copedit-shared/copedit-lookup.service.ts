import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { IVersion, Version } from 'app/shared/model/version.model';
import { Domain } from 'app/shared/model/domain.model';

type EntityResponseType = HttpResponse<IVersion>;
type EntityArrayResponseType = HttpResponse<IVersion[]>;

@Injectable({ providedIn: 'root' })
export class CopeditLookupService {
    private resourceUrl = SERVER_API_URL + 'api/copedit/lookup';

    constructor(private http: HttpClient) { }

    getVersions(): Observable<EntityArrayResponseType> {
        return this.http.get<Version[]>(`${this.resourceUrl}/versions`, { observe: 'response' });
    }

    getDomains(): Observable<HttpResponse<Domain[]>> {
        return this.http.get<Domain[]>(`${this.resourceUrl}/domains`, { observe: 'response' });
    }
}
