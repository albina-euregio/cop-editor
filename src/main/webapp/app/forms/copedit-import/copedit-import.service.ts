import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class CopeditImportService {
    private resourceUrl = SERVER_API_URL + 'api/copedit/import';

    constructor(private http: HttpClient) {}

    uploadFiles(files: File[], domain: string) {
        const formData = new FormData();
        files.forEach(f => {
            formData.append('files[]', f);
        });
        const head = new Headers();
        head.append('Content-Type', 'multipart/form-data');
        head.append('Accept', 'application/json');
        return this.http.post<any>(this.resourceUrl + `?domain=${domain}`, formData, { observe: 'response' });
    }

    getBranches() {
        const head = new Headers();
        head.append('Accept', 'application/json');
        return this.http.get<any>(this.resourceUrl + '/git/getRemoteBranches', { observe: 'response' });
    }

    initBranch() {
        return this.http.get<any>(this.resourceUrl + '/git/initBranch', { observe: 'response' });
    }

    selectBranch(localBranch: string) {
        let options: HttpParams = new HttpParams();
        options = options.set('lb', localBranch);
        return this.http.get<any>(this.resourceUrl + '/git/selectBranch', { params: options, observe: 'response' });
    }

    importFromRepoGit(domainName: string) {
        let options: HttpParams = new HttpParams();
        options = options.set('domain', domainName);
        return this.http.get<any>(this.resourceUrl + '/git/importFromRepoGit', { params: options, observe: 'response' });
    }
}
