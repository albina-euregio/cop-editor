import { UserRouteAccessService } from 'app/core';
import { CopeditImportComponent } from 'app/forms/copedit-import/copedit-import.component';
import { Routes } from '@angular/router';
import { DomainsResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';

export const copeditImportRoute: Routes = [
    {
        path: 'import',
        component: CopeditImportComponent,
        resolve: {
            domains: DomainsResolver
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'appApp.import.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
