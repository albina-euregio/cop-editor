import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { copeditPhrasesRoute } from 'app/forms/copedit-phrases/copedit-phrases.route';
import { CopeditPhrasesListComponent } from 'app/forms/copedit-phrases/copedit-phrases-list/copedit-phrases-list.component';
import { CopEditPhraseService, PhraseResolver, NewPhraseResolver } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { BreadcrumbModule } from 'primeng/components/breadcrumb/breadcrumb';
import { CopeditPhraseEditComponent } from 'app/forms/copedit-phrases/copedit-phrase-edit/copedit-phrase-edit.component';
import { MessageModule } from 'primeng/message';
import { GrowlModule } from 'primeng/growl';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { CopeditPhraseItemEditorComponent } from 'app/forms/copedit-phrases/copedit-phrase-edit/copedit-phrase-item-editor/copedit-phrase-item.editor.component';
import { TabViewModule } from '../../../../../../node_modules/primeng/primeng';
import { DragulaModule } from '../../../../../../node_modules/ng2-dragula';
import { CopeditScrollSharedService } from 'app/forms/copedit-phrases/copedit-phrase-edit/copedit-phrase-item-editor/copedit-scroll-shared.service';
import { CopeditPhraseOptionPipe } from 'app/forms/copedit-phrases/copedit-phrase-edit/copedit-phrase-item-editor/copedit-phraseOption.pipe';

const ENTITY_STATES = [...copeditPhrasesRoute];

@NgModule({
    imports: [
        AppSharedModule,
        FieldsetModule,
        CheckboxModule,
        InputTextModule,
        DropdownModule,
        ButtonModule,
        TableModule,
        BreadcrumbModule,
        MessageModule,
        GrowlModule,
        DragulaModule,
        ConfirmDialogModule,
        TabViewModule,
        RouterModule.forChild(ENTITY_STATES),
        DialogModule
    ],
    declarations: [CopeditPhrasesListComponent, CopeditPhraseEditComponent, CopeditPhraseItemEditorComponent, CopeditPhraseOptionPipe],
    providers: [CopEditPhraseService, ConfirmationService, PhraseResolver, CopeditScrollSharedService, NewPhraseResolver],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopeditPhraseModule {}
