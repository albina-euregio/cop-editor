import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MenuItem } from 'primeng/components/common/menuitem';
import { Phrase } from '../../../shared/model/phrase.model';
import { Domain } from 'app/shared/model/domain.model';
import { Version } from 'app/shared/model/version.model';
import { CopEditPhraseService } from 'app/forms/copedit-phrases/copedit-phrase.service';
import { JhiParseLinks } from 'ng-jhipster';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from '../../../../../../../node_modules/ngx-webstorage';
import { DomainDTO } from 'app/shared/copedit-dto/domainDTO.model';
import { PhraseOptionListItemDTO } from 'app/shared/copedit-dto/phraseOptionListItemDTO.model';
import { MessageService } from '../../../../../../../node_modules/primeng/components/common/messageservice';
import { ConfirmationService } from '../../../../../../../node_modules/primeng/api';
import { SidebarService } from 'app/shared/sidebar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-copedit-phrases-list',
    templateUrl: 'copedit-phrases-list.component.html',
    providers: [MessageService]
})
export class CopeditPhrasesListComponent implements OnInit {
    viewDeleted: Boolean;
    viewJoker: Boolean;
    breadcrumbs: MenuItem[];
    home: MenuItem;
    phrases: Phrase[];
    loading: Boolean;
    totalRecords: number;
    currentPageNr;
    first: any;
    versions: Array<Version>;
    domains: Array<Domain>;
    positions: Array<any>;
    phraseOptions: Array<PhraseOptionListItemDTO>;
    @ViewChild('dt') dt;
    @LocalStorage('currentDomain') public currentDomain: DomainDTO;

    constructor(
        private copeditPhrasesService: CopEditPhraseService,
        private parseLinks: JhiParseLinks,
        private route: ActivatedRoute,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private router: Router,
        private sidebarService: SidebarService,
        private translateService: TranslateService
    ) {}

    ngOnInit() {
        this.sidebarService.showSidebar(false);
        this.home = { icon: 'pi pi-home', routerLink: [''] };
        this.first = 0;
        this.breadcrumbs = [{ label: 'Phrases' }];
        /* init filter on domain */
        this.dt.filters = {
            domain: { value: this.currentDomain.id, matchMode: 'equals' }
        };

        this.versions = this.route.snapshot.data.versions;
        this.domains = this.route.snapshot.data.domains;
        this.phraseOptions = this.route.snapshot.data.phraseOptions;
        this.loading = true;
    }

    loadNextPage($event) {
        /* compose search filters and pass to service */
        this.currentPageNr = $event.first / $event.rows;
        const filter = {
            page: this.currentPageNr,
            size: 20
        };
        /* add sorting */
        if ($event.multiSortMeta !== undefined) {
            $event.multiSortMeta.forEach(field => {
                let dir;
                if (field.order === 1) {
                    dir = 'asc';
                } else if (field.order === -1) {
                    dir = 'desc';
                }
                filter['sort'] = [field.field + ',' + dir];
            });
        }
        Object.keys($event.filters).forEach(element => {
            filter[element] = $event.filters[element].value;
        });
        this.loading = true;
        this.copeditPhrasesService.findAll(filter).subscribe(
            res => {
                (<any>this.phrases) = res.body;
                this.totalRecords = Number(res.headers.get('x-total-count'));
                this.loading = false;
            },
            (err: HttpErrorResponse) => {
                console.error(err);
            }
        );
    }

    filterData($event) {
        /* compose search filters and pass to service */
        const filter = {
            page: this.currentPageNr,
            size: 20,
            domain: this.currentDomain.id
        };
        Object.keys($event.filters).forEach(element => {
            filter[element] = $event.filters[element].value;
        });
        /* add sorting */
        if ($event.multisortmeta !== undefined) {
            $event.multisortmeta.forEach(field => {
                let dir;
                if (field.order === 1) {
                    dir = 'asc';
                } else if (field.order === -1) {
                    dir = 'desc';
                }
                filter['sort'] = [field.field + ',' + dir];
            });
        }
        this.copeditPhrasesService.findAll(filter).subscribe(
            res => {
                (<any>this.phrases) = res.body;
                (<any>this.phrases) = res.body;
                this.totalRecords = Number(res.headers.get('x-total-count'));
                this.loading = false;
            },
            (err: HttpErrorResponse) => {
                console.error(err);
            }
        );
    }

    sortPhraseOptions($event) {
        this.loading = true;
        const filter = {
            page: this.currentPageNr,
            size: 20
        };
        /* add sorting */
        if ($event.multisortmeta !== undefined) {
            $event.multisortmeta.forEach(field => {
                let dir;
                if (field.order === 1) {
                    dir = 'asc';
                } else if (field.order === -1) {
                    dir = 'desc';
                }
                filter['sort'] = [field.field + ',' + dir];
            });
        }
        if ($event.hasOwnProperty('filters')) {
            Object.keys($event.filters).forEach(element => {
                filter[element] = $event.filters[element].value;
            });
        }

        this.copeditPhrasesService.findAll(filter).subscribe(
            res => {
                (<any>this.phrases) = res.body;
                this.totalRecords = Number(res.headers.get('x-total-count'));
                this.loading = false;
            },
            (err: HttpErrorResponse) => {
                console.error(err);
            }
        );
    }

    createNewPhrase() {
        this.router.navigate(['newPhrase']);
    }

    editPhrase(name) {
        this.router.navigate(['phraseEdit', name]);
    }

    confirmDelete(id: number, phraseName: string) {
        this.confirmationService.confirm({
            message: this.translateService.instant('appApp.phrasesList.grid.messages.deleteConfirmationMessage', { name: phraseName }),
            accept: () => {
                this.copeditPhrasesService.delete(id).subscribe(
                    res => {
                        this.filterData({ filters: this.dt.filters });
                        this.messageService.add({
                            severity: 'success',
                            detail: this.translateService.instant('appApp.phrasesList.grid.messages.deleteSuccessMessage', {
                                name: phraseName
                            })
                        });
                    },
                    err => {
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.phrasesList.grid.messages.deleteErrorMessage')
                        });
                        console.error(err);
                    }
                );
            }
        });
    }
}
