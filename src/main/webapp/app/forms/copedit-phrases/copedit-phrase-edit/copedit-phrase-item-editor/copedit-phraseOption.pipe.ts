import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'phraseOptionPipe'
})
export class CopeditPhraseOptionPipe implements PipeTransform {
    transform(items: any[], filters: any, filter: number): any {
        if (!items || !filter) {
            return items;
        }
        return items.filter(x => x.phrase.itemPartNo === filter);
    }
}
