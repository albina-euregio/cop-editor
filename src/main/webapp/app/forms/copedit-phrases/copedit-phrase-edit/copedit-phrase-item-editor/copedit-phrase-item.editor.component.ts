import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { PhraseDTO } from 'app/shared/copedit-dto/phraseDTO.model';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { CopeditScrollSharedService } from 'app/forms/copedit-phrases/copedit-phrase-edit/copedit-phrase-item-editor/copedit-scroll-shared.service';
import { ConfirmationService } from '../../../../../../../../node_modules/primeng/api';
import { PhraseOptionItemDTO } from 'app/shared/copedit-dto/phraseOptionItemDTO.model';

@Component({
    selector: 'jhi-copedit-phrase-item-editor',
    templateUrl: 'copedit-phrase-item.editor.component.html',
    styleUrls: ['copedit-phrase-item-editor.scss']
})
export class CopeditPhraseItemEditorComponent implements OnInit {
    @Input('language') language;
    @Input('phrases') phrases: Array<PhraseOptionItemDTO>;
    @Input('phrasesItemPartNo2') phrasesItemPartNo2: Array<PhraseOptionItemDTO>;
    @ViewChild('scroll') scroll: ElementRef;
    @Output() onExcpetionRemoval = new EventEmitter<string>();
    dragulaOptions: any = {
        removeOnSpill: true,
        moves: (el, source, handle, sibling) => {
            return !el.classList.contains('no-drag');
        }
    };

    constructor(
        private dragulaService: DragulaService,
        private copeditScrollSharedService: CopeditScrollSharedService,
        private confirmationService: ConfirmationService
    ) {}

    ngOnInit() {
        /* synchronize scrolling with every language */
        this.copeditScrollSharedService.scrollEvent$.subscribe(res => {
            const scroll = this.scroll.nativeElement as HTMLElement;
            scroll.scrollTop = res;
        });

        this.dragulaService.removeModel.subscribe(value => {
            this.syncItemNo();
            /* this.confirmationService.confirm({
                message: 'Are you sure that you want to remove the phrase?',
                accept: () => {
                    this.syncItemNo();
                },
                reject: () => {
                    this.dragulaService.find('bag-a').drake.cancel(true);
                }
            }); */
        });
        this.dragulaService.drop.subscribe(value => {
            this.syncItemNo();
        });
    }

    /**
     * Emits the event to synchronize the scrolling of every element in synch
     *
     * @param {*} $event scrolling event
     * @memberof CopeditPhraseItemEditorComponent
     */
    emitScroll($event) {
        this.copeditScrollSharedService.sharedScroll($event);
    }

    /**
     * Synchronizes the phrases list setting the itemNo as the index inside the array
     *
     * @param {PhraseLanguageDTO} phraseLang phraseLanguage to set the index
     * @memberof CopeditPhraseEditComponent
     */
    syncItemNo(): void {
        this.phrases.map((x, index) => (x.phrase.itemNo = index + 1));
        this.phrasesItemPartNo2.map((x, index) => (x.phrase.itemNo = index + 1));
    }

    /**
     * Add an exception to the current language, creting an array of the same size of the phrases array and setting itemPartNo = 2 to
     * all elements
     *
     * @memberof CopeditPhraseItemEditorComponent
     */
    addLanguageException() {
        this.phrases.forEach(ph => {
            const item: PhraseOptionItemDTO = new PhraseOptionItemDTO();
            item.itemNo = ph.itemNo;
            item.phraseOptionId = ph.phraseOptionId;
            item.phraseOptionItemId = ph.phraseOptionItemId;
            item.version = ph.version;
            item.basisVersion = ph.basisVersion;
            item.deleted = false;

            const phrase: PhraseDTO = new PhraseDTO();
            phrase.itemPartNo = 2;
            phrase.itemNo = ph.phrase.itemNo;
            phrase.incorrect = false;
            item.phrase = phrase;
            this.phrasesItemPartNo2.push(item);
        });
    }

    /**
     * Removes the exception for the current phrase and notifies it to parent component which updates the model accordingly
     *
     * @memberof CopeditPhraseItemEditorComponent
     */
    removeLanguageException() {
        this.confirmationService.confirm({
            message: `Are you sure that you remove the exception?`,
            accept: () => {
                this.phrasesItemPartNo2 = [];
                this.onExcpetionRemoval.emit(this.language);
            }
        });
    }
}
