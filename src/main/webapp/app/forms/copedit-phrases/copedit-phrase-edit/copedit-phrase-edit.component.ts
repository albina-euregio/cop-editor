import { Component, OnInit, ViewChild, DoCheck, IterableDiffers, ElementRef, AfterViewInit } from '@angular/core';
import { PhraseOptionEditDTO } from 'app/shared/copedit-dto/phraseEditDTO.model';
import { MenuItem, Message, ConfirmationService } from '../../../../../../../node_modules/primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '../../../../../../../node_modules/primeng/components/common/messageservice';
import { PhraseLanguageDTO } from 'app/shared/copedit-dto/phraseLanguageDTO.model';
import { CopeditPhraseItemEditorComponent } from 'app/forms/copedit-phrases/copedit-phrase-edit/copedit-phrase-item-editor/copedit-phrase-item.editor.component';
import { CopEditPhraseService } from '../copedit-phrase.service';
import { PhraseOptionItemDTO } from 'app/shared/copedit-dto/phraseOptionItemDTO.model';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

// For preview iframe
import { Renderer2 } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { CopeditSentencesService } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { TEXTCAT_API } from 'app/app.constants';

@Component({
    selector: 'jhi-copedit-phrase-edit',
    templateUrl: 'copedit-phrase-edit.component.html',
    providers: [MessageService]
})
export class CopeditPhraseEditComponent implements OnInit, DoCheck, AfterViewInit {
    phrase: PhraseOptionEditDTO;
    home: MenuItem;
    breadcrumbs: MenuItem[];
    differ;
    @ViewChild('de') de: CopeditPhraseItemEditorComponent;
    @ViewChild('it') it: CopeditPhraseItemEditorComponent;
    @ViewChild('en') en: CopeditPhraseItemEditorComponent;
    @ViewChild('fr') fr: CopeditPhraseItemEditorComponent;

    public pmUrl: SafeUrl;
    showProgressModal: Boolean = false;
    @ViewChild('receiver') receiver: ElementRef;
    display: Boolean = false;
    isInited: boolean;

    showDialog() {
        this.display = true;
    }
    hideDialog() {
        this.display = false;
    }

    constructor(
        private route: ActivatedRoute,
        private messageService: MessageService,
        private differs: IterableDiffers,
        private phraseService: CopEditPhraseService,
        private location: Location,
        private translateService: TranslateService,
        private renderer: Renderer2,
        private sanitizer: DomSanitizer,
        private router: Router,
        private sentenceService: CopeditSentencesService,
        private confirmationService: ConfirmationService
    ) {
        this.differ = differs.find([]).create(null);
    }

    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: [''] };
        this.breadcrumbs = [{ label: 'Phrases', routerLink: ['/phrasesList'] }, { label: 'Edit phrase' }];
        this.phrase = this.route.snapshot.data.phrase;

        this.display = false;
        this.pmUrl = this.sanitizer.bypassSecurityTrustResourceUrl(TEXTCAT_API + 'preview_pm.html?l=de&d=' + this.phrase.domain.name);
        window.scrollTo(0, 0);
    }

    ngAfterViewInit() {
        this.isInited = true;
    }

    showSentences() {
        this.phraseService.findPhraseByName(this.phrase.phrasesDE.name, true).subscribe(x => {
            this.phrase = x.body;
        });
    }

    /**
     * Update name of phrase in Italian, English and French when German changes
     *
     * @param {*} $event value to update to
     * @memberof CopeditPhraseEditComponent
     */
    updateName($event): void {
        const newname = $event.target.value;
        this.phrase.phrasesIT.name = newname;
        this.phrase.phrasesEN.name = newname;
        this.phrase.phrasesFR.name = newname;
    }

    savePhrase() {
        if (this.isPhraseValid()) {
            if (this.phrase.phrasesDE.id === null) {
                this.phraseService.create(this.phrase).subscribe(
                    res => {
                        this.messageService.add({ severity: 'success', detail: 'Phrase saved successfully!' });
                        this.location.replaceState('/phraseEdit/' + this.phrase.phrasesDE.name);
                        this.phrase = res.body;
                        this.differ = this.differs.find([]).create(null);
                    },
                    err => {
                        if (err.hasOwnProperty('status') && err.status === 400) {
                            this.messageService.add({ severity: 'error', detail: err.error.message });
                        } else if (err.hasOwnProperty('status') && err.status === 500) {
                            this.messageService.add({ severity: 'error', detail: 'An error occurred while saving the phrase' });
                        } else {
                            this.messageService.add({ severity: 'error', detail: 'An error occurred while saving the phrase' });
                        }
                    }
                );
            } else {
                this.phraseService.update(this.phrase).subscribe(
                    res => {
                        this.messageService.add({ severity: 'success', detail: 'Phrase saved successfully!' });
                        this.phrase = res.body;
                        this.differ = this.differs.find([]).create(null);
                    },
                    err => {
                        if (err.hasOwnProperty('status') && err.status === 400) {
                            this.messageService.add({ severity: 'error', detail: err.error.message });
                        } else if (err.hasOwnProperty('status') && err.status === 500) {
                            this.messageService.add({ severity: 'error', detail: 'An error occurred while saving the phrase' });
                        } else {
                            this.messageService.add({ severity: 'error', detail: 'An error occurred while saving the phrase' });
                        }
                    }
                );
            }
        }
        this.reloadTexcat(null, '', 'de', 0);
    }

    /**
     * Adds a new line to every phrase in every language
     *
     * @memberof CopeditPhraseEditComponent
     */
    addLine(): void {
        this.phrase.phrasesDE.phrases.push(new PhraseOptionItemDTO(this.phrase.phrasesDE.id));
        this.phrase.phrasesIT.phrases.push(new PhraseOptionItemDTO(this.phrase.phrasesIT.id));
        this.phrase.phrasesEN.phrases.push(new PhraseOptionItemDTO(this.phrase.phrasesEN.id));
        this.phrase.phrasesFR.phrases.push(new PhraseOptionItemDTO(this.phrase.phrasesFR.id));
        /* add to exceptions as well if present */
        if (this.phrase.phrasesIT.phrasesItemPartNo2.length > 0) {
            this.phrase.phrasesIT.phrasesItemPartNo2.push(new PhraseOptionItemDTO(this.phrase.phrasesFR.id));
        }
        if (this.phrase.phrasesEN.phrasesItemPartNo2.length > 0) {
            this.phrase.phrasesEN.phrasesItemPartNo2.push(new PhraseOptionItemDTO(this.phrase.phrasesFR.id));
        }
        if (this.phrase.phrasesFR.phrasesItemPartNo2.length > 0) {
            this.phrase.phrasesFR.phrasesItemPartNo2.push(new PhraseOptionItemDTO(this.phrase.phrasesFR.id));
        }
        this.syncItemNo(this.phrase.phrasesDE);
        this.syncItemNo(this.phrase.phrasesIT);
        this.syncItemNo(this.phrase.phrasesEN);
        this.syncItemNo(this.phrase.phrasesFR);
    }

    /**
     * Synchronizes the phrases list setting the itemNo as the index inside the array
     *
     * @param {PhraseLanguageDTO} phraseLang phraseLanguage to set the index
     * @memberof CopeditPhraseEditComponent
     */
    syncItemNo(phraseLang: PhraseLanguageDTO): void {
        phraseLang.phrases.map((x, index) => (x.phrase.itemNo = index + 1));
        phraseLang.phrases.map((x, index) => {
            if (x.phrase.itemPartNo === undefined) {
                x.phrase.itemPartNo = 1;
            }
        });
        phraseLang.phrasesItemPartNo2.map((x, index) => {
            if (x.phrase.itemPartNo === undefined) {
                x.phrase.itemPartNo = 2;
            }
        });
    }

    /**
     * Trick to force correct positioning of scrollbar on tab change
     *
     * @memberof CopeditPhraseEditComponent
     */
    changeTab(): void {
        this.de.scroll.nativeElement.scrollTop++;
        this.de.scroll.nativeElement.scrollTop--;
    }

    /**
     * When a phrase is deleted in German, the phrase with the same itemPartNo is
     * deleted in all other languages accordingly. When a phrase is moved to another
     * position in German, the phrase is moved accordingly in all the other languages
     *
     * @memberof CopeditPhraseEditComponent
     */
    ngDoCheck() {
        const change = this.differ.diff(this.phrase.phrasesDE.phrases);
        if (change && change._removalsHead) {
            const removedElement = change._removalsHead.item;
            /* remove from all phrases the one with same itemNo */
            this.removeElementByItemNo(removedElement.itemNo, this.phrase.phrasesIT);
            this.removeElementByItemNo(removedElement.itemNo, this.phrase.phrasesEN);
            this.removeElementByItemNo(removedElement.itemNo, this.phrase.phrasesFR);
            /*  this.it.syncItemNo();
            this.en.syncItemNo();
            this.fr.syncItemNo(); */
        }
        if (change && change._movesHead && !change._removalsHead) {
            const previousIndex = change._movesHead.previousIndex;
            const currentIndex = change._movesHead.currentIndex;
            this.moveElement(previousIndex, currentIndex, this.phrase.phrasesIT);
            this.moveElement(previousIndex, currentIndex, this.phrase.phrasesEN);
            this.moveElement(previousIndex, currentIndex, this.phrase.phrasesFR);
        }
        if (change && (change._removalsHead || change._movesHead)) {
            this.it.syncItemNo();
            this.en.syncItemNo();
            this.fr.syncItemNo();
        }
    }

    /**
     * Deletes the phrase with the given itemPartNo in the given phraseLanguage
     *
     * @param {number} itemNo itemNo to delete
     * @param {PhraseLanguageDTO} lang phraseLanguage to update
     * @memberof CopeditPhraseEditComponent
     */
    removeElementByItemNo(itemNo: number, lang: PhraseLanguageDTO): void {
        const index = lang.phrases.findIndex(x => x.phrase.itemPartNo === itemNo);
        lang.phrases.splice(index, 1);
    }

    /**
     * Swaps the two elements at the given indexes
     *
     * @param {number} previousIndex previous index
     * @param {number} currentIndex current index
     * @param {PhraseLanguageDTO} lang phraseLanguage to reorder
     * @memberof CopeditPhraseEditComponent
     */
    moveElement(previousIndex: number, currentIndex: number, lang: PhraseLanguageDTO): void {
        const pre = lang.phrases[previousIndex];
        const post = lang.phrases[currentIndex];
        // swap elements
        lang.phrases[previousIndex] = post;
        lang.phrases[currentIndex] = pre;

        if (lang.phrasesItemPartNo2.length > 0) {
            const preItem2 = lang.phrasesItemPartNo2[previousIndex];
            const postItem2 = lang.phrasesItemPartNo2[currentIndex];
            // swap elements
            lang.phrasesItemPartNo2[previousIndex] = postItem2;
            lang.phrasesItemPartNo2[currentIndex] = preItem2;
        }
    }

    /**
     * Checks whether the phrase is complete for save or not
     *
     * @returns {Boolean}
     * @memberof CopeditPhraseEditComponent
     */
    isPhraseValid(): Boolean {
        const errorMsg = Array<Message>();

        let isValid = true;
        /* all values in phrases in every language are required */
        /*  if (this.phrase.phrasesDE.phrases.some(x => x.phrase.value === undefined || x.phrase.value === '')) {
             errorMsg.push({ severity: 'error', detail: 'All German phrases must have a value' });
             isValid = false;
         }
         if (this.phrase.phrasesIT.phrases.some(x => x.phrase.value === undefined || x.phrase.value === '')) {
             errorMsg.push({ severity: 'error', detail: 'All Italian phrases must have a value' });
             isValid = false;
         }
         if (this.phrase.phrasesEN.phrases.some(x => x.phrase.value === undefined || x.phrase.value === '')) {
             errorMsg.push({ severity: 'error', detail: 'All English phrases must have a value' });
             isValid = false;
         }
         if (this.phrase.phrasesFR.phrases.some(x => x.phrase.value === undefined || x.phrase.value === '')) {
             errorMsg.push({ severity: 'error', detail: 'All French phrases must have a value' });
             isValid = false;
         } */
        /*  all headers and name in German are required */
        if (this.phrase.phrasesDE.name === '' || this.phrase.phrasesDE.name === undefined) {
            errorMsg.push({ severity: 'error', detail: 'The name of the phrase is required' });
            isValid = false;
        }
        const arr = [
            this.phrase.phrasesDE.header,
            this.phrase.phrasesIT.header,
            this.phrase.phrasesEN.header,
            this.phrase.phrasesFR.header
        ];
        /* check if any header is empty */
        const hasInvalidHeader: Boolean = arr.some(x => x === undefined || x === '');
        if (hasInvalidHeader) {
            errorMsg.push({ severity: 'error', detail: 'All headers are required' });
            isValid = false;
        }
        /* At least one phrase per language must be present */
        /* if (
            this.phrase.phrasesDE.phrases.length === 0 ||
            this.phrase.phrasesEN.phrases.length === 0 ||
            this.phrase.phrasesIT.phrases.length === 0 ||
            this.phrase.phrasesFR.phrases.length === 0
        ) {
            errorMsg.push({ severity: 'error', detail: 'Every language must have at least one phrase' });
            isValid = false;
        } */
        if (!isValid) {
            this.messageService.addAll(errorMsg);
        }
        return isValid;
    }

    emptyExceptionsArray(lang: string) {
        switch (lang) {
            case 'it':
                this.phrase.phrasesIT.phrasesItemPartNo2 = [];
                break;
            case 'en':
                this.phrase.phrasesEN.phrasesItemPartNo2 = [];
                break;
            case 'fr':
                this.phrase.phrasesFR.phrasesItemPartNo2 = [];
                break;
        }
    }

    onIframeLoad($event, field, l, txtDef) {
        if (this.isInited) {
            setTimeout(() => {
                const receiver = this.receiver.nativeElement.contentWindow;
                $event.preventDefault();
                // make Json to send to pm
                const inputDef = {
                    textField: field,
                    textDef: txtDef + '[]',
                    srcLang: 'de',
                    currentLang: 'de'
                };
                const pmData = JSON.stringify(inputDef);
                receiver.postMessage(pmData, '*');
            }, 500);
        }
    }

    loadPreview($event, field, l, txtDef) {
        this.onIframeLoad($event, field, l, txtDef);
        this.showDialog();
    }

    trySentence($event, field, l, txtDef) {
        $event.preventDefault();
        this.confirmationService.confirm({
            header: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessageHeader'),
            message: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessage', {
                lang: l,
                domain: this.phrase.domain.name
            }),
            icon: 'fa fa-warning',
            accept: () => {
                this.sentenceService.reloadTexcat(l, this.phrase.domain.name).subscribe(
                    res => {
                        this.loadPreview($event, field, l, txtDef);
                    },
                    err => {
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadErrorMessage')
                        });
                    }
                );
            },
            reject: () => {
                this.loadPreview($event, field, l, txtDef);
            }
        });
    }

    gotoSentence(name) {
        this.router.navigate(['sentenceEdit', name]);
    }

    reloadTexcat($event, field, l, txtDef) {
        this.confirmationService.confirm({
            header: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessageHeader'),
            message: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadConfirmationMessage', {
                lang: l,
                domain: this.phrase.domain.name
            }),
            icon: 'fa fa-warning',
            accept: () => {
                this.showProgressModal = true;
                this.sentenceService.reloadTexcat(l, this.phrase.domain.name).subscribe(
                    res => {
                        this.showProgressModal = false;
                    },
                    err => {
                        this.showProgressModal = false;
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceEdit.reloadTextcat.messages.reloadErrorMessage')
                        });
                    }
                );
            },
            reject: () => {
                this.showProgressModal = false;
            }
        });
    }
}
