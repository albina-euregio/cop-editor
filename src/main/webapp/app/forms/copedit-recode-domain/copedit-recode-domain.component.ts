import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SidebarService } from 'app/shared/sidebar.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';
import { MenuItem, ConfirmationService } from 'primeng/api';
import { Domain } from 'app/shared/model/domain.model';
import { CopeditPromoteDomainService } from './copedit-recode-domain.service';

@Component({
    selector: 'jhi-copedit-recode-domain',
    templateUrl: 'copedit-recode-domain.component.html'
})
export class CopeditRecodeDomainComponent implements OnInit {
    domains: Array<Domain>;
    domainFrom: Domain;
    domainTo: Domain;
    showProgressModal: Boolean = false;
    breadcrumbs: MenuItem[];
    home: MenuItem;

    constructor(
        private route: ActivatedRoute,
        private http: HttpClient,
        private sidebarService: SidebarService,
        private messageService: MessageService,
        private translateService: TranslateService,
        private confirmationService: ConfirmationService,
        private promoteDomainService: CopeditPromoteDomainService
    ) {}

    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: [''] };
        this.breadcrumbs = [{ label: 'Recode domain' }];
        this.domains = this.route.snapshot.data.domains;
        this.sidebarService.showSidebar(false);
    }

    disablePromoteButton(): Boolean {
        return (
            this.domainFrom === undefined ||
            this.domainTo === undefined ||
            (this.domainFrom !== undefined && this.domainTo !== undefined && this.domainFrom === this.domainTo)
        );
    }

    promoteDomain(): void {
        this.confirmationService.confirm({
            header: this.translateService.instant('appApp.promoteDomain.messages.promoteConfirmationMessageHeader'),
            message: this.translateService.instant('appApp.promoteDomain.messages.promoteConfirmationMessage', {
                domainFrom: this.domainFrom.name,
                domainTo: this.domainTo.name
            }),
            icon: 'fa fa-warning',
            accept: () => {
                this.showProgressModal = true;
                this.promoteDomainService.promoteDomain(this.domainFrom.id, this.domainTo.id).subscribe(
                    res => {
                        this.showProgressModal = false;
                        this.messageService.add({
                            severity: 'success',
                            detail: this.translateService.instant('appApp.promoteDomain.messages.recodeSuccessMessage')
                        });
                    },
                    err => {
                        this.showProgressModal = false;
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.promoteDomain.messages.recodeErrorMessage')
                        });
                    }
                );
            }
        });
    }
}
