import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { DomainsResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { GrowlModule } from 'primeng/growl';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { NavbarComponent } from 'app/layouts';
import { DialogModule } from 'primeng/dialog';
import { ProgressBarModule, BreadcrumbModule } from 'primeng/primeng';
import { CopeditRecodeDomainComponent } from './copedit-recode-domain.component';
import { recodeDomainRoute } from './copedit-recode-domain.route';
import { CopeditPromoteDomainService } from './copedit-recode-domain.service';

const ENTITY_STATES = [...recodeDomainRoute];

@NgModule({
    imports: [
        AppSharedModule,
        FieldsetModule,
        InputTextModule,
        DropdownModule,
        DialogModule,
        BreadcrumbModule,
        ButtonModule,
        RouterModule.forChild(ENTITY_STATES),
        MessagesModule,
        MessageModule,
        GrowlModule,
        ConfirmDialogModule,
        ProgressBarModule
    ],
    declarations: [CopeditRecodeDomainComponent],
    providers: [DomainsResolver, NavbarComponent, CopeditPromoteDomainService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopeditRecodeDomainModule {}
