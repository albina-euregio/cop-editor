import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CopeditPromoteDomainService {
    private resourceUrl = SERVER_API_URL + 'api/copedit/promoteDomain';

    constructor(private http: HttpClient) {}

    promoteDomain(domainFrom: number, domainTo: number) {
        return this.http.get<any>(this.resourceUrl + `?domainFrom=${domainFrom}&domainTo=${domainTo}`, { observe: 'response' });
    }
}
