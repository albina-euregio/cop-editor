import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { DomainsResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { GrowlModule } from 'primeng/growl';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CopeditExportComponent } from 'app/forms/copedit-export/copedit-export.component';
import { copeditExportRoute } from 'app/forms/copedit-export/copedit-export.route';
import { NavbarComponent } from 'app/layouts';
import { DialogModule } from 'primeng/dialog';
import { ProgressBarModule, BreadcrumbModule } from 'primeng/primeng';
import { CopeditExportService } from 'app/forms/copedit-export/copedit-export.service';

const ENTITY_STATES = [...copeditExportRoute];

@NgModule({
    imports: [
        AppSharedModule,
        FieldsetModule,
        CheckboxModule,
        InputTextModule,
        DropdownModule,
        DialogModule,
        BreadcrumbModule,
        ButtonModule,
        RouterModule.forChild(ENTITY_STATES),
        MessagesModule,
        MessageModule,
        GrowlModule,
        ConfirmDialogModule,
        ProgressBarModule
    ],
    declarations: [CopeditExportComponent],
    providers: [DomainsResolver, NavbarComponent, CopeditExportService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopeditExportModule {}
