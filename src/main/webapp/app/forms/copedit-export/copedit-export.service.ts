import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class CopeditExportService {
    private resourceUrl = SERVER_API_URL + 'api/copedit/export';

    constructor(private http: HttpClient) {}

    getRemoteBranches() {
        const head = new Headers();
        head.append('Accept', 'application/json');
        return this.http.get<any>(this.resourceUrl + '/git/getRemoteBranches', { observe: 'response' });
    }

    initBranch() {
        return this.http.get<any>(this.resourceUrl + '/git/initBranch', { observe: 'response' });
    }

    selectRemoteBranch(localBranch: string) {
        let options: HttpParams = new HttpParams();
        options = options.set('rb', localBranch);
        return this.http.get<any>(this.resourceUrl + '/git/selectRemoteBranch', { params: options, observe: 'response' });
    }

    pushToGit(domainId: number, branch: string) {
        let options: HttpParams = new HttpParams();
        options = options.set('domain', domainId.toString());
        options = options.set('branchName', branch);
        return this.http.get<any>(this.resourceUrl + '/git/pushToGit', { params: options, observe: 'response' });
    }

    exportDB(domainId: number) {
        let options: HttpParams = new HttpParams();
        options = options.set('domain', domainId.toString());
        return this.http.get<any>(this.resourceUrl + '/git/exportDB', { params: options, observe: 'response' });
    }
}
