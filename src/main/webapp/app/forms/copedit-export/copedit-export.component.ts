import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { Domain } from 'app/shared/model/domain.model';
import { SidebarService } from 'app/shared/sidebar.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenuItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { CopeditExportService } from 'app/forms/copedit-export/copedit-export.service';
import { Branch } from 'app/shared/model/branch.model';

@Component({
    selector: 'jhi-copedit-export',
    templateUrl: 'copedit-export.component.html'
})
export class CopeditExportComponent implements OnInit {
    private resourceUrl = SERVER_API_URL + 'api/copedit/export';
    domains: Array<Domain>;
    selectedDomain: Domain;
    showProgressModal: Boolean = false;
    breadcrumbs: MenuItem[];
    home: MenuItem;
    branches: Array<Branch>;
    selectedBranch: Branch;
    status: string;
    loading: boolean;

    constructor(
        private route: ActivatedRoute,
        private http: HttpClient,
        private sidebarService: SidebarService,
        private messageService: MessageService,
        private copeditExportService: CopeditExportService,
        private translateService: TranslateService
    ) {}

    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: [''] };
        this.breadcrumbs = [{ label: 'Export' }];
        this.domains = this.route.snapshot.data.domains;
        this.sidebarService.showSidebar(false);
        this.status = '';
        this.loading = false;
        this.copeditExportService.getRemoteBranches().subscribe(res => {
            this.branches = res.body;
        });
    }

    loadBranch($event) {
        this.copeditExportService.selectRemoteBranch($event.value.name).subscribe(res => {
            // this.branches = res.body;
            const debug = '';
        });
    }

    pushToGit() {
        this.status = 'Please wait... Export to git started';
        this.loading = true;
        this.copeditExportService.pushToGit(this.selectedDomain.id, this.selectedBranch.label).subscribe(res => {
            this.status = 'Export to git finished';
            this.loading = false;
            const debug = '';
        });
    }

    exportDB() {
        this.status = 'Please wait... Export db started';
        this.loading = true;
        this.copeditExportService.exportDB(this.selectedDomain.id).subscribe(res => {
            this.status = 'Export db finished';
            this.loading = false;
            const debug = '';
        });
    }

    /**
     * Call to get export zip file
     *
     * @memberof CopeditExportComponent
     */
    download() {
        this.showProgressModal = true;
        this.http
            .get(`${this.resourceUrl}?domainId=${this.selectedDomain.id}`, {
                responseType: 'blob',
                observe: 'response',
                headers: { Accept: 'application/octet-stream' }
            })
            .subscribe(
                res => {
                    this.showProgressModal = false;
                    this.downLoadFile(res);
                },
                err => {
                    this.showProgressModal = false;
                    this.messageService.add({
                        severity: 'error',
                        detail: this.translateService.instant('appApp.export.messages.exportErrorMessage')
                    });
                }
            );
    }

    /**
     * Downloads the file
     * @param data - response
     */
    downLoadFile(data: any) {
        const myRegexp = /.*filename="(.*)"/g;
        const match = myRegexp.exec(data.headers.get('content-disposition'));
        const fileName = match[1];

        const url = window.URL.createObjectURL(data.body);
        const anchor = document.createElement('a');
        anchor.download = fileName;
        anchor.href = url;
        document.body.appendChild(anchor);
        anchor.click();
        setTimeout(() => {
            anchor.remove();
        }, 3000);
    }
}
