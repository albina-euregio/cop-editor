export interface IVersion {
    id?: number;
    version?: string;
    basisVersion?: string;
}

export class Version implements IVersion {
    constructor(public id?: number, public version?: string, public basisVersion?: string) {}
}
