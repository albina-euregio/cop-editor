export interface IBranch {
    label?: string;
    name?: string;
}

export class Branch implements IBranch {
    constructor(public label?: string, public name?: string) {}
}
