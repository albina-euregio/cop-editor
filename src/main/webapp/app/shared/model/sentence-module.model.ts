import { Moment } from 'moment';

export interface ISentenceModule {
    id?: number;
    sentenceId?: number;
    phraseOptionId?: number;
    moduleNo?: number;
    datenew?: Moment;
    datelast?: Moment;
}

export class SentenceModule implements ISentenceModule {
    constructor(
        public id?: number,
        public sentenceId?: number,
        public phraseOptionId?: number,
        public moduleNo?: number,
        public datenew?: Moment,
        public datelast?: Moment
    ) {}
}
