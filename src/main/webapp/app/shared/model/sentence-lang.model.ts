import { Moment } from 'moment';

export enum LanguageEnum {
    DE = 'DE',
    EN = 'EN',
    IT = 'IT',
    FR = 'FR',
    ES = 'ES',
    CA = 'CA',
    OC = 'OC'
}

export interface ISentenceLang {
    id?: number;
    name?: string;
    header?: string;
    language?: LanguageEnum;
    updatedDate?: Moment;
    structure?: string;
    remark?: string;
}

export class SentenceLang implements ISentenceLang {
    constructor(
        public id?: number,
        public name?: string,
        public header?: string,
        public language?: LanguageEnum,
        public updatedDate?: Moment,
        public structure?: string,
        public remark?: string
    ) {}
}
