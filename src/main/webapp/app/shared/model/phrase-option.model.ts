import { Moment } from 'moment';

export interface IPhraseOption {
    id?: number;
    name?: string;
    language?: string;
    header?: string;
    version?: string;
    basisVersion?: string;
    remark?: string;
    deleted?: boolean;
    domainId?: number;
    datenew?: Moment;
    datelast?: Moment;
}

export class PhraseOption implements IPhraseOption {
    constructor(
        public id?: number,
        public name?: string,
        public language?: string,
        public header?: string,
        public version?: string,
        public basisVersion?: string,
        public remark?: string,
        public deleted?: boolean,
        public domainId?: number,
        public datenew?: Moment,
        public datelast?: Moment
    ) {
        this.deleted = false;
    }
}
