import { Moment } from 'moment';

export interface ISentence {
    id?: number;
    name?: string;
    header?: string;
    language?: string;
    structure?: string;
    version?: string;
    basisVersion?: string;
    remark?: string;
    deleted?: boolean;
    jokerSentence?: boolean;
    domainId?: number;
    datenew?: Moment;
    datelast?: Moment;
}

export class Sentence implements ISentence {
    constructor(
        public id?: number,
        public name?: string,
        public header?: string,
        public language?: string,
        public structure?: string,
        public version?: string,
        public basisVersion?: string,
        public remark?: string,
        public deleted?: boolean,
        public jokerSentence?: boolean,
        public domainId?: number,
        public datenew?: Moment,
        public datelast?: Moment
    ) {
        this.deleted = false;
        this.jokerSentence = false;
    }
}
