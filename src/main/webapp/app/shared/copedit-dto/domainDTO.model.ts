import { Moment } from 'moment';

export interface IDomainDTO {
    id?: number;
    name?: string;
}

export class DomainDTO implements IDomainDTO {
    constructor(public id?: number, public name?: string) { }
}
