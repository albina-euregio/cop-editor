import { Moment } from 'moment';
import { DomainDTO } from 'app/shared/copedit-dto/domainDTO.model';
import { ISentenceLanguageDTO } from 'app/shared/copedit-dto/sentenceLanguageDTO.model';
import { VersionDTO } from 'app/shared/copedit-dto/versionDTO.model';

export interface ISentenceDTO {
    domain?: DomainDTO;
    deleted?: Boolean;
    jokerSentence?: Boolean;
    datelast?: Moment;
    version?: VersionDTO;
    sentencesDE?: ISentenceLanguageDTO;
    sentencesIT?: ISentenceLanguageDTO;
    sentencesEN?: ISentenceLanguageDTO;
    sentencesFR?: ISentenceLanguageDTO;
    sentencesES?: ISentenceLanguageDTO;
    sentencesCA?: ISentenceLanguageDTO;
    sentencesOC?: ISentenceLanguageDTO;
}

export class SentenceDTO implements ISentenceDTO {
    constructor(
        public domain?: DomainDTO,
        public deleted?: Boolean,
        public jokerSentence?: Boolean,
        public datelast?: Moment,
        public version?: VersionDTO,
        public sentencesDE?: ISentenceLanguageDTO,
        public sentencesIT?: ISentenceLanguageDTO,
        public sentencesEN?: ISentenceLanguageDTO,
        public sentencesFR?: ISentenceLanguageDTO,
        public sentencesES?: ISentenceLanguageDTO,
        public sentencesCA?: ISentenceLanguageDTO,
        public sentencesOC?: ISentenceLanguageDTO
    ) {}
}
