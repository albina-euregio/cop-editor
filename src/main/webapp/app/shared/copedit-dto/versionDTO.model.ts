export interface IVersionDTO {
    version?: string;
    baseVersion?: string;
}

export class VersionDTO implements IVersionDTO {
    constructor(public version?: string, public baseVersion?: string) {}
}
