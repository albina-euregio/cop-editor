import { Moment } from 'moment';
import { PhraseDTO } from 'app/shared/copedit-dto/phraseDTO.model';

export interface IPhraseOptionItemDTO {
    phraseOptionItemId?: number;
    itemNo?: number;
    itemPartNo?: number;
    deleted?: Boolean;
    phraseOptionId?: number;
    version?: string;
    basisVersion?: string;
    datenew?: Moment;
    datelast?: Moment;
    phrase?: PhraseDTO;
}

export class PhraseOptionItemDTO implements IPhraseOptionItemDTO {
    constructor(
        public phraseOptionId?: number,
        public phraseOptionItemId?: number,
        public itemNo?: number,
        public itemPartNo?: number,
        public deleted?: Boolean,
        public version?: string,
        public basisVersion?: string,
        public datenew?: Moment,
        public datelast?: Moment,
        public phrase?: PhraseDTO
    ) {
        this.phrase = new PhraseDTO();
        this.deleted = false;
        this.phraseOptionId = phraseOptionId;
        this.basisVersion = '1.0';
        this.version = '1.0';
    }
}
