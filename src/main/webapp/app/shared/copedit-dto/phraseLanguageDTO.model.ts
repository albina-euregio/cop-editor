import { Moment } from 'moment';
import { PhraseOptionItemDTO } from 'app/shared/copedit-dto/phraseOptionItemDTO.model';

export interface IPhraseLanguageDTO {
    id?: number;
    language?: string;
    name?: string;
    header?: string;
    remark?: string;
    datenew?: Moment;
    datelast?: Moment;
    phrases?: Array<PhraseOptionItemDTO>;
    phrasesItemPartNo2?: Array<PhraseOptionItemDTO>;
}

export class PhraseLanguageDTO implements IPhraseLanguageDTO {
    constructor(
        public id?: number,
        public language?: string,
        public name?: string,
        public header?: string,
        public remark?: string,
        public datenew?: Moment,
        public datelast?: Moment,
        public phrases?: Array<PhraseOptionItemDTO>,
        public phrasesItemPartNo2?: Array<PhraseOptionItemDTO>
    ) {}
}
