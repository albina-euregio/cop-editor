import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { AppSharedLibsModule, AppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { SidebarService } from 'app/shared/sidebar.service';

@NgModule({
    imports: [AppSharedLibsModule, AppSharedCommonModule],
    declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }, SidebarService],
    entryComponents: [JhiLoginModalComponent],
    exports: [AppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppSharedModule {}
