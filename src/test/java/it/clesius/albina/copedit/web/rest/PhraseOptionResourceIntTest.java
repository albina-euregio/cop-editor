package it.clesius.albina.copedit.web.rest;

import it.clesius.albina.copedit.AppApp;

import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.repository.PhraseOptionRepository;
import it.clesius.albina.copedit.service.PhraseOptionService;
import it.clesius.albina.copedit.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static it.clesius.albina.copedit.web.rest.TestUtil.sameInstant;
import static it.clesius.albina.copedit.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PhraseOptionResource REST controller.
 *
 * @see PhraseOptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApp.class)
public class PhraseOptionResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE = "BBBBBBBBBB";

    private static final String DEFAULT_HEADER = "AAAAAAAAAA";
    private static final String UPDATED_HEADER = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_BASIS_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_BASIS_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_REMARK = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_DOMAIN_ID = 1;
    private static final Integer UPDATED_DOMAIN_ID = 2;

    private static final ZonedDateTime DEFAULT_DATENEW = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATENEW = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DATELAST = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATELAST = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private PhraseOptionRepository phraseOptionRepository;

    

    @Autowired
    private PhraseOptionService phraseOptionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPhraseOptionMockMvc;

    private PhraseOption phraseOption;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PhraseOptionResource phraseOptionResource = new PhraseOptionResource(phraseOptionService);
        this.restPhraseOptionMockMvc = MockMvcBuilders.standaloneSetup(phraseOptionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhraseOption createEntity(EntityManager em) {
        PhraseOption phraseOption = new PhraseOption()
            .name(DEFAULT_NAME)
            .language(DEFAULT_LANGUAGE)
            .header(DEFAULT_HEADER)
            .version(DEFAULT_VERSION)
            .basisVersion(DEFAULT_BASIS_VERSION)
            .remark(DEFAULT_REMARK)
            .deleted(DEFAULT_DELETED)
            .domainId(DEFAULT_DOMAIN_ID)
            .datenew(DEFAULT_DATENEW)
            .datelast(DEFAULT_DATELAST);
        return phraseOption;
    }

    @Before
    public void initTest() {
        phraseOption = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhraseOption() throws Exception {
        int databaseSizeBeforeCreate = phraseOptionRepository.findAll().size();

        // Create the PhraseOption
        restPhraseOptionMockMvc.perform(post("/api/phrase-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phraseOption)))
            .andExpect(status().isCreated());

        // Validate the PhraseOption in the database
        List<PhraseOption> phraseOptionList = phraseOptionRepository.findAll();
        assertThat(phraseOptionList).hasSize(databaseSizeBeforeCreate + 1);
        PhraseOption testPhraseOption = phraseOptionList.get(phraseOptionList.size() - 1);
        assertThat(testPhraseOption.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPhraseOption.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testPhraseOption.getHeader()).isEqualTo(DEFAULT_HEADER);
        assertThat(testPhraseOption.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testPhraseOption.getBasisVersion()).isEqualTo(DEFAULT_BASIS_VERSION);
        assertThat(testPhraseOption.getRemark()).isEqualTo(DEFAULT_REMARK);
        assertThat(testPhraseOption.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testPhraseOption.getDomainId()).isEqualTo(DEFAULT_DOMAIN_ID);
        assertThat(testPhraseOption.getDatenew()).isEqualTo(DEFAULT_DATENEW);
        assertThat(testPhraseOption.getDatelast()).isEqualTo(DEFAULT_DATELAST);
    }

    @Test
    @Transactional
    public void createPhraseOptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phraseOptionRepository.findAll().size();

        // Create the PhraseOption with an existing ID
        phraseOption.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhraseOptionMockMvc.perform(post("/api/phrase-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phraseOption)))
            .andExpect(status().isBadRequest());

        // Validate the PhraseOption in the database
        List<PhraseOption> phraseOptionList = phraseOptionRepository.findAll();
        assertThat(phraseOptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPhraseOptions() throws Exception {
        // Initialize the database
        phraseOptionRepository.saveAndFlush(phraseOption);

        // Get all the phraseOptionList
        restPhraseOptionMockMvc.perform(get("/api/phrase-options?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phraseOption.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].header").value(hasItem(DEFAULT_HEADER.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())))
            .andExpect(jsonPath("$.[*].basisVersion").value(hasItem(DEFAULT_BASIS_VERSION.toString())))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK.toString())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].domainId").value(hasItem(DEFAULT_DOMAIN_ID)))
            .andExpect(jsonPath("$.[*].datenew").value(hasItem(sameInstant(DEFAULT_DATENEW))))
            .andExpect(jsonPath("$.[*].datelast").value(hasItem(sameInstant(DEFAULT_DATELAST))));
    }
    

    @Test
    @Transactional
    public void getPhraseOption() throws Exception {
        // Initialize the database
        phraseOptionRepository.saveAndFlush(phraseOption);

        // Get the phraseOption
        restPhraseOptionMockMvc.perform(get("/api/phrase-options/{id}", phraseOption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(phraseOption.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.header").value(DEFAULT_HEADER.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.toString()))
            .andExpect(jsonPath("$.basisVersion").value(DEFAULT_BASIS_VERSION.toString()))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK.toString()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.domainId").value(DEFAULT_DOMAIN_ID))
            .andExpect(jsonPath("$.datenew").value(sameInstant(DEFAULT_DATENEW)))
            .andExpect(jsonPath("$.datelast").value(sameInstant(DEFAULT_DATELAST)));
    }
    @Test
    @Transactional
    public void getNonExistingPhraseOption() throws Exception {
        // Get the phraseOption
        restPhraseOptionMockMvc.perform(get("/api/phrase-options/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhraseOption() throws Exception {
        // Initialize the database
        phraseOptionService.save(phraseOption);

        int databaseSizeBeforeUpdate = phraseOptionRepository.findAll().size();

        // Update the phraseOption
        PhraseOption updatedPhraseOption = phraseOptionRepository.findById(phraseOption.getId()).get();
        // Disconnect from session so that the updates on updatedPhraseOption are not directly saved in db
        em.detach(updatedPhraseOption);
        updatedPhraseOption
            .name(UPDATED_NAME)
            .language(UPDATED_LANGUAGE)
            .header(UPDATED_HEADER)
            .version(UPDATED_VERSION)
            .basisVersion(UPDATED_BASIS_VERSION)
            .remark(UPDATED_REMARK)
            .deleted(UPDATED_DELETED)
            .domainId(UPDATED_DOMAIN_ID)
            .datenew(UPDATED_DATENEW)
            .datelast(UPDATED_DATELAST);

        restPhraseOptionMockMvc.perform(put("/api/phrase-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPhraseOption)))
            .andExpect(status().isOk());

        // Validate the PhraseOption in the database
        List<PhraseOption> phraseOptionList = phraseOptionRepository.findAll();
        assertThat(phraseOptionList).hasSize(databaseSizeBeforeUpdate);
        PhraseOption testPhraseOption = phraseOptionList.get(phraseOptionList.size() - 1);
        assertThat(testPhraseOption.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPhraseOption.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testPhraseOption.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testPhraseOption.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testPhraseOption.getBasisVersion()).isEqualTo(UPDATED_BASIS_VERSION);
        assertThat(testPhraseOption.getRemark()).isEqualTo(UPDATED_REMARK);
        assertThat(testPhraseOption.isDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testPhraseOption.getDomainId()).isEqualTo(UPDATED_DOMAIN_ID);
        assertThat(testPhraseOption.getDatenew()).isEqualTo(UPDATED_DATENEW);
        assertThat(testPhraseOption.getDatelast()).isEqualTo(UPDATED_DATELAST);
    }

    @Test
    @Transactional
    public void updateNonExistingPhraseOption() throws Exception {
        int databaseSizeBeforeUpdate = phraseOptionRepository.findAll().size();

        // Create the PhraseOption

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPhraseOptionMockMvc.perform(put("/api/phrase-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phraseOption)))
            .andExpect(status().isBadRequest());

        // Validate the PhraseOption in the database
        List<PhraseOption> phraseOptionList = phraseOptionRepository.findAll();
        assertThat(phraseOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhraseOption() throws Exception {
        // Initialize the database
        phraseOptionService.save(phraseOption);

        int databaseSizeBeforeDelete = phraseOptionRepository.findAll().size();

        // Get the phraseOption
        restPhraseOptionMockMvc.perform(delete("/api/phrase-options/{id}", phraseOption.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PhraseOption> phraseOptionList = phraseOptionRepository.findAll();
        assertThat(phraseOptionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhraseOption.class);
        PhraseOption phraseOption1 = new PhraseOption();
        phraseOption1.setId(1L);
        PhraseOption phraseOption2 = new PhraseOption();
        phraseOption2.setId(phraseOption1.getId());
        assertThat(phraseOption1).isEqualTo(phraseOption2);
        phraseOption2.setId(2L);
        assertThat(phraseOption1).isNotEqualTo(phraseOption2);
        phraseOption1.setId(null);
        assertThat(phraseOption1).isNotEqualTo(phraseOption2);
    }
}
