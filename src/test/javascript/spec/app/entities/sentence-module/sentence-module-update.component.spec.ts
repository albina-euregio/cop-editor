/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { SentenceModuleUpdateComponent } from 'app/entities/sentence-module/sentence-module-update.component';
import { SentenceModuleService } from 'app/entities/sentence-module/sentence-module.service';
import { SentenceModule } from 'app/shared/model/sentence-module.model';

describe('Component Tests', () => {
    describe('SentenceModule Management Update Component', () => {
        let comp: SentenceModuleUpdateComponent;
        let fixture: ComponentFixture<SentenceModuleUpdateComponent>;
        let service: SentenceModuleService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceModuleUpdateComponent]
            })
                .overrideTemplate(SentenceModuleUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SentenceModuleUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SentenceModuleService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SentenceModule(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sentenceModule = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SentenceModule();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sentenceModule = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
