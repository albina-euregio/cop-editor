/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppTestModule } from '../../../test.module';
import { SentenceModuleComponent } from 'app/entities/sentence-module/sentence-module.component';
import { SentenceModuleService } from 'app/entities/sentence-module/sentence-module.service';
import { SentenceModule } from 'app/shared/model/sentence-module.model';

describe('Component Tests', () => {
    describe('SentenceModule Management Component', () => {
        let comp: SentenceModuleComponent;
        let fixture: ComponentFixture<SentenceModuleComponent>;
        let service: SentenceModuleService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceModuleComponent],
                providers: []
            })
                .overrideTemplate(SentenceModuleComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SentenceModuleComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SentenceModuleService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SentenceModule(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.sentenceModules[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
