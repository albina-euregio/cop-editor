/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { SentenceModuleDetailComponent } from 'app/entities/sentence-module/sentence-module-detail.component';
import { SentenceModule } from 'app/shared/model/sentence-module.model';

describe('Component Tests', () => {
    describe('SentenceModule Management Detail Component', () => {
        let comp: SentenceModuleDetailComponent;
        let fixture: ComponentFixture<SentenceModuleDetailComponent>;
        const route = ({ data: of({ sentenceModule: new SentenceModule(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceModuleDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SentenceModuleDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SentenceModuleDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.sentenceModule).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
