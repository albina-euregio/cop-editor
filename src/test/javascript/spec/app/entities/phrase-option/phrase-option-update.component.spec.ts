/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { PhraseOptionUpdateComponent } from 'app/entities/phrase-option/phrase-option-update.component';
import { PhraseOptionService } from 'app/entities/phrase-option/phrase-option.service';
import { PhraseOption } from 'app/shared/model/phrase-option.model';

describe('Component Tests', () => {
    describe('PhraseOption Management Update Component', () => {
        let comp: PhraseOptionUpdateComponent;
        let fixture: ComponentFixture<PhraseOptionUpdateComponent>;
        let service: PhraseOptionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [PhraseOptionUpdateComponent]
            })
                .overrideTemplate(PhraseOptionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PhraseOptionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PhraseOptionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PhraseOption(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.phraseOption = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PhraseOption();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.phraseOption = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
