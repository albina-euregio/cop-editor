/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppTestModule } from '../../../test.module';
import { PhraseOptionDeleteDialogComponent } from 'app/entities/phrase-option/phrase-option-delete-dialog.component';
import { PhraseOptionService } from 'app/entities/phrase-option/phrase-option.service';

describe('Component Tests', () => {
    describe('PhraseOption Management Delete Component', () => {
        let comp: PhraseOptionDeleteDialogComponent;
        let fixture: ComponentFixture<PhraseOptionDeleteDialogComponent>;
        let service: PhraseOptionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [PhraseOptionDeleteDialogComponent]
            })
                .overrideTemplate(PhraseOptionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PhraseOptionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PhraseOptionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
