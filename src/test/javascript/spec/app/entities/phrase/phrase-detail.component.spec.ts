/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { PhraseDetailComponent } from 'app/entities/phrase/phrase-detail.component';
import { Phrase } from 'app/shared/model/phrase.model';

describe('Component Tests', () => {
    describe('Phrase Management Detail Component', () => {
        let comp: PhraseDetailComponent;
        let fixture: ComponentFixture<PhraseDetailComponent>;
        const route = ({ data: of({ phrase: new Phrase(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [PhraseDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PhraseDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PhraseDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.phrase).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
