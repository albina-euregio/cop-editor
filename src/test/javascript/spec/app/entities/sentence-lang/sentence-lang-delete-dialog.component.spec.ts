/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppTestModule } from '../../../test.module';
import { SentenceLangDeleteDialogComponent } from 'app/entities/sentence-lang/sentence-lang-delete-dialog.component';
import { SentenceLangService } from 'app/entities/sentence-lang/sentence-lang.service';

describe('Component Tests', () => {
    describe('SentenceLang Management Delete Component', () => {
        let comp: SentenceLangDeleteDialogComponent;
        let fixture: ComponentFixture<SentenceLangDeleteDialogComponent>;
        let service: SentenceLangService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceLangDeleteDialogComponent]
            })
                .overrideTemplate(SentenceLangDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SentenceLangDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SentenceLangService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
