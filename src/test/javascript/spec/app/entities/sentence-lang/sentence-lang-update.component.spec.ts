/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { SentenceLangUpdateComponent } from 'app/entities/sentence-lang/sentence-lang-update.component';
import { SentenceLangService } from 'app/entities/sentence-lang/sentence-lang.service';
import { SentenceLang } from 'app/shared/model/sentence-lang.model';

describe('Component Tests', () => {
    describe('SentenceLang Management Update Component', () => {
        let comp: SentenceLangUpdateComponent;
        let fixture: ComponentFixture<SentenceLangUpdateComponent>;
        let service: SentenceLangService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceLangUpdateComponent]
            })
                .overrideTemplate(SentenceLangUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SentenceLangUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SentenceLangService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SentenceLang(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sentenceLang = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SentenceLang();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sentenceLang = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
